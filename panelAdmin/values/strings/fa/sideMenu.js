const DASHBOARD = "داشبورد";
const SIDEBAR_ONE_TITLE = "عمومی";
const SIDEBAR_TWO_TITLE = "کاربردی";
const SETTING_WEB = "تنظیمات سایت";

const SEE_VARIABLE = "مشاهده متغییر";
const VARIABLE = "متغییر";
const ADD_VARIABLE = "افزودن متغییر";

const GALLERY = "گالری";
const GALLERIES = "گالری ها";
const ADD_GALLERY = "افزودن گالری";
const SEE_GALLERIES = "مشاهده گالری ها";

const CATEGORY = "دسته بندی";
const CATEGORIES = "دسته بندی ها";
const ADD_CATEGORY = "افزودن دسته بندی";
const SEE_CATEGORIES = "مشاهده دسته بندی ها";

const PRODUCT = "محصول ";
const ADD_PRODUCT = "افزودن محصول";
const SEE_PRODUCTS = "مشاهده محصولات ";

const SLIDER = "اسلایدر ";
const ADD_SLIDER = "افزودن اسلایدر";
const SEE_SLIDERS = "مشاهده اسلایدر ها ";

const BANNER = "بنر ";
const ADD_BANNER = "افزودن بنر";
const SEE_BANNERS = "مشاهده بنر ها ";

const ARTICLES = "مقالات";
const BLOG_CATEGORY = "دسته بندی مقالات";
const SHOW_ARTICLES = " مشاهده مقالات";
const COMMENT = "نظرات";
const sideMenu = {
  DASHBOARD,

  SIDEBAR_ONE_TITLE,
  SIDEBAR_TWO_TITLE,

  SETTING_WEB,

  CATEGORIES,

  SEE_VARIABLE,
  VARIABLE,
  ADD_VARIABLE,

  GALLERY,
  GALLERIES,
  ADD_GALLERY,
  SEE_GALLERIES,

  CATEGORY,
  ADD_CATEGORY,
  SEE_CATEGORIES,

  PRODUCT,
  ADD_PRODUCT,
  SEE_PRODUCTS,

  SLIDER,
  ADD_SLIDER,
  SEE_SLIDERS,

  BANNER,
  ADD_BANNER,
  SEE_BANNERS,

  ARTICLES,
  BLOG_CATEGORY,
  SHOW_ARTICLES,

  COMMENT,
};
export default sideMenu;
