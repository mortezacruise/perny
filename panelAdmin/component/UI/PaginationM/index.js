import React, { Fragment } from "react";
import { Pagination } from "react-bootstrap";
const PaginationM = (props) => {
  const { limited, pages, activePage, onClick } = props;
  console.log({ pages });

  let sort = [];
  if (pages === undefined || pages === 1) return <div></div>;
  Number(activePage);
  // console.log({ limited, pages, activePage });
  let viewNext = +limited + +activePage;
  let viewPrev = activePage - limited;
  console.log({ viewNext, viewPrev });
  for (let index = 1; index <= pages; index++) {
    if (pages >= 9) {
      // console.log({ check: pages - limited }, limited - pages === index, { index });

      if (2 == index && viewPrev == 2) sort.unshift(1);
      if (viewPrev <= index && viewNext >= index) sort.push(index);
    } else if (pages <= 9) sort.push(index);
  }

  if (pages - limited - 1 == activePage) sort.push(pages);
  console.log({ active: pages - limited - 1 == activePage, sort });

  let prevFirst = Number(activePage) < Number(limited);
  let prevSecond = Number(activePage) === Number(1);
  let nextFirst = Number(activePage) === Number(pages);
  let nextSecond = Number(activePage) > Number(pages - limited);
  // console.log({ prevFirst });
  // console.log({ prevSecond });
  // console.log({ nextFirst });
  // console.log({ nextSecond });

  let Pagination_Sort = (
    <div className="Pagination-wrapper">
      {" "}
      <div className="paginationIcon centerAll translateR transition0-2" disabled={prevFirst} onClick={() => (prevFirst ? "" : onClick(1))}>
        <i className="fa fa-angle-double-right" aria-hidden="true" />
      </div>
      <div className="paginationIcon centerAll translateR transition0-2" disabled={prevSecond} onClick={() => (prevSecond ? "" : onClick(activePage - 1))}>
        <i className="fa fa-angle-right" aria-hidden="true" />
      </div>
      {Number(activePage) > 2 + Number(limited) && pages >= 9 && (
        <Fragment>
          <div className="paginationIcon centerAll translateT transition0-2" onClick={() => onClick(1)}>
            {1}
          </div>
          <div className="paginationIcon centerAll">
            <i className="fa fa-ellipsis-h" aria-hidden="true" />
          </div>
        </Fragment>
      )}
      {sort.map((number, index) => {
        if (number == 1 && +activePage > +limited + 2 && +pages >= 9) return;
        else if (number === pages && activePage < pages - limited && pages >= 9) return;
        else
          return (
            <>
              <div className={`paginationIcon centerAll translateT transition0-2 ${number === Number(activePage) && "actived"}`} onClick={() => onClick(number === Number(activePage) ? false : number)} key={index + "m"}>
                {number}
              </div>
            </>
          );
      })}
      {pages - limited - 1 == activePage && (
        <div className={`paginationIcon centerAll translateT transition0-2 `} onClick={() => onClick(number === Number(activePage) ? false : pages)}>
          {pages}
        </div>
      )}
      {activePage < pages - limited - 1 && pages >= 9 && (
        <Fragment>
          <div className="paginationIcon centerAll">
            <i className="fa fa-ellipsis-h" aria-hidden="true" />
          </div>

          <div className="paginationIcon centerAll translateT transition0-2" onClick={() => onClick(pages)}>
            {pages}
          </div>
        </Fragment>
      )}
      {/* <div className="paginationIcon centerAll translateT transition0-2">1</div>
      <div className="paginationIcon centerAll translateT transition0-2">2</div> */}
      {/* <div className="paginationIcon centerAll">
        <i className="fa fa-ellipsis-h" aria-hidden="true" />
      </div> */}
      <div className="paginationIcon centerAll translateL transition0-2" disabled={nextFirst} onClick={() => (nextFirst ? "" : onClick(+activePage + +1))}>
        <i className="fa fa-angle-left" aria-hidden="true" />
      </div>
      <div className="paginationIcon centerAll translateL transition0-2" disabled={nextSecond} onClick={() => (nextSecond ? "" : onClick(Number(pages)))}>
        <i className="fa fa-angle-double-left" aria-hidden="true" />
      </div>
      {/* <Pagination>
        <Pagination.First disabled={Number(activePage) < Number(limited)} onClick={() => onClick(1)} />
        <Pagination.Prev disabled={Number(activePage) === Number(1)} onClick={() => onClick(activePage - 1)} />
        {Number(activePage) > 1 + Number(limited) && pages >= 9 && (
          <Fragment>
            <Pagination.Item onClick={() => onClick(1)}>{1}</Pagination.Item>
            <Pagination.Ellipsis />
          </Fragment>
        )}
        {sort.map((number, index) => {
          if (number === 1 && activePage > +limited + +1 && pages >= 9) return;
          else if (number === pages && activePage < pages - limited && pages >= 9) return;
          else
            return (
              <Pagination.Item onClick={() => onClick(number === Number(activePage) ? false : number)} key={index + "m"} active={number === Number(activePage)}>
                {number}
              </Pagination.Item>
            );
        })}

        {activePage < pages - limited && pages >= 9 && (
          <Fragment>
            <Pagination.Ellipsis />

            <Pagination.Item onClick={() => onClick(pages)}>{pages}</Pagination.Item>
          </Fragment>
        )}
        <Pagination.Next disabled={Number(activePage) === Number(pages)} onClick={() => onClick(+activePage + +1)} />
        <Pagination.Last disabled={Number(activePage) > Number(pages - limited)} onClick={() => onClick(Number(pages))} />
      </Pagination> */}
    </div>
  );

  return Pagination_Sort;
};

export default PaginationM;
