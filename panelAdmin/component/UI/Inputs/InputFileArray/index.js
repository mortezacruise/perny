import React, { Fragment, useState } from "react";
const InputFileArray = (props) => {
  const { className, onChange, inputLabel, name, value, progress, disabled, cancelUpload, onKeyDown, removeHandel } = props;
  const [state, setState] = useState({ Form: {}, formIsValid: false });

  const elements = (
    <Fragment>
      {value.length > 0 && (
        <div className="data-show-array">
          {value.map((data, index) => {
            return (
              <div onClick={() => removeHandel(data)} key={index + "moj"}>
                {/* <img className="show-image-array" src={data} alt="image" /> */}
                <span>{data}</span>
              </div>
            );
          })}
        </div>
      )}
      <div className={`addFileModalContainer ${className}`}>
        <div>{"  انتخاب نمایید ..."}</div>
        <label>
          <span>{progress ? progress + "%" : inputLabel}</span>
          <input onKeyDown={onKeyDown} disabled={progress ? true : disabled} type="file" onChange={onChange} name={name} />
        </label>
      </div>
    </Fragment>
  );
  return elements;
};

export default InputFileArray;
