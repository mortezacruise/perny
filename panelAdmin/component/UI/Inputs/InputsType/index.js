import React, { useState, useEffect } from "react";
import Inputs from "../Input";
import onChanges from "../../../../util/onChanges";
import toastify from "../../../../util/toastify";

const InputsType = (props) => {
  const { showModal, onHideModal, inputsData, acceptedTitle, imageType, label, elementType, stateData } = props;
  const [data, setData] = useState(stateData);
  const [state, setState] = useState(false);
  const [loading, setLoading] = useState(false);
  ////console.log({ dataFile: data.Form.file });
  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  useEffect(() => {
    if (!showModal) {
      setData(stateData);
      setState(false);
    }
  }, [showModal]);
  const _handelOnchange = async (event, image) => {
    await onChanges.globalChange({ event, data, setData, setState, setLoading, imageType: imageType });
  };
  const returnData = () => {
    if (data) {
      if (data.Form.file.value === "") toastify("اطلاعات کامل نمی باشد", "error");
      else if (data) {
        ////console.log({ data });

        inputsData(data.Form.file.value.toString());
        onHideModal();
      } else toastify("اطلاعات کامل نمی باشد", "error");
    } else toastify("اطلاعات کامل نمی باشد", "error");
  };
  const elements = stateArray.map((formElement) => {
    const invalid = !formElement.config.valid;
    const shouldValidate = formElement.config.validation;
    const touched = formElement.config.touched;
    let accepted;
    const inputClasses = ["InputElement"];
    if (invalid && shouldValidate && touched) inputClasses.push("Invalid");
    return (
      <Inputs
        key={formElement.id}
        elementType={formElement.config.elementType}
        elementConfig={formElement.config.elementConfig}
        value={formElement.config.value}
        invalid={invalid}
        shouldValidate={shouldValidate}
        touched={touched}
        changed={(e) => _handelOnchange({ value: e.currentTarget.value, name: formElement.id, type: e.currentTarget.type, files: e.currentTarget.files })}
        accepted={accepted}
        // removeHandel={index => removeHandel(index, formElement.id)}
        disabled={loading}
        label={formElement.config.label}
        progress={state.progressPercentImage}
      />
    );
  });
  return (
    <div className="subjectModal-AddInformation">
      {elements}
      <div className="btn-submited-container">
        <div className="submitedBtn">
          <button disabled={loading} onClick={returnData}>
            {acceptedTitle}
          </button>
        </div>
        <div className="submitedBtn closed">
          <button onClick={!loading ? onHideModal : ""}>انصراف</button>
        </div>
      </div>
    </div>
  );
};

export default InputsType;
