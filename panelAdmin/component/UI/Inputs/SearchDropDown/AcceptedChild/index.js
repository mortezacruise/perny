import React from "react";

const AcceptedChild = ({ data }) => {
  return data ? (
    <div className="serached-item">
      {data.image ? (
        <div className="searched-image">
          <img src={data.image} alt="search title" />
        </div>
      ) : (
        ""
      )}
      <div className="searched-text">
        <span className="searched-title">{data.title}</span>
        <span className="searched-description">{data.description}</span>
      </div>
    </div>
  ) : (
    ""
  );
};

export default AcceptedChild;
