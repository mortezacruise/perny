import React, { useState, useRef, useEffect } from "react";
import { Fragment } from "react";
const InputPush = (props) => {
  const {
    accepted,
    inputType,
    className,
    value,
    removeHandel,
    name,
    elementConfig,
    disabled,
    checkSubmited,
  } = props;

  const [Typing, setTyping] = useState("");
  const acceptedClick = () => {
    if (Typing.length > 0) {
      accepted(Typing);
      setTyping("");
    }
  };
  useEffect(() => {
    setTyping("");
  }, [checkSubmited]);
  const submitRef = useRef(null);
  const inputClasses = [className, "transition0-3"];
  const handelOnkeyDown = (e) => {
    if (e.key === "Enter") {
      submitRef.current.click();
    }
  };
  return (
    <Fragment>
      {value.length > 0 && (
        <div className="data-show-array">
          {value &&
            value.map((data, index) => {
              return (
                <span onClick={() => removeHandel(data)} key={index}>
                  {data}
                </span>
              );
            })}
        </div>
      )}
      <div className="input-push-wrapper">
        <input
          disabled={disabled}
          type={inputType}
          className={inputClasses.join(" ")}
          onKeyDown={handelOnkeyDown}
          value={Typing}
          onChange={(e) => setTyping(e.currentTarget.value)}
          autoComplete="off"
          {...elementConfig}
        />
        <span ref={submitRef} type="submited" onClick={acceptedClick}>
          ثبت
        </span>
      </div>
    </Fragment>
  );
};

export default InputPush;
