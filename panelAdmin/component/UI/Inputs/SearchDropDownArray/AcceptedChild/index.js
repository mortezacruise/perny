import React from "react";

const AcceptedChild = ({ data, removeHandel }) => {
  ////console.log({ data });

  return data.length
    ? data.map((data, index) => {
        return (
          <div onClick={() => removeHandel(data.value)} key={index + "sadasd"} className="serached-item">
            {data.image ? (
              <div className="searched-image">
                <img src={data.image} alt="search title" />
              </div>
            ) : (
              ""
            )}
            <div className="searched-text">
              <span className="searched-title">{data.title}</span>
            </div>
          </div>
        );
      })
    : "";
};

export default AcceptedChild;
