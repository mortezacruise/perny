import React, { useState, useEffect } from "react";
import { FormControl, Dropdown } from "react-bootstrap";
import LazyImage from "../../../../../components/LazyImage";
const CustomMenu = React.forwardRef(({ children, style, className, "aria-labelledby": labeledBy }, ref) => {
  const [value, setValue] = useState("");

  return (
    <div ref={ref} style={style} className={className} aria-labelledby={labeledBy}>
      <FormControl autoFocus className="mx-3 my-2 w-auto" placeholder="جستجو ..." onChange={(e) => setValue(e.target.value)} value={value} />
      <ul className="list-unstyled">{React.Children.toArray(children).filter((child) => !value || child.props.children.toLowerCase().startsWith(value))}</ul>
    </div>
  );
});

const InputDropDownSearch = (props) => {
  const { accepted, dropDownData, value, className, checkSubmitted, disabled } = props;
  // //console.log({ dropDownData });

  let propsVal, index;
  index = dropDownData?.findIndex((d) => d.value === value);
  if (index >= 0) propsVal = dropDownData[index].title;
  const [Title, setTitle] = useState("");
  useEffect(() => {
    setTitle();
  }, [checkSubmitted]);
  const clickedElement = (value, title) => {
    setTitle(title);
    accepted(value);
  };
  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <a
      className={className ? className : ""}
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        return disabled ? "" : onClick(e);
      }}
    >
      {children}
      &#x25bc;
    </a>
  ));
  return (
    <Dropdown>
      <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
        {Title ? Title : propsVal ? propsVal : value ? value : "انتخاب نمایید  "}
      </Dropdown.Toggle>

      <Dropdown.Menu as={CustomMenu}>
        {dropDownData &&
          dropDownData.map((info, index) => {
            return (
              <Dropdown.Item className="drop-item" key={index + "m"} active={value === info.value} onClick={() => clickedElement(info.value, info.title)} eventKey={index + 1}>
                {info.image ? (
                  <div className="drop-image">
                    {" "}
                    <LazyImage src={info.image} defaultImage={false} alt={"search title"} />
                    {/* <img src={info.image} alt="search title" /> */}
                  </div>
                ) : (
                  ""
                )}
                <div className="drop-text">
                  <span className="drop-title">{info.title}</span>
                  <span className="drop-description">{info.description}</span>
                </div>
              </Dropdown.Item>
            );
          })}
      </Dropdown.Menu>
    </Dropdown>
  );
};

export default InputDropDownSearch;
