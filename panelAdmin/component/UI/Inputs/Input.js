import React, { memo } from "react";

import InputPush from "./InputPush";
import InputFile from "./InputFile";
import SearchDropDown from "./SearchDropDown";
import InputDropDownSearch from "./InputDropDownSearch";
import InputFileArray from "./InputFileArray";
// import DateCelander from "./DateCelander";

import DropdownBoot from "./DropdownBoot";
import DateInput from "./DateInput";
import SearchDropDownArray from "./SearchDropDownArray";
import panelAdmin from "../../..";
import InputDropDownSearchArray from "./InputDropDownSearchArray";
import CkEditor from "./CkEditor";
const handleKey = panelAdmin.utils.handleKey;
const Inputs = memo((props) => {
  const { titleValidity, progress, elementType, elementConfig, value, changed, accepted, label, invalid, shouldValidate, touched, removeHandel, dropDownData, defaultInputDesable, checkSubmitted, disabled, display, searchAccepted, setSearchAccepted, staticTitle } = props;
  let inputElement = null;
  const inputClasses = ["InputElement"];
  if (invalid && shouldValidate && touched) {
    inputClasses.push("Invalid");
  }
  let validStyle = "red";
  if (!touched && inputClasses.includes("Invalid")) {
    validStyle = "red";
  }
  if (touched && !inputClasses.includes("Invalid")) {
    validStyle = "green";
  }
  switch (elementType) {
    case "input":
      inputElement = <input onKeyDown={handleKey} disabled={disabled} className={inputClasses.join(" ")} {...elementConfig} value={value} onChange={changed} />;
      break;
    case "inputPush":
      inputElement = <InputPush checkSubmitted={checkSubmitted} disabled={disabled} className={inputClasses.join(" ")} accepted={accepted} value={value} elementConfig={elementConfig} removeHandel={removeHandel} />;
      break;

    case "inputFile":
      inputElement = <InputFile accepted={accepted} disabled={disabled} progress={progress} className={inputClasses.join(" ")} onChange={changed} value={value} {...elementConfig} inputLabel={"انتخاب"} label={label} />;
      break;
    case "InputFileArray":
      inputElement = <InputFileArray onKeyDown={handleKey} accepted={accepted} removeHandel={removeHandel} disabled={disabled} progress={progress} className={inputClasses.join(" ")} onChange={changed} value={value} {...elementConfig} inputLabel={"انتخاب"} label={label} />;
      break;

    case "inputSearch":
      inputElement = (
        <SearchDropDown
          onKeyDown={handleKey}
          checkSubmitted={checkSubmitted}
          dropDownData={dropDownData}
          accepted={accepted}
          className={inputClasses.join(" ")}
          onChange={changed}
          value={value}
          label={label}
          elementConfig={elementConfig}
          disabled={disabled}
          searchAccepted={searchAccepted}
          setSearchAccepted={setSearchAccepted}
          staticTitle={staticTitle}
        />
      );
      break;
    case "inputSearchArray":
      inputElement = (
        <SearchDropDownArray
          onKeyDown={handleKey}
          checkSubmitted={checkSubmitted}
          dropDownData={dropDownData}
          accepted={accepted}
          className={inputClasses.join(" ")}
          onChange={changed}
          value={value}
          label={label}
          elementConfig={elementConfig}
          disabled={disabled}
          searchAccepted={searchAccepted}
          setSearchAccepted={setSearchAccepted}
          staticTitle={staticTitle}
          removeHandel={removeHandel}
        />
      );
      break;

    case "inputDropDownSearch":
      inputElement = <InputDropDownSearch dropDownData={dropDownData} disabled={disabled} className={inputClasses.join(" ")} accepted={accepted} value={value} elementConfig={elementConfig} checkSubmitted={checkSubmitted} />;
      break;
    case "inputDropDownSearchArray":
      inputElement = <InputDropDownSearchArray dropDownData={dropDownData} disabled={disabled} className={inputClasses.join(" ")} accepted={accepted} value={value} elementConfig={elementConfig} checkSubmitted={checkSubmitted} />;
      break;
    case "textarea":
      inputElement = <textarea onKeyDown={handleKey} disabled={disabled} className={inputClasses.join(" ")} {...elementConfig} value={value} onChange={changed} />;
      break;
    case "select":
      inputElement = (
        <select disabled={disabled} className={inputClasses.join(" ")} value={value} onChange={changed}>
          {elementConfig.options.map((option) => (
            <option key={option.value} value={option.value}>
              {option.displayValue}
            </option>
          ))}
        </select>
      );
      break;
    case "inputDropDown":
      inputElement = <DropdownBoot dropDownData={dropDownData} disabled={disabled} className={inputClasses.join(" ")} accepted={accepted} value={value} elementConfig={elementConfig} checkSubmitted={checkSubmitted} />;
      break;
    // case "date":
    //   inputElement = (
    //     <DateCelander
    //       checkSubmitted={checkSubmitted}
    //       onKeyDown={handleKey}
    //       disabled={disabled}
    //       className={inputClasses.join(" ")}
    //       elementConfig={elementConfig}
    //       value={value}
    //       accepted={accepted}
    //     />
    //   );
    //   break;
    case "dateInput":
      inputElement = <DateInput checkSubmitted={checkSubmitted} onKeyDown={handleKey} disabled={disabled} className={inputClasses.join(" ")} elementConfig={elementConfig} value={value} onChange={changed} />;
      break;
    case "ckEditor":
      inputElement = <CkEditor onKeyDown={handleKey} disabled={disabled} className={inputClasses.join(" ")} {...elementConfig} value={value} onChange={changed} />;
      break;
    default:
      return defaultInputDesable ? "" : (inputElement = <input onKeyDown={handleKey} className={inputClasses.join(" ")} {...elementConfig} value={value} onChange={changed} />);
  }
  return (
    <div className={"Input"} style={{ display }}>
      <label className={"Label"}>
        {label}
        {shouldValidate?.required ? <span style={{ color: validStyle }}>{" * "}</span> : ""}
      </label>
      <div style={{ width: "100%" }}>
        <div style={{ width: "100%", display: "flex" }}>{inputElement}</div>
        <span className="input-alert-validation">{titleValidity}</span>
      </div>
    </div>
  );
});

export default Inputs;
