import React, { useState } from "react";
import { Dropdown } from "react-bootstrap";
const DropdownBoot = (props) => {
  const { accepted, dropDownData, value, className, checkSubmited, disabled } = props;

  let index = dropDownData && dropDownData.findIndex((d) => d.value === value);
  let propsVal;

  if (index >= 0) propsVal = dropDownData[index].title;
  const [Title, setTitle] = useState("");
  const clickedElement = (value, title) => {
    if (value) accepted(value, title);
    if (title) setTitle(title);
  };
  return (
    <Dropdown>
      <Dropdown.Toggle variant="success" id="dropdown-basic">
        {Title ? Title : propsVal ? propsVal : value ? value : "انتخاب نمایید  "}
      </Dropdown.Toggle>

      <Dropdown.Menu>
        {dropDownData &&
          dropDownData.map((info, index) => {
            // ////console.log({ value, infoval: info });

            return (
              <Dropdown.Item key={index + "boot"} active={value === info.title} onClick={() => clickedElement(info.value, info.title)}>
                {info.title}
              </Dropdown.Item>
            );
          })}
      </Dropdown.Menu>
    </Dropdown>
  );
};

export default DropdownBoot;
