import React, { useEffect, useState, memo } from "react";
const DateInput = memo((props) => {
  const { checkSubmited, onKeyDown, disabled, className, elementConfig, value, onChange } = props;
  const changedDate = (event) => {
    let name = event.currentTarget.name;
    let value = event.currentTarget.value;

    const valid = ["month", "day"];
    // if (value < 10 && value.length < 2 && valid.includes(name)) {
    //   event.currentTarget.value = 0 + value;
    // } else if (value < 10 == 0 && value.length === 2 && value.charAt(0) == 0 && valid.includes(name)) {
    //   event.currentTarget.value = value.slice(0, 0);
    // }

    onChange(event, name);
  };

  return (
    <div className="date-input-entry-data">
      <div>
        <label>سال</label>
        <input maxLength="4" name={"year"} value={value.year} onChange={changedDate} className={className} onKeyDown={onKeyDown} />
      </div>
      <div>
        <label>ماه</label>
        <input maxLength={2} name={"month"} value={value.month} onChange={changedDate} className={className} onKeyDown={onKeyDown} />
      </div>
      <div>
        <label>روز</label>
        <input maxLength={2} name={"day"} value={value.day} onChange={changedDate} className={className} onKeyDown={onKeyDown} />
      </div>
    </div>
  );
});

export default DateInput;

// const [state, setState] = useState({ year: "1300", month: "0", day: "0" });
// useEffect(() => {
//   if (value) changeValue();
// }, [value]);

// const changeValue = () => {
//   setState({ year: value.split("/")[0], month: value.split("/")[1], day: value.split("/")[2] });
// };
// const onChangeElement = (event) => {
//   let name = event.currentTarget.name;
//   let value = event.currentTarget.value;
//   let newState = { ...state };
//   let patt1 = /[0-9]/g;
//   let result = value.match(patt1);
//   if (result) newState[name] = value;
//   setState({ ...state, [name]: value });
//   if (!Object.values(state).includes("")) ////console.log("etelaat poor ast");
//   event.currentTarget.value = newState.year + "/" + newState.month + "/" + newState.day;
//   onChange(event);
// };
