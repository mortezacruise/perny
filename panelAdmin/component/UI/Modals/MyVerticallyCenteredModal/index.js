import React from "react";
import { Modal, Button } from "react-bootstrap";
const MyVerticallyCenteredModal = (props) => {
  const { status, setStatus, onChange, title, onSubmit, acceptedBtn, cancelledBtn, size, acceptedDisabled } = props;
  const submitModal = (e) => {
    setStatus(false);
    onSubmit(e);
  };
  return (
    <Modal
      // {...props}
      size={size}
      style={{ direction: "rtl" }}
      aria-labelledby="contained-modal-title-vcenter"
      centered
      show={status}
      onHide={submitModal}
      animation={true}
    >
      <Modal.Header closeButton>
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>{props.children}</Modal.Body>
      <Modal.Footer>
        {cancelledBtn ? (
          <Button variant="secondary" onClick={() => setStatus(false)}>
            {cancelledBtn}
          </Button>
        ) : (
          ""
        )}
        {acceptedBtn ? (
          <Button disabled={acceptedDisabled} variant="primary" onClick={submitModal}>
            {acceptedBtn}
          </Button>
        ) : (
          ""
        )}
      </Modal.Footer>
    </Modal>
  );
};

export default MyVerticallyCenteredModal;
