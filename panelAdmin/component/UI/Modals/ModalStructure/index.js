import React from "react";
import ModalBox from "../ModalBox";
import ModalTrueFalse from "../ModalTrueFalse";

const ModalStructure = (props) => {
  const { modalDetails, onHideModal, reqApiRemove, modalRequest, loading } = props;

  return (
    <div style={{ position: "absolute" }} className={modalDetails.kindOf === "component" ? "width80" : ""}>
      <ModalBox onHideModal={onHideModal} showModal={modalDetails.show}>
        {modalDetails.kindOf === "question" && <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={modalRequest} loading={loading} />}
        {props.children}
      </ModalBox>
    </div>
  );
};

export default ModalStructure;
