import React, { useState, useEffect } from "react";
import AddNewSubjectModal from "./AddNewSubjectModal";
import BackgrandCover from "../../BackgrandCover";

const ModalInput = (props) => {
  const { showModal, inputData, onCancelModal, subTitle, acceptedTitle, canceledTitle, type } = props;
  const [data, setData] = useState();
  const [fileValue, setFileValue] = useState("");
  const [modalHide, setModalHide] = useState(true);

  useEffect(() => {
    if (showModal) {
      setModalHide(false);
    } else if (!showModal) {
    }
  }, [showModal]);
  const _handelOnchange = (e) => {
    let value = e.currentTarget.value;
    let type = e.currentTarget.type;
    //////console.log({ value });

    if (type === "file") {
      let files = e.currentTarget.files[0];

      setData(files);
      setFileValue(value);
    } else {
      setData(value);
    }
  };

  const returnData = () => {
    if (data) {
      inputData(data);
    }
    onCancelModal();
  };
  const endAnimation = () => {
    if (!showModal) {
      onCancelModal();
      setModalHide(true);
      setData();
      setFileValue();
    }
  };
  //////console.log({ data });
  return (
    <div className={"modal_container-box"} style={{ display: modalHide ? "none" : "" }}>
      <AddNewSubjectModal onCancel={onCancelModal} onChange={_handelOnchange} value={data} fileValue={fileValue} returns={returnData} subTitle={subTitle} acceptedTitle={acceptedTitle} canceledTitle={canceledTitle} type={type} showModal={showModal} endAnimation={endAnimation} />
      <BackgrandCover onClick={onCancelModal} fadeIn={!modalHide} />
    </div>
  );
};

export default ModalInput;
