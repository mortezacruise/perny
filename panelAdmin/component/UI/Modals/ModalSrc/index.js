import React, { useState, useEffect } from "react";
import VideoPlayer from "../../../components/VideoPlayer";
import AudioPlayer from "../../../components/AudioPlayer";
import BackgrandCover from "../../BackgrandCover";
const ModalSrc = (props) => {
  const [modalHide, setModalHide] = useState(true);
  const { showModalSource, played, onHideModalSource } = props;
  ////console.log({ played });

  useEffect(() => {
    if (showModalSource) {
      setModalHide(false);
    }
  }, [showModalSource]);
  const endAnimation = () => {
    if (!showModalSource) {
      onHideModalSource();
      setModalHide(true);
    }
  };
  return (
    <div className={"modal_container-box"} style={{ display: modalHide ? "none" : "" }}>
      <div onAnimationEnd={endAnimation} id="subjectModal" className={showModalSource ? "fadeIn" : "fadeOut"}>
        {played.name === "VIDEO" && <VideoPlayer src={played.src} />}
        {played.name === "VOICE" && <AudioPlayer src={played.src} />}
      </div>
      <BackgrandCover onClick={onHideModalSource} fadeIn={!modalHide} />
    </div>
  );
};

export default ModalSrc;
