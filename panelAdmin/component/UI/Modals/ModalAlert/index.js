import React, { useEffect, useState } from "react";
import "./index.css";
import icError from "../../../assets/img/Icon/cancel.png";
import icSubmit from "../../../assets/img/Icon/acceptedGreen.png";

const ModalAlert = ({
  title,
  subtitle,
  isError,
  onSubmit,
  showModalAlert,
  onHideModalAlert
}) => {
  const [modalHide, setModalHide] = useState(true);
  const onSubmitClick = () => {
    onSubmit();
  };
  useEffect(() => {
    if (showModalAlert) {
      setModalHide(false);
    }
  }, [showModalAlert]);
  const endAnimation = () => {
    if (!showModalAlert) {
      onHideModalAlert();
      setModalHide(true);
    }
  };
  return (
    <div
      style={{ display: modalHide ? "none" : "" }}
      className={"modalContainer"}
    >
      <div
        onAnimationEnd={endAnimation}
        className={` modalBox ${showModalAlert ? "fadeIn" : "fadeOut"} `}
      >
        <img
          className={isError ? "icError" : "icSubmit"}
          src={isError ? icError : icSubmit}
          alt={"ModalIcon"}
        />
        <h2>{title}</h2>
        <h3>{subtitle}</h3>
        <button onClick={onSubmitClick}>تایید</button>
      </div>
    </div>
  );
};

export default ModalAlert;
