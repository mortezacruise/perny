import React, { useRef } from "react";
const InputVsIcon = (props) => {
  const { icon, placeholder, name, onChange, inputRef, dir } = props;

  return (
    <div style={{ direction: dir }} className="input-icon-wrapper centerAll">
      <input name={name} placeholder={placeholder} ref={inputRef} onChange={onChange} autoComplete="off" />
      <i className={icon} />
    </div>
  );
};

export default InputVsIcon;
