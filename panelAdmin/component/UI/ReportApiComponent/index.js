import React from "react";

const ReportApiComponent = ({ title }) => {
  return (
    <div className="Report-Api-Component-Wrapper centerAll">
      <div className="centerAll">
        <span>{title}</span>
      </div>
    </div>
  );
};

export default ReportApiComponent;
