import React from "react";

const MenuTitle = ({ title }) => {
	return (
		<li className="side-header change-position">
			<h6>{title}</h6>
		</li>
	);
};

export default MenuTitle;
