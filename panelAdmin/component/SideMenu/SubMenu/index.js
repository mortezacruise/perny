import React, { useRef, useEffect } from "react";
import Link from "next/link";

const SubMenu = ({
  menu,
  setMenuTitle,
  showLi,
  selectedMenu,
  windowLocation,
}) => {
  const location = windowLocation;
  const sideMenuLi = useRef(null);
  const onSubMenuClicked = (subMenu, menu) => {
    setMenuTitle(menu.menuTitle);
  };
  let liWidth = 0;
  if (sideMenuLi.current) liWidth = sideMenuLi.current.clientHeight;

  return (
    <ul
      style={{
        height:
          showLi === menu.menuTitle ? menu.subMenu.length * liWidth + "px" : "",
      }}
      className={`side-child-navigation transition0-3 ${
        showLi === menu.menuTitle && menu.subMenu.length ? "showIn" : "showOut"
      }`}
    >
      {menu.subMenu.map((child, i) => (
        <li
          ref={sideMenuLi}
          className={`side-iteme`}
          key={"subMenu-" + i}
          onClick={() => onSubMenuClicked(child, menu)}
        >
          <Link href={child.route} as={child.route}>
            <a
              className={`side-link side-child-ling ${
                child.route === selectedMenu || location.includes(child.route)
                  ? "activedSideChild"
                  : ""
              } `}
              id="sideChildTitle"
            >
              {child.title}
            </a>
          </Link>
        </li>
      ))}
    </ul>
  );
};

export default SubMenu;
