import React, { useState, Fragment } from "react";
import "./index.scss";
// import dictionary from "../../../util/dictionary";
const DetailsArrayCheckBox = ({ Info, sendNewVal, fieldName, questions, label, toastify }) => {
  const [state, setState] = useState({
    change: false,
    Info: [...Info],
  });
  ////console.log({ Info });

  const _handelEdit = () => {
    setState((prev) => ({
      ...prev,
      change: !state.change,
      Info: [...Info],
    }));
  };
  const _handelSendChanged = async () => {
    const data = { fieldChange: fieldName, newValue: state.Info };
    sendNewVal(data);

    _handelEdit();
  };
  const _handelonClick = (param, index) => {
    ////console.log({ param, index });
    let stateInfo = state.Info;
    stateInfo.includes(param)
      ? (stateInfo = stateInfo.filter((info, infoIndex) => {
          if (info !== param) return info;
        }))
      : stateInfo.push(param);
    setState({ ...state, Info: stateInfo });
  };

  return (
    <React.Fragment>
      <div className="card-details-row">
        <div className="about-title">
          <span>{label} :</span> {state.change ? "" : <i onClick={_handelEdit} className=" icon-pencil transition0-2 rotate-84"></i>}
          {state.change ? (
            <div className="btns-container">
              <a className="btns btns-success" onClick={_handelSendChanged}>
                {" "}
                ثبت{" "}
              </a>
              <a className="btns btns-warning" onClick={_handelEdit}>
                {" "}
                لغو{" "}
              </a>
            </div>
          ) : (
            ""
          )}
        </div>
        {state.change ? (
          <div className="checked-row" style={{ display: "flex" }}>
            {questions.map((ques, index) => {
              return (
                <div key={index} className="centerAll">
                  {/* <label>{dictionary(ques)} </label> */}
                  <input type="checkbox" onClick={() => _handelonClick(ques, index)} checked={state.Info.includes(ques)} value={state.Info ? "تایید شده" : "بسته شده"} />
                </div>
              );
            })}
            {/* <div className="centerAll">
              <label>{questions.question2} </label>
              <input type="checkbox" onClick={() => _handelonClick(false)} checked={!state.Info ? true : false} value={state.Info ? "تایید شده" : "بسته شده"} />
            </div> */}
          </div>
        ) : (
          <div className={"centerAll"} style={{ justifyContent: "start" }}>
            {Info.map((info, index) => {
              return <p>{/* {dictionary(info)} {index < info.length - 1 ? "  " : ""} */}</p>;
            })}
          </div>
        )}
      </div>
    </React.Fragment>
  );
};

export default DetailsArrayCheckBox;
