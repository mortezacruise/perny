import React, { useState, useEffect, useRef } from "react";
import profileImage from "../../../assets/Images/icons/user.png";
// import "./index.scss";
import Link from "next/link";
// import Cookies from "js-cookie";

const HeaderProfile = () => {
  const [state, setState] = useState({
    showModal: false,
    clickedComponent: false,
  });
  const wrapperRef = useRef(null);

  const showModalprofile = () => {
    // let noting = ;
    setState((prev) => ({
      ...prev,
      showModal: !state.showModal,
      clickedComponent: true,
    }));
  };
  // ////console.log({ state: state.showModal });

  // const logOut = () => Cookies.remove("RimtalToken");
  const adminTitleModal = [
    {
      title: "پروفایل",
      iconClass: "fas fa-user",
      href: "#",
      onClick: null,
    },
    {
      title: "پیام ها",
      iconClass: "fas fa-envelope",
      href: "#",
      value: "",
      onClick: null,
    },
    {
      title: "تنظیمات",
      iconClass: "fas fa-cog",
      href: "#",
      onClick: null,
    },
    { title: "خروج", iconClass: " fas fa-sign-out-alt", href: "#", onClick: null },
  ];

  const handleClickOutside = (event) => {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
      showModalprofile();
    }
  };
  useEffect(() => {
    if (state.showModal) {
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }
  });
  const adminTitleModal_map = (
    <ul className={`profile-modal-container ${state.showModal ? "actived" : state.clickedComponent ? "deActive" : "showOutStatic"}`}>
      {adminTitleModal.map((admin, index) => {
        return (
          <li onClick={admin.onClick} key={index}>
            <i className={admin.iconClass}></i>

            <Link href={admin.href} as={admin.href}>
              <a>{admin.title}</a>
            </Link>
            {admin.value ? <span className="show-modal-icon-value">{admin.value}</span> : ""}
          </li>
        );
      })}
    </ul>
  );
  return (
    <ul onClick={showModalprofile} ref={wrapperRef} className="panel-navbar-profile ">
      <li className="pointer hoverColorblack normalTransition">
        <div className="centerAll">
          <i className="fas fa-angle-down"></i>
        </div>
        <div className="admin-profile-name  icon-up-dir ">
          <span>ادمین</span>{" "}
        </div>

        <div className="admin-profile-image">
          <img src={profileImage} alt="profile" />
        </div>
      </li>

      {adminTitleModal_map}
    </ul>
  );
};

export default HeaderProfile;
