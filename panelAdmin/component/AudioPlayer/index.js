import React from "react";
import "./index.scss";
const AudioPlayer = ({ src, type }) => {
  ////console.log({ src, type });
  return (
    <div className="show-audio-src-container justifyCenter">
      <audio controls>
        <source src={src} type={type ? type : "audio/mpeg"} />
      </audio>
    </div>
  );
};

export default AudioPlayer;
