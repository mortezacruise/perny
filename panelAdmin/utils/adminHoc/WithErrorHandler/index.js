import React, { useState, useEffect } from "react";
import axios from "../../../api/axios-orders";
import checkError from "./checkError";
import toastify from "../../toastify";

const WithErrorHandler = (WrappedComponent) => {
  return (props) => {
    const [error, setError] = useState(null);
    const reqInterceptor = axios.interceptors.request.use((req) => {
      // console.log({ req });
      setError(null);
      return req;
    });
    const resInterceptor = axios.interceptors.response.use(
      (res) => {
        // console.log({ res });
        return res;
      },
      (err) => {
        setError(err);
      }
    );
    useEffect(() => {
      return () => {
        axios.interceptors.request.eject(reqInterceptor);
        axios.interceptors.response.eject(resInterceptor);
      };
    }, [reqInterceptor, resInterceptor]);
    console.log({ error });
    // const errorDisable = "/admin/owner/s//";
    // console.log(errorDisable.search(error?.config?.url));
    const test = error?.config?.url.split("/");
    // if (test) console.log(test[3], test[4], test[5]);
    console.log({ test });

    if (error)
      if (test[3] !== "" && !test[4])
        if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
        else toastify(checkError({ errorCode: error?.response?.data?.CODE, statusCode: error?.response?.status }), "error");

    return (
      <>
        <WrappedComponent {...props} />
      </>
    );
  };
};

export default WithErrorHandler;
