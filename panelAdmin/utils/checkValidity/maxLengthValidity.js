const maxLengthValidity = ({ array, beforeValue, value, rules }) => {
  let maxLength = true;

  if (array) beforeValue.map((val) => (maxLength = val.length >= rules.maxLength && maxLength));
  else maxLength = value.length <= rules.maxLength && maxLength;

  return maxLength;
};
export default maxLengthValidity;
