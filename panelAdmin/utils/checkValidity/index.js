import minLengthValidity from "./minLengthValidity";
import maxLengthValidity from "./maxLengthValidity";
import numberValidity from "./numberValidity";

export const checkValidity = (value, rules, array, beforeValue) => {
  let isValid = true;
  let errorTitle = false;
  let object = true;
  let checkValid = true;
  // console.log({ moojValid: { value, rules, array, beforeValue } });
  // return { isValid, errorTitle };
  // ////console.log({ moooojValidtypeof: typeof value });
  if (!rules.required) return { isValid, errorTitle };
  if (!rules) {
    return { isValid, errorTitle };
  }
  if (array) {
    isValid = beforeValue.length ? true : false;
    ////console.log({ mooj: beforeValue.length ? true : false });

    if (!isValid) errorTitle = "خالی بودن فیلد مجاز نمی باشد";
  } else {
    if (rules.required) {
      if (typeof value === "object") {
        for (const key in value) object = value[key].trim() !== "" && object;
        if (!object) errorTitle = "خالی بودن فیلد مجاز نمی باشد";
        isValid = object && isValid;
      } else {
        isValid = (value?.trim() || value) !== "" && isValid;
        errorTitle = (value?.trim() || value) === "" && ".خالی بودن فیلد مجاز نمی باشد";
      }
    }
  }
  if (isValid) {
    if (rules.minLength) {
      checkValid = minLengthValidity({ array, beforeValue, value, rules });
      ////console.log({ checkValid });
      if (!checkValid) errorTitle = ` کمترین مقدار مجاز فیلد ${rules.minLength} کلمه یا رقم می باشد `;
      isValid = isValid && checkValid;
    }

    if (rules.maxLength) {
      checkValid = maxLengthValidity({ array, beforeValue, value, rules });
      if (!checkValid) errorTitle = ` بیشترین مقدار مجاز فیلد ${rules.maxLength} کلمه یا رقم می باشد `;
      isValid = isValid && checkValid;
    }
    if (rules.isNumeric) {
      checkValid = numberValidity({ array, beforeValue, value });
      if (!checkValid) errorTitle = "فقط شماره مجاز می باشد";

      isValid = isValid && checkValid;
    }
    if (rules.isEmail) {
      const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      isValid = pattern.test(value) && isValid;
    }

    if (rules.isPhone) {
      const pattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
      // /^0\d{2,3}-\d{8}|\d{8}$/ regex(/^0\d{2,3}-\d{8}|\d{8}$/)
      isValid = pattern.test(value) && isValid;
    }
    if (rules.isMobile) {
      const pattern = /^09[\w]{9}$/;

      if (array) beforeValue.map((val) => (isValid = pattern.test(val) && isValid));
      // const pattern = /[0,9]{2}\d{9}/g;
      // const pattern = /^(+98|0)?9\d{9}$/;
      else isValid = pattern.test(value) && isValid;
    }

    if (rules.isEn) {
      const pattern = /^[a-zA-Z0-9$@$!%*?&#^-_. +]+$/;
      if (array) beforeValue.map((val) => (isValid = pattern.test(val) && isValid));
      else isValid = pattern.test(value) && isValid;
    }
    if (rules.isFa) {
      const pattern = /^[\u0600-\u06FF\s]+$/;
      if (array) beforeValue.map((val) => (isValid = pattern.test(val) && isValid));
      else isValid = pattern.test(value) && isValid;
    }
  }

  // //////console.log({ mooojisValid: isValid });

  return { isValid, errorTitle };
};
