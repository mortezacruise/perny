const objectValidity = ({ array, beforeValue, value, rules }) => {
  let minLength = true;

  if (array) beforeValue.map((val) => (minLength = val.length >= rules.minLength && minLength));
  else minLength = value.length >= rules.minLength && minLength;

  return minLength;
};

export default objectValidity;
