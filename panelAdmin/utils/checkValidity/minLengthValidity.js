const minLengthValidity = ({ array, beforeValue, value, rules }) => {
  let minLength = true;

  if (array) {
    beforeValue.map((val) => {
      ////console.log({ minLengthVal: val });

      return (minLength = val.length >= rules.minLength && minLength);
    });
  } else minLength = value.length >= rules.minLength && minLength;
  ////console.log({ minLength, array, beforeValue, value, rules });

  return minLength;
};

export default minLengthValidity;
