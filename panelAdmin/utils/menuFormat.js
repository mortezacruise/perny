import panelAdmin from "..";
import values from "../values/index";

const menuFormat = [
  {
    title: "عمومی",
    menus: [
      {
        route: values.routes.GS_ADMIN_DASHBOARD,
        menuTitle: values.strings.DASHBOARD,
        menuIconImg: false,
        menuIconClass: "fas fa-tachometer-slowest",
        subMenu: [
          // { title: "SubDashboard", route: "/dashboard1" },
          // { title: "SubDashboard", route: "/dashboard2" }
        ],
      },
    ],
  },
  {
    title: "کاربردی",
    menus: [
      {
        route: false,
        menuTitle: values.strings.GALLERY,
        menuIconImg: false,
        menuIconClass: "fad fa-images",
        subMenu: [
          {
            title: values.strings.SEE_GALLERIES,
            route: values.routes.GS_ADMIN_GALLERY,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.strings.CATEGORY,
        menuIconImg: false,
        menuIconClass: "fad fa-cubes",
        subMenu: [
          {
            title: values.strings.SEE_CATEGORIES,
            route: values.routes.GS_ADMIN_CATEGORY,
          },
          {
            title: values.strings.ADD_CATEGORY,
            route: values.routes.GS_ADMIN_ADD_CATEGORY,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.strings.PRODUCT,
        menuIconImg: false,
        menuIconClass: "fad fa-store",
        subMenu: [
          {
            title: values.strings.SEE_PRODUCTS,
            route: values.routes.GS_ADMIN_PRODUCT,
          },
          {
            title: values.strings.ADD_PRODUCT,
            route: values.routes.GS_ADMIN_ADD_PRODUCT,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.strings.SLIDER,
        menuIconImg: false,
        menuIconClass: "fas fa-presentation",
        subMenu: [
          {
            title: values.strings.SEE_SLIDERS,
            route: values.routes.GS_ADMIN_SLIDER,
          },
          {
            title: values.strings.ADD_SLIDER,
            route: values.routes.GS_ADMIN_ADD_SLIDER,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.strings.BANNER,
        menuIconImg: false,
        menuIconClass: "far fa-archive",
        subMenu: [
          {
            title: values.strings.SEE_BANNERS,
            route: values.routes.GS_ADMIN_BANNER,
          },
          {
            title: values.strings.ADD_BANNER,
            route: values.routes.GS_ADMIN_ADD_BANNER,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.strings.ARTICLES,
        menuIconImg: false,
        menuIconClass: "fad fa-sticky-note",
        subMenu: [
          {
            title: values.strings.BLOG_CATEGORY,
            route: values.routes.GS_ADMIN_SEE_BLOG_CATEGORY,
          },
          {
            title: values.strings.SHOW_ARTICLES,
            route: values.routes.GS_ADMIN_SEE_ARTICLE,
          },
        ],
      },
      {
        route: values.routes.GS_ADMIN_COMMENT,
        menuTitle: values.strings.COMMENT,
        menuIconImg: false,
        menuIconClass: "fad fa-comments",
        subMenu: [],
      },
    ],
  },
];

export default menuFormat;
