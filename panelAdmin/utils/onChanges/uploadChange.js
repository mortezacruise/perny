import React from "react";
import { post } from "../../api";
import toastify from "../toastify";
import voiceValid from "./validUpload/voicevalid";

const uploadChange = async (props) => {
  const { event, setLoading, imageType, setState, valid, fileName, dispatch } = props;
  ////console.log({ eventUpload: event });

  let files = event.files[0];
  ////console.log({ imageType, fileName });

  let returnData = false;
  if (files) {
    switch (valid) {
      case "image":
        if (files.type.includes("image")) {
          if (fileName) {
            if (
              // dispatch(sagaActions.uploadImageData({ data: files, imageType, fileName }))
              await post.imageUpload(files, setLoading, setState, fileName)
            )
              returnData = fileName;
          } else {
            toastify("لطفا اطلاعات قبلی را پر کنید", "error");
          }
        }
        break;
      case "video":
        if (files.type.includes("video")) returnData = await post.videoUpload(files, setLoading, imageType, setState);
        break;
      case "voice":
        if (voiceValid(files.type)) {
          if (fileName) {
            if (await post.voiceUpload(files, setLoading, imageType, setState, fileName)) returnData = fileName;
          } else {
            toastify("لطفا اطلاعات قبلی را پر کنید", "error");
          }
        }
        break;
      default:
        toastify("فایل شما نباید " + files.type + " باشد", "error");
        break;
    }
  }

  ////console.log({ files: files, returnData, fileName });

  if (!returnData && files && (valid === "image" ? fileName : true)) toastify("فایل شما نباید " + files.type + " باشد", "error");

  setState((prev) => ({
    ...prev,
    progressPercentImage: null,
    progressPercentVideo: null,
    progressPercentSongs: null,
  }));
  return returnData;
};
export default uploadChange;
