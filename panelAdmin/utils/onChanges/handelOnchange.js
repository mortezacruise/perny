const handelOnchange = async ({ event, data, setData, setState, setLoading, imageType, validName, checkValidity, updateObject, uploadChange, fileName }) => {
  let changeValid,
    updatedForm,
    updatedFormElement = {};
  let formIsValid = true;
  let value = data.Form[event.name].value;
  let update = { ...value };
  let eventValue = event.value;
  let typeofData = typeof value;
  let isArray, isObject, isString;
  typeofData === "object" && value != undefined ? (value && value.length >= 0 ? (isArray = true) : (isObject = true)) : (isString = true);
  const remove = (index) => value.splice(index, 1)[0];
  const push = (val, newVal) => (newVal != undefined ? val.push(newVal) : "");
  if (event.type === "file") {
    const uploadFile = await uploadChange({
      event,
      setLoading,
      imageType,
      setState,
      valid: data.Form[event.name].kindOf,
      fileName,
    });
    if (uploadFile) {
      if (isArray) value.includes(uploadFile) ? remove(value.findIndex((d) => d === uploadFile)) : push(value, uploadFile);
      else value = uploadFile;
    } else return;
  } else if (isArray) value.includes(eventValue) ? remove(value.findIndex((d) => d === eventValue)) : push(value, eventValue);
  else if (isObject) {
    if (event.child) {
      value[event.child] = eventValue;
    } else value = eventValue;
  } else if (isString) value = eventValue;

  let checkValidValue;
  if (typeofData === "object") checkValidValue = eventValue;
  else checkValidValue = value;
  // ////console.log({ moooooj: checkValidity(checkValidValue, data.Form[event.name].validation, isArray).isValid });
  console.log({ checkValidValue, eventValue, value, typeofData: typeofData, checkEvent: data.Form[event.name] });

  updatedFormElement[event.name] = updateObject(data.Form[event.name], {
    value: value,
    valid: data.Form[event.name].validation.required ? (typeof event.value === "object" ? true : checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).isValid) : true,
    touched: true,
    block: false,
    titleValidity: checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).errorTitle,
  });

  if (validName) {
    changeValid = updateObject(data.Form[validName], { block: true });
    updatedForm = updateObject(data.Form, { ...updatedFormElement, [validName]: changeValid });
  } else updatedForm = updateObject(data.Form, { ...updatedFormElement });

  console.log({ updatedFormElement, updatedForm });

  for (let name in updatedForm) formIsValid = (updatedForm[name].valid && formIsValid) || (updatedForm[name].block && formIsValid);

  return setData({ Form: updatedForm, formIsValid: formIsValid });
  // setData({ Form: updatedForm, formIsValid: formIsValid });
};
export default handelOnchange;
