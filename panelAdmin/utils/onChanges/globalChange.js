import { checkValidity } from "../checkValidity";
import updateObject from "../updateObject";
import uploadChange from "./uploadChange";
import handelOnchange from "./handelOnchange";
import arrayOnchange from "./arrayOnchange";

const globalChange = async (props) => {
  const { event, data, setData, setState, setLoading, imageType, validName, fileName, dispatch } = props;
  let typeCheck = typeof event;
  // ////console.log(typeCheck === "object" && event.length > 0);
  ////console.log({ event });

  if (typeCheck === "object" && event.length > 0)
    return arrayOnchange({
      event,
      data,
      setData,
      setState,
      setLoading,
      imageType,
      validName,
      checkValidity,
      updateObject,
      uploadChange,
      fileName,
      dispatch,
    });
  else if (typeCheck === "object")
    return handelOnchange({
      event,
      data,
      setData,
      setState,
      setLoading,
      imageType,
      validName,
      checkValidity,
      updateObject,
      uploadChange,
      fileName,
      dispatch,
    });
};
export default globalChange;
