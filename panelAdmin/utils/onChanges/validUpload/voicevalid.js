let formats = ["mp3", "voice", "audio"];
const voiceValid = (type) => {
  let valid = formats.map((format) => {
    if (type.includes(format)) return true;
    else return false;
  });
  let finalValid = valid.includes(true) ? true : false;
  return finalValid;
};
export default voiceValid;
