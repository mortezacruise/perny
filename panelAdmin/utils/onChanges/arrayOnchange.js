const arrayOnchange = async (props) => {
  const { event: e, data, setData, setState, setLoading, imageType, validName, checkValidity, updateObject, uploadChange } = props;
  // ////console.log({ validName });
  let changeValid,
    updatedForm,
    updatedFormElement = {};
  e.map(async (event) => {
    let value = data.Form[event.name].value;
    let update = { ...value };
    let eventValue = event.value;
    let typeofData = typeof value;
    let isArray, isObject, isString;
    typeofData === "object" && value != null ? (value.length >= 0 ? (isArray = true) : (isObject = true)) : (isString = true);
    const remove = (index) => value.splice(index, 1)[0];
    const push = (val, newVal) => (newVal != undefined ? val.push(newVal) : "");
    // ////console.log({ eventValue, value });
    // if (event.type === "file") {
    //   const uploadFile = await uploadChange(event, setLoading, imageType, setState);
    //   if (uploadFile) {
    //     if (isArray) value.includes(uploadFile) ? remove(value.findIndex((d) => d === uploadFile)) : push(value, uploadFile);
    //     else value = uploadFile;
    //   } else return;
    // } else if (isArray) value.includes(eventValue) ? remove(value.findIndex((d) => d === eventValue)) : push(value, eventValue);
    // else if (isObject) {
    //   value = eventValue;
    //   if (event.child) value = update[event.child] = eventValue;
    // } else if (isString)
    value = eventValue;

    let checkValidValue;
    if (typeofData === "object") checkValidValue = eventValue;
    else checkValidValue = value;

    updatedFormElement[event.name] = updateObject(data.Form[event.name], {
      value: value,
      valid: data.Form[event.name].validation.required ? (typeof event.value === "object" ? true : checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).isValid) : true,

      touched: true,
      titleValidity: checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).errorTitle,
    });
    if (validName) {
      changeValid = updateObject(data.Form[validName], { valid: true });
      updatedForm = updateObject(data.Form, { ...updatedFormElement, [validName]: changeValid });
    } else updatedForm = updateObject(data.Form, { ...updatedFormElement });

    console.log({ updatedFormElement, updatedForm });

    let formIsValid = false;
    for (let name in updatedForm) formIsValid = updatedForm[name].valid && formIsValid;

    return setData({ Form: updatedForm, formIsValid: formIsValid });
  });
};
export default arrayOnchange;
