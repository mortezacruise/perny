function timeNow(date) {
  var currentTime = date || new Date();
  var hours = currentTime.getHours();
  var minutes = currentTime.getMinutes();
  var seconds = currentTime.getSeconds();
  var meridiem = "AM";
  var isbHours = 5 + hours;
  var romeHours = 1 + hours;
  var sydneyHours = 11 + hours;

  if (seconds < 10) {
    seconds = "0" + seconds;
  }

  if (minutes < 10) {
    minutes = "0" + minutes;
  }

  if (hours < 10) {
    hours = "0" + hours;
  }

  if (sydneyHours < 10) {
    sydneyHours = "0" + sydneyHours;
  }

  if (hours > 12) {
    hours = hours;
    // meridiem = "PM";
  }

  //   if (isbHours > 12) {
  //     isbHours = isbHours - 12;
  //     meridiem = "PM";
  //   }

  //   if (romeHours > 12) {
  //     romeHours = romeHours - 12;
  //     meridiem = "PM";
  //   }

  //   if (sydneyHours > 12) {
  //     sydneyHours = sydneyHours - 12;
  //     meridiem = "PM";
  //   }

  let localClock;
  localClock = hours + ":" + minutes + ":" + seconds + " ";
  // localClock.innerText += "\n" + "Local Time";
  //   let isbClock;
  //   isbClock = isbHours + ":" + minutes + ":" + seconds + " " + meridiem;
  // isbClock.innerText += "\n" + "Islamabad Time";
  //   let sydneyClock;
  //   sydneyClock = sydneyHours + ":" + minutes + ":" + seconds + " " + meridiem;
  // sydneyClock.innerText += "\n" + "Sydney Time";
  //   let romeClock;
  //   romeClock = romeHours + ":" + minutes + ":" + seconds + " " + meridiem;
  // romeClock.innerText += "\n" + "Rome Time";
  return localClock;
}
// setInterval(timeNow, 1000);
export default timeNow;
