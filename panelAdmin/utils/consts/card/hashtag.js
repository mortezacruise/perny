import React from "react";
import panelAdmin from "../../..";

const hashtag = (data) => {
  const cardFormat = [];
  ////console.log({ data });

  for (let index in data) {
    let noEntries = panelAdmin.values.strings.NO_ENTRIES;
    let dataIndex = data[index];
    let description = dataIndex.titleFa ? dataIndex.titleFa : noEntries;
    let title = dataIndex.titleFa ? dataIndex.titleFa : noEntries;
    // let images = dataIndex.images ? dataIndex.images : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      // image: { value: images },
      body: [
        {
          right: [{ elementType: "text", value: title, style: { color: "black", fontSize: "0.67rem", fontWeight: "bold" } }],
        },
        {
          right: [
            {
              elementType: "text",
              value: description,
              title: description,
              style: { color: "#8c8181", fontSize: "0.6em", fontWeight: "500" },
            },
          ],
        },
      ],
    });
  }
  return cardFormat;
};

export default hashtag;
