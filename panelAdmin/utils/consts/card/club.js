import React, { Fragment } from "react";
import PanelString from "../../../value/PanelString";
// // import dictionary from "../../dictionary";

const Club = (data) => {
  const cardFormat = [];
  for (let index in data) {
    let title = data[index].title ? data[index].title : "";
    let membership = data[index].membership ? data[index].membership : [];
    let owner = data[index].owner ? data[index].owner : "";
    let ownerDistrict = owner.district ? owner.district : "";
    let category = data[index].category ? data[index].category : "";

    cardFormat.push({
      _id: data[index]._id,
      isActive: data[index].isActive,
      image: { value: data[index].slides[0] },
      body: [
        {
          right: [
            { elementType: "text", value: title, style: { color: "black", fontSize: "1.3em", fontWeight: "bold" } },
          ],
          left: [
            {
              elementType: "text",
              value: category.titleFa,
              style: { color: PanelString.color.GREEN, fontSize: "1em", fontWeight: "800" },
            },
          ],
        },
        {
          right: [
            {
              elementType: "text",
              value: owner.title,
              style: { color: PanelString.color.GRAY, fontSize: "1em", fontWeight: "500" },
            },
          ],
        },
        {
          right: [{ elementType: "district", value: ownerDistrict }],
          left: [
            {
              elementType: "text",
              value: membership.map((text, i) => (
                <Fragment>
                  {/* {dictionary(text)} */}
                  {i < membership.length - 1 ? "/" : ""}
                </Fragment>
              )),
              style: { color: PanelString.color.GRAY, fontSize: "1em", fontWeight: "500" },
            },
          ],
        },
      ],
    });
  }
  return cardFormat;
};

export default Club;
