import React from "react";
import userPng from "../../../assets/Images/icons/userIcon.png";
// import dictionary from "../../dictionary";
const admin = (data) => {
  let NotSelected = "انتخاب نشده";
  const cardFormat = [];
  for (let index in data) {
    let fullName = data[index].fullName ? data[index].fullName : NotSelected;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotSelected;
    let productsLength = data[index].products.length;
    let role = data[index].role ? data[index].role : NotSelected;
    let pubType = data[index].pubType ? data[index].pubType : NotSelected;
    cardFormat.push({
      _id: data[index]._id,
      isActive: data[index].isActive,
      image: { value: data[index].avatar ? data[index].avatar : userPng },
      body: [
        {
          right: [
            { elementType: "text", value: fullName, style: { color: "black", fontSize: "1.3em", fontWeight: "bold" } },
          ],
          left: [
            { elementType: "text", value: role, style: { color: "black", fontSize: "1.3em", fontWeight: "bold" } },
          ],
        },

        {
          right: [
            {
              elementType: "icon",
              value: productsLength,
              className: "icon-shopping-bag",
              direction: "ltr",
              style: { fontSize: "1.4em", fontWeight: "500" },
              iconStyle: { fontSize: "1.4em" },
            },
          ],
          left: [
            {
              elementType: "icon",
              value: phoneNumber,
              className: "icon-phone",
              direction: "ltr",
              style: { fontSize: "1em", fontWeight: "500" },
              iconStyle: { fontSize: "1.4em" },
            },
          ],
        },
        {
          right: [
            {
              elementType: "text",
              value: pubType,
              style: { fontSize: "1.4em", fontWeight: "500" },
              iconStyle: { fontSize: "1.4em" },
            },
          ],
        },
      ],
    });
  }
  return cardFormat;
};

export default admin;
