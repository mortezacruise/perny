import React from "react";

const product = (data) => {
  ////console.log({ productCard: data });

  const cardFormat = [];
  for (let index in data) {
    let NO_ENTERED = "وارد نشده";
    let dataIndex = data[index];

    let category = dataIndex.category ? dataIndex.category : "";
    let categoryName = category ? category.name : NO_ENTERED;
    let name = dataIndex.name ? dataIndex.name : NO_ENTERED;
    let unit = dataIndex.unit ? dataIndex.unit : NO_ENTERED;
    let weight = dataIndex.weight ? dataIndex.weight : NO_ENTERED;
    let realPrice = dataIndex.realPrice ? dataIndex.realPrice : NO_ENTERED;
    let newPrice = dataIndex.newPrice ? dataIndex.newPrice : NO_ENTERED;
    let image = dataIndex.image ? dataIndex.image : NO_ENTERED;

    cardFormat.push({
      _id: dataIndex._id,
      // isActive: dataIndex.isActive,
      image: { value: image },
      body: [
        {
          right: [{ elementType: "text", value: name, style: { color: "black", fontSize: "1.3em", fontWeight: "bold" } }],
        },
        { right: [{ elementType: "text", value: categoryName, title: categoryName, style: { color: "", fontSize: "0.9em", fontWeight: "500" } }] },

        {
          left: [{ elementType: "price", value: realPrice, direction: "ltr", style: { color: "red", fontSize: "", fontWeight: "" } }],
          right: [{ elementType: "price", value: newPrice, direction: "ltr", style: { color: "green", fontSize: "", fontWeight: "" } }],
        },
        {
          right: [{ elementType: "text", value: unit }],
          left: [{ elementType: "icon", value: weight, className: "", direction: "ltr", style: { fontSize: "1em", fontWeight: "500" }, iconStyle: { fontSize: "1.4em" } }],
        },
      ],
    });
  }
  return cardFormat;
};

export default product;
