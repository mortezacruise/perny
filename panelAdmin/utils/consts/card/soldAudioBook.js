import React from "react";
import PanelString from "../../../value/PanelString";

const soldAudioBook = (data) => {
  const cardFormat = [];
  for (let index in data) {
    let dataIndex = data[index];
    let audioBook = dataIndex.audioBook;
    let customer = dataIndex.customer;
    let title = audioBook.title ? audioBook.title : "";
    let description = audioBook.description ? audioBook.description : "";
    let price = audioBook.price ? audioBook.price : "";
    let category = audioBook.category ? audioBook.category : "";
    let categoryTitle = category.title ? category.title : "";
    let fullName = customer.fullName ? customer.fullName : "";
    let phoneNumber = customer.phoneNumber ? customer.phoneNumber : "";

    cardFormat.push({
      _id: audioBook._id,
      // isActive: audioBook.isActive,
      image: { value: audioBook.cover },
      body: [
        {
          right: [{ elementType: "text", value: title, style: { color: "black", fontSize: "1.3em", fontWeight: "bold" } }],
        },
        {
          right: [{ elementType: "text", value: description, title: description, style: { color: PanelString.color.GRAY, fontSize: "1em", fontWeight: "500" } }],
        },
        {
          right: [{ elementType: "text", value: categoryTitle }],
          left: [{ elementType: "price", value: price, direction: "ltr" }],
        },
        {
          right: [{ elementType: "text", value: fullName }],
          left: [{ elementType: "icon", value: phoneNumber, className: "icon-phone", direction: "ltr", style: { fontSize: "1em", fontWeight: "500" }, iconStyle: { fontSize: "1.4em" } }],
        },
      ],
    });
  }
  return cardFormat;
};

export default soldAudioBook;
