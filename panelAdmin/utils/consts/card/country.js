import React from "react";

const country = (data) => {
  const cardFormat = [];
  ////console.log({ data });

  for (let index in data) {
    let dataIndex = data[index];
    let description = dataIndex.titleFa ? dataIndex.titleFa : "";
    let price = dataIndex.price ? dataIndex.price : "";
    let title = dataIndex.titleFa ? dataIndex.titleFa : "";
    let images = data[index].flag ? data[index].flag : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: { value: images },
      body: [
        {
          right: [{ elementType: "text", value: title, style: { color: "black", fontSize: "0.67rem", fontWeight: "bold" } }],
        },
        {
          right: [
            {
              elementType: "text",
              value: description,
              title: description,
              style: { color: "#8c8181", fontSize: "0.6em", fontWeight: "500" },
            },
          ],
        },

        {
          left: [{ elementType: "price", value: price, direction: "ltr" }],
        },
      ],
    });
  }
  return cardFormat;
};

export default country;
