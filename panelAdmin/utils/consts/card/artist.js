const artist = (data, acceptedCard) => {
  const cardFormat = [];
  ////console.log({ insss: data });

  for (let index in data) {
    let dataIndex = data[index];
    let web = dataIndex.web ? dataIndex.web : "";
    let phone = dataIndex.phone ? dataIndex.phone : "";
    let title = dataIndex.nameFa ? dataIndex.nameFa : "";
    let description = dataIndex.descriptionFa ? dataIndex.descriptionFa : "";
    let images = dataIndex.avatar ? dataIndex.avatar : "";
    // ////console.log(images);

    cardFormat.push({
      _id: data[index]._id,
      isActive: data[index].isActive,
      image: { value: images },
      body: [
        {
          right: [{ elementType: "text", value: title, style: { color: "black", fontSize: "0.67rem", fontWeight: "bold" } }],
        },
        {
          right: [{ elementType: "text", value: description, style: { color: "#8c8181", fontSize: "0.67rem", fontWeight: "bold" } }],
        },
      ],
    });
  }
  return cardFormat;
};

export default artist;
