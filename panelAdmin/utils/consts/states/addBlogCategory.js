const addBlogCategory = {
  Form: {
    name: {
      label: "نام دسته  :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام دسته ",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addBlogCategory;
