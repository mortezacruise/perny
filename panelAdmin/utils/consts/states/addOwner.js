const addOwner = {
  Form: {
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    subTitle: {
      label: "توضیحات :",

      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات ",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    phoneNumber: {
      label: "تلفن همراه :",

      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "تلفن همراه",
      },
      value: "",
      validation: {
        required: true,
        minLength: 11,
        maxLength: 11,
        isNumeric: true,
        isMobile: true,
      },
      valid: false,
      touched: false,
    },
    phone: {
      label: "تلفن ثابت :",

      elementType: "inputPush",
      elementConfig: {
        placeholder: "تلفن ثابت",
      },
      validation: {
        required: true,
        minLength: 4,
        isNumeric: true,
      },
      value: [],
      valid: false,
      touched: false,
    },
    district: {
      label: "ناحیه :",

      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "ناحیه",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    address: {
      label: "آدرس :",

      elementType: "textarea",
      elementConfig: {
        placeholder: "آدرس",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    // rating: {
    //   label: "امتیاز :",
    //   elementType: "input",
    //   elementConfig: {
    //     type: "number",
    //     placeholder: "امتیاز"
    //   },
    //   value: "",
    //   validation: {
    //     minLength: 1,
    //     maxLength: 1,
    //     isNumeric: true,
    //     required: true
    //   },
    //   valid: false,
    //   touched: false
    // },
    thumbnail: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    coordinate: {
      label: "مختصات :",
      elementType: "parentInput",
      kindOf: "image",
      value: { lat: "", lng: "" },

      elementConfig: {
        type: "number",
        placeholder: "مختصات",
      },
      validation: {
        minLength: 1,
        isNumeric: true,
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addOwner;
