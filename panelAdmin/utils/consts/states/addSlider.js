const addSlider = {
  Form: {
    parentType: {
      label: " ",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نوع والد",
      },
      childValue: [
        { name: "دسته بندی", value: "Category" },
        { name: "محصولات", value: "Product" },
      ],
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },

    category: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    product: {
      label: "محصولات :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "محصولات",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addSlider;
