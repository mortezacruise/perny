const addGallery = {
  Form: {
    imageName: {
      label: "نام عکس :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام عکس",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    image: {
      label: " عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addGallery;
