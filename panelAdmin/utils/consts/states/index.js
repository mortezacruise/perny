import addCategory from "./addCategory";
import addProduct from "./addProduct";
import addGallery from "./addGallery";
import addBanner from "./addBanner";
import addSlider from "./addSlider";
import addBlogCategory from "./addBlogCategory";
import addArticle from "./addArticle";

const states = {
  addCategory,
  addProduct,
  addGallery,
  addBanner,
  addSlider,
  addBlogCategory,
  addArticle,
};
export default states;
