const addSong = {
  Form: {
    songName: {
      label: "نام آهنگ :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام آهنگ",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },

    song: {
      label: " آهنگ :",
      elementType: "inputFile",
      kindOf: "voice",
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addSong;
