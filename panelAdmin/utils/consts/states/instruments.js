const addInstruments = {
  Form: {
    titleFa: {
      label: "عنوان فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان فارسی",
      },
      value: "",
      validation: {
        required: true,
        isFa: true,
      },
      valid: false,
      touched: false,
    },
    titleEn: {
      label: "عنوان انگلیسی :",

      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " عنوان انگلیسی",
      },
      value: "",
      validation: {
        required: true,
        isEn: true,
      },
      valid: false,
      touched: false,
    },
    descriptionFa: {
      label: "توضیحات فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "توضیحات فارسی",
      },
      value: "",
      validation: {
        required: true,
        isFa: true,
      },
      valid: false,
      touched: false,
    },
    descriptionEn: {
      label: "توضیحات انگلیسی :",

      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: " توضیحات انگلیسی",
      },
      value: "",
      validation: {
        required: true,
        isEn: true,
      },
      valid: false,
      touched: false,
    },
    thumbnail: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "",
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addInstruments;
