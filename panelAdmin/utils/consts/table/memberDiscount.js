import React from "react";
import formatMoney from "../../formatMoney";

const memberDiscount = (data) => {
  ////console.log({ data });

  // let data =datas.discount
  const thead = ["#", "نام", "قیمت ", "تاریخ", "وضعیت"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let dataIndex = data[index];
    let discount = data[index].discount;
    let discountName = discount.title;
    let dateSplit = data[index].usedDate ? data[index].usedDate.split(" ") : "";
    let date = dateSplit ? <span>{dateSplit[1] + " - " + dateSplit[0]}</span> : "استفاده نشده";
    let price = dataIndex.price ? formatMoney(dataIndex.price) : "0";
    let isUsed = dataIndex.isUsed ? "استفاده شده" : "استفاده نشده";

    tbody.push({
      data: [discountName, price, date, isUsed],
      style: {},
    });
  }
  return [thead, tbody];
};

export default memberDiscount;
