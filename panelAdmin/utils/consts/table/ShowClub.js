import React, { Fragment } from "react";

const ShowClub = (data) => {
  const thead = ["#", "عکس", "نام ", " امتیاز ", "عضویت ", "دسته بندی ", " تخفیف", "فروش", "بازدید ", "وضعیت", "ویرایش"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let cardElement = <i style={{ fontSize: "1.2em" }} className="fal fa-credit-card"></i>;
    let applicationElement = <i style={{ fontSize: "1.2em" }} className="fal fa-mobile-android"></i>;
    let active = <i style={{ fontSize: "1em", color: "green", fontWeight: "900" }} className="far fa-check-circle"></i>;
    let deActive = <i style={{ fontSize: "1em", color: "red", fontWeight: "900" }} className="fas fa-ban"></i>;
    let NotEntered = "وارد نشده";
    let thumbnail = data[index].slides[0] ? data[index].slides[0] : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let rating = data[index].rating ? data[index].rating : 0;
    let percent = data[index].percent ? data[index].percent + " %" : NotEntered;
    let boughtCount = data[index].boughtCount ? data[index].boughtCount : "0";
    let category = data[index].category ? data[index].category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : "";
    let membership = data[index].membership.length ? data[index].membership : NotEntered;
    let membershipData =
      membership.includes("CARD") && membership.includes("APPLICATION") ? (
        <div style={{ fontSize: "1.2em", fontWeight: "900", display: "flex", justifyContent: "space-around" }}>
          {" "}
          <div>{cardElement} </div>
          <div> {applicationElement}</div>
        </div>
      ) : membership.includes("APPLICATION") ? (
        applicationElement
      ) : membership.includes("CARD") ? (
        cardElement
      ) : (
        ""
      );
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, { option: { star: true, value: rating } }, membershipData, categoryTitleFa, percent, boughtCount, viewCount, isActive, { option: { edit: true } }],
      style: {},
    });
  }
  return [thead, tbody];
};

export default ShowClub;
