const blogCategory = (data) => {
  const thead = ["#", "نام ", "مقالات", "تنظیمات"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let title = data[index].name || NotEntered;
    let count = data[index].count || "0";
    tbody.push({ data: [title, count, { option: { edit: true, remove: true } }], style: {} });
  }
  return { thead, tbody };
};

export default blogCategory;
