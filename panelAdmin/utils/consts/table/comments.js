import React from "react";
import jalaliDateConvert from "../../jalaliDateConvert";
import dictionary from "../../dictionary";

const articles = (data) => {
  const thead = ["#", "نام ", "شماره همراه", "نظر در مورد", "عنوان ", "قیمت", "تاریخ", "نظر", "تنظیمات"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let userInfo = data[index]?.user;
    let productInfo = data[index]?.parent;
    let userName = userInfo.name || NotEntered;
    let userPhoneNumber = userInfo?.phoneNumber || NotEntered;
    let productName = productInfo?.name || NotEntered;
    let productPrice = productInfo?.newPrice || productInfo?.realPrice || NotEntered;
    let content = { option: { eye: true, name: "content" } };
    let parentType = dictionary(data[index]?.parentType || NotEntered);
    let jalali = jalaliDateConvert(data[index]?.createdAt);
    let jalaliDate = data[index]?.createdAt ? jalali.clock + " - " + jalali.date : noEntries;
    let isConfirmed = data[index]?.isConfirmed;
    tbody.push({ data: [userName, userPhoneNumber, parentType, productName, productPrice, jalaliDate, content, { option: { remove: true, accept: !isConfirmed ? true : false } }], style: {} });
  }
  return { thead, tbody };
};

export default articles;
