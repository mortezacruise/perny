import panelAdmin from "../../..";

const products = (data) => {
  ////console.log({ data });

  const thead = ["#", "عکس", "عنوان", "قیمت اصلی ", " قیمت جدید", "وزن", "تخفیف", "بازدید", "تنظیمات"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let image = data[index].image || NotEntered;
    let name = data[index].name || NotEntered;
    let realPrice = data[index].realPrice ? panelAdmin.utils.formatMoney(data[index]?.realPrice) : NotEntered;
    let newPrice = data[index].newPrice ? panelAdmin.utils.formatMoney(data[index]?.newPrice) : NotEntered;
    let weight = (data[index].weight || "0") + " " + (data[index]?.unit || "");

    let discount = data[index].discount ? "%" + data[index].discount : "0";
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    tbody.push({
      data: [image, name, realPrice, newPrice, weight, discount, viewCount, { option: { edit: true, remove: true, disable: true } }],
      style: { backgroundColor: data[index].isDisable ? "#ff00005c" : "" },
    });
  }
  return { thead, tbody };
};

export default products;
