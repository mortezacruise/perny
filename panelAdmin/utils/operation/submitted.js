import updateObject from "../updateObject";

const submitted = async (props) => {
  const { removeKey, editKey, setSubmitLoading, data, editData, put, post, setData, states, setEdit, propsHideModal, setCheckSubmitted, checkSubmitted } = props;
  setSubmitLoading(true);
  setCheckSubmitted(!checkSubmitted);
  const formData = {};
  for (let formElementIdentifier in data.Form) {
    if (formElementIdentifier === editKey?.beforeKey) formData[editKey?.afterKey] = data.Form[formElementIdentifier].value;
    else if (formElementIdentifier !== removeKey) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
  }
  console.log({ formData });

  if (editData) {
    if (await put({ id: editData._id, data: formData })) {
      setEdit();
      if (propsHideModal) propsHideModal();
      if (setEdit) setEdit();
    }
  } else if (await post(formData)) {
    setData({ ...states });
    if (propsHideModal) propsHideModal();
    if (setEdit) setEdit();
  }
  if (states.Form["phone"]) {
    states.Form["phone"].value = [];
    ////console.log("phone", states.Form["phone"]);
  }

  if (states.Form["slides"]) {
    states.Form["slides"].value = [];
    ////console.log("slides", states.Form["slides"]);
  }

  setSubmitLoading(false);
};

export default submitted;
