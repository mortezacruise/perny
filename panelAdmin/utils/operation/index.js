import submitted from "./submitted";
import FormManagement from "./FormManagement";
import formTouchChange from "./formTouchChange";

const operation = { submitted, FormManagement, formTouchChange };
export default operation;
