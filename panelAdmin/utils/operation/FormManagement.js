const FormManagement = ({ categoryData, formElement, showModal, inputChangedHandler, accept, ProductData, removeHandel, staticTitle, storeData }) => {
  let disabled, staticTitleValue;
  let value = formElement.config.value;
  if (formElement.id === staticTitle?.name) staticTitleValue = staticTitle?.value;
  let key = formElement.id;
  let elementType = formElement.config.elementType;
  let elementConfig = formElement.config.elementConfig;
  let remove = (index) => removeHandel(index, formElement.id);
  let label = formElement.config.label;
  let titleValidity = formElement.config.titleValidity;
  let dataChunk = { titleValidity, value, key, elementType, elementConfig, removeHandel: remove, label, disabled, staticTitle: staticTitleValue };

  switch (formElement.id) {
    case "slides":
      disabled = true;
      return { ...dataChunk, disabled: true, accepted: () => showModal({ kindOf: "showGallery", name: formElement.id }) };
    // break;
    case "image":
      disabled = true;
      return { ...dataChunk, disabled: true, accepted: () => showModal({ kindOf: "showGallery", name: formElement.id }) };
    // break;
    case "thumbnail":
      disabled = true;
      return { ...dataChunk, disabled: true, accepted: () => showModal({ kindOf: "showGallery", name: formElement.id }) };
    case "isRequire":
      accepted = (value) => inputChangedHandler({ value: value || "", name: formElement.id });
    case "content":
      return { ...dataChunk, disabled: true, changed: (value) => inputChangedHandler({ value: value || "", name: formElement.id }) };

    case "coordinate":
      return {
        ...dataChunk,
        changed: (value) => {
          ////console.log({ value });

          inputChangedHandler({
            value: value,
            name: formElement.id,
          });
        },
      };
    case "location":
      return {
        ...dataChunk,
        changed: (value) => {
          ////console.log({ value });

          inputChangedHandler({
            value: value,
            name: formElement.id,
          });
        },
      };
    // break;
    case "category":
      return { ...dataChunk, accepted: (value) => accept({ value, name: formElement.id }), dropDownData: categoryData };
    case "blogCategory":
      return { ...dataChunk, accepted: (value) => accept({ value, name: formElement.id }), dropDownData: categoryData };

    case "product":
      return { ...dataChunk, accepted: (value) => accept({ value, name: formElement.id }), dropDownData: ProductData };
    // break;
    case "store":
      return { ...dataChunk, accepted: (value) => accept({ value, name: formElement.id }), dropDownData: ProductData };
    // break;

    default:
      return {
        ...dataChunk,
        accepted: (value) => inputChangedHandler({ value: value, name: formElement.id }),
        changed: (e) =>
          inputChangedHandler({
            value: e.currentTarget?.value,
            name: formElement.id,
            type: e.currentTarget?.type,
            files: e.currentTarget?.files,
          }),
      };
  }
};

export default FormManagement;
