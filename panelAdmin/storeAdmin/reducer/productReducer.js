import panelAdmin from "../..";

export const productInitialState = {
  productData: null,
  searchProductData: null,
};

function productReducer(state = productInitialState, action) {
  const atRedux = panelAdmin.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_PRODUCT_DATA:
      return { ...state, ...{ productData: action.data } };
    case atRedux.SET_SEARCH_PRODUCT_DATA:
      return { ...state, ...{ searchProductData: action.data } };
    default:
      return state;
  }
}

export default productReducer;
