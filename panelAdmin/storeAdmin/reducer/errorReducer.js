import panelAdmin from "../..";

export const errorInitialState = {
  error: null,
};

export const addFailure = (state, action) => {
  return { ...state, error: action.error };
};

export const removeFailure = (state) => {
  return { ...state, error: null };
};

function errorReducer(state = errorInitialState, action) {
  const atRedux = panelAdmin.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.ADD_FAILURE:
      return addFailure(state, action);
    case atRedux.REMOVE_FAILURE:
      return removeFailure(state);
    default:
      return state;
  }
}

export default errorReducer;
