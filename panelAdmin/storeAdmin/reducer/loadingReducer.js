import panelAdmin from "../..";

export const loadingInitialState = {
  loadingAll: false,
  loadingSearch: false,
};

function loadingReducer(state = loadingInitialState, action) {
  const atRedux = panelAdmin.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.CHANGE_LOADING_ALL:
      return { ...state, ...{ loadingAll: action.data } };
    case atRedux.CHANGE_LOADING_SEARCH:
      return { ...state, ...{ loadingSearch: action.data } };
    default:
      return state;
  }
}

export default loadingReducer;
