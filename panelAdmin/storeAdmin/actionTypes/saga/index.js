const atSaga = {
  POST_UPLOAD_IMAGE: "POST_UPLOAD_IMAGE_SAGA",
  GET_GALLERY_DATA: "GET_GALLERY_DATA_SAGA",
  //============================================================= CATEGORY
  GET_CATEGORY_DATA: "GET_CATEGORY_DATA_SAGA",
  GET_SEARCH_CATEGORY_DATA: "GET_SEARCH_CATEGORY_DATA_SAGA",
  //============================================================= PRODUCT
  GET_PRODUCT_DATA: "GET_PRODUCT_DATA_SAGA",
  GET_SEARCH_PRODUCT_DATA: "GET_SEARCH_PRODUCT_DATA_SAGA",
};

export default atSaga;
