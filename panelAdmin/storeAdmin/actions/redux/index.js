import atRedux from "../../actionTypes/redux";
// =================================================== LOADING_ALL
export function changeLoading(data) {
  return { type: atRedux.CHANGE_LOADING_ALL, data };
}
// =================================================== LOADING_SEARCH
export function changeLoadingSearch(data) {
  return { type: atRedux.CHANGE_LOADING_SEARCH, data };
}
// =================================================== NAVBAR
export function setPageName(data) {
  return { type: atRedux.SET_PAGE_NAME, data };
}
// =================================================== UPLOAD
export function setUploadImage(data) {
  return { type: atRedux.SET_UPLOAD_IMAGE, data };
}
// =================================================== GALLERY
export function setGalleryData(data) {
  return { type: atRedux.SET_GALLERY_DATA, data };
}
export function changeAddGalleryData(data) {
  return { type: atRedux.CHANGE_ADD_GALLERY_DATA, data };
}
// =================================================== CATEGORY
export function setCategoryData(data) {
  return { type: atRedux.SET_CATEGORY_DATA, data };
}
export function setSearchCategoryData(data) {
  return { type: atRedux.SET_SEARCH_CATEGORY_DATA, data };
}
// =================================================== PRODUCT
export function setProductData(data) {
  return { type: atRedux.SET_PRODUCT_DATA, data };
}
export function setSearchProductData(data) {
  return { type: atRedux.SET_SEARCH_PRODUCT_DATA, data };
}
