import atSaga from "../../actionTypes/saga";

export function uploadImageData(data) {
  return { type: atSaga.POST_UPLOAD_IMAGE, data: data };
}
export function getGalleryData(data) {
  return { type: atSaga.GET_GALLERY_DATA, data: data };
}
// ========================================================= CATEGORY

export function getCategoryData({ page }) {
  ////console.log({ mojtaba1: page });
  return { type: atSaga.GET_CATEGORY_DATA, page };
}
export function getSearchCategoryData({ title, page }) {
  return { type: atSaga.GET_SEARCH_CATEGORY_DATA, page, title };
}
// ========================================================= PRODUCT

export function getProductData({ page }) {
  return { type: atSaga.GET_PRODUCT_DATA, page };
}
export function getSearchProductData({ title, page }) {
  return { type: atSaga.GET_SEARCH_PRODUCT_DATA, page, title };
}
// ========================================================= SLIDER
export function getSliderData({ page }) {
  return { type: atSaga.GET_SLIDER_DATA, page };
}
export function getSearchSliderData({ title, page }) {
  return { type: atSaga.GET_SEARCH_SLIDER_DATA, page, title };
}
// ========================================================= MOOD
export function getMoodData({ page }) {
  return { type: atSaga.GET_MOOD_DATA, page };
}
export function getSearchMoodData({ title, page }) {
  return { type: atSaga.GET_SEARCH_MOOD_DATA, page, title };
}
// ========================================================= ALBUM
export function getAlbumData({ page }) {
  return { type: atSaga.GET_ALBUM_DATA, page };
}
export function getSearchAlbumData({ title, page }) {
  return { type: atSaga.GET_SEARCH_ALBUM_DATA, page, title };
}
// ========================================================= HASHTAG
export function getHashtagData({ page }) {
  return { type: atSaga.GET_HASHTAG_DATA, page };
}
export function getSearchHashtagData({ title, page }) {
  return { type: atSaga.GET_SEARCH_HASHTAG_DATA, page, title };
}

// ========================================================= GENRE
export function getGenreData({ page }) {
  return { type: atSaga.GET_GENRE_DATA, page };
}
export function getSearchGenreData({ title, page }) {
  return { type: atSaga.GET_SEARCH_GENRE_DATA, page, title };
}
