import { put } from "redux-saga/effects";
import actions from "../../../actions";
import panelAdmin from "../../../../index";

export function* categoryData({ page }) {
  yield put(actions.reduxActions.changeLoading(true));

  ////console.log({ mojtaba3: page });
  try {
    // const res = yield panelAdmin.api.get.artists({ page });
    const res = yield panelAdmin.api.get.categories({ page });
    ////console.log({ resCategoryData: res });
    yield put(actions.reduxActions.setCategoryData(res.data));
    yield put(actions.reduxActions.changeLoading(false));
  } catch (err) {
    ////console.log({ errCategoryData: err });
    if (!err.response) yield put(actions.reduxActions.setFailure(1090));
    yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  }
}

export function* categorySearchData({ page, title }) {
  yield put(actions.reduxActions.changeLoading(true));

  try {
    const res = yield panelAdmin.api.get.CategorySearch({ page, title });
    ////console.log({ resCategorySearchData: res });

    yield put(actions.reduxActions.setSearchCategoryData(res.data));
  } catch (err) {
    ////console.log({ errCategorySearchData: err });

    if (!err.response) yield put(actions.reduxActions.setFailure(1090));
    yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
