import { put } from "redux-saga/effects";
import actions from "../../../actions";
import panelAdmin from "../../../../index";

export function* countryData({ page }) {
  yield put(actions.reduxActions.startGetCountry());

  ////console.log({ mojtaba3: page });
  try {
    const res = yield panelAdmin.api.get.country({ page });
    ////console.log({ resCountryData: res });
    yield put(actions.reduxActions.setCountryData(res.data));
  } catch (err) {
    ////console.log({ errCountryData: err });
    if (!err.response) yield put(actions.reduxActions.setFailure(1090));
    yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  }
}

export function* countrySearchData({ page, title }) {
  yield put(actions.reduxActions.startSearchCountry());

  //   try {
  //     const res = yield panelAdmin.api.get.CountrySearch({ page, title });
  //     ////console.log({ resCountrySearchData: res });

  //     yield put(actions.reduxActions.setSearchCountryData(res.data));
  //   } catch (err) {
  //     ////console.log({ errCountrySearchData: err });

  //     if (!err.response) yield put(actions.reduxActions.setFailure(1090));
  //     yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  //   }
}
