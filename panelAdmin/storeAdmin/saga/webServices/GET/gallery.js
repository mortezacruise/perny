import { put } from "redux-saga/effects";
import actions from "../../../actions";
import panelAdmin from "../../../../index";
export function* galleryData({ data }) {
  ////console.log("omad inja");
  ////console.log({ mehrad: panelAdmin.api.get.gallery, data });

  try {
    const res = yield panelAdmin.api.get.gallery({ ...data });
    ////console.log({ res });
    yield put(actions.reduxActions.setGalleryData(res.data)); //********** Level 5 **********//
  } catch (err) {
    ////console.log({ err });

    if (!err.response) yield put(actions.reduxActions.setFailure(1090));
    yield put(actions.reduxActions.setFailure(err.response.data.CODE)); //********** Level 6 **********//
  }
}
