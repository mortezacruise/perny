import { put } from "redux-saga/effects";
import actions from "../../../actions";
import panelAdmin from "../../../../index";

export function* productData({ page }) {
  yield put(actions.reduxActions.changeLoading(true));

  ////console.log({ mojtaba3: page });
  try {
    // const res = yield panelAdmin.api.get.artists({ page });
    const res = yield panelAdmin.api.get.products({ page });
    ////console.log({ resProductData: res });
    yield put(actions.reduxActions.setProductData(res.data));
    yield put(actions.reduxActions.changeLoading(false));
  } catch (err) {
    ////console.log({ errProductData: err });
    if (!err.response) yield put(actions.reduxActions.setFailure(1090));
    yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  }
}

export function* productSearchData({ page, title }) {
  yield put(actions.reduxActions.changeLoading(true));

  try {
    const res = yield panelAdmin.api.get.ProductSearch({ page, title });
    ////console.log({ resProductSearchData: res });

    yield put(actions.reduxActions.setSearchProductData(res.data));
  } catch (err) {
    ////console.log({ errProductSearchData: err });

    if (!err.response) yield put(actions.reduxActions.setFailure(1090));
    yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
