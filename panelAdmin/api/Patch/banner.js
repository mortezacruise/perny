import axios from "../../../utils/axiosBase";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const banner = async (id, param, loading) => {
  loading(true);

  let URL = Strings.ApiString.BANNERS;
  ////console.log({ apiParam: param });
  return axios
    .patch(URL + "/" + id, param)
    .then((Response) => {
      loading(false);
      ////console.log({ Response });
      if (Response.data);
      toastify("با موفقیت ثبت شد", "success");
      return true;
    })
    .catch((error) => {
      ////console.log({ error });
      loading(false);

      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

      return false;
    });
};
export default banner;
