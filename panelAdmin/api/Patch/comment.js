import axios from "../axios-orders";
import panelAdmin from "../..";

const comment = async (id) => {
  const toastify = panelAdmin.utils.toastify;
  let URL = panelAdmin.values.apiString.COMMENT;
  return axios
    .patch(URL, id)
    .then((Response) => {
      console.log({ Response });
      toastify("با موفقیت تایید شد", "success");
      return true;
    })
    .catch((error) => {
      console.log({ error });
      return false;
    });
};
export default comment;
