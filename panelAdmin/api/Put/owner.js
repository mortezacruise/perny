import globalUtils from "../../../globalUtils";
import panelAdmin from "../..";

const owner = async (param) => {
  const axios = globalUtils.axiosBase;
  const toastify = panelAdmin.utils.toastify;
  const URL = panelAdmin.values.apiString.OWNERS;
  //////console.log({ apiParam: param });
  // setLoading(true);

  return axios
    .put(URL + "/" + param.id, param.data)
    .then((Response) => {
      //////console.log({ Response });
      // setLoading(false);
      if (Response.data);
      toastify("با موفقیت ثبت شد", "success");
      return true;
    })
    .catch((error) => {
      ////console.log({ error });

      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

      return false;
    });
};
export default owner;
