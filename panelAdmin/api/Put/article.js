import axios from "../axios-orders";
import panelAdmin from "../..";

const article = async (param) => {
  const toastify = panelAdmin.utils.toastify;
  const URL = panelAdmin.values.apiString.BLOG;

  return axios
    .put(URL + "/" + param.id, param.data)
    .then((Response) => {
      console.log({ Response });
      toastify("با موفقیت ثبت شد", "success");
      return true;
    })
    .catch((error) => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

      return false;
    });
};
export default article;
