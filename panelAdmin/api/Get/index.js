import categories from "./categories";
import products from "./products";
import productSearch from "./productSearch";
import gallery from "./gallery";
import sliders from "./sliders";
import banners from "./banners";
import blogCategory from "./blogCategory";
import articles from "./articles";
import articleSearch from "./articleSearch";
import comments from "./comments";

const get = {
  categories,
  products,
  productSearch,
  gallery,
  sliders,
  banners,
  blogCategory,
  articles,
  articleSearch,
  comments,
};
export default get;
