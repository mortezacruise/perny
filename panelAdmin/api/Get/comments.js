import axios from "../axios-orders";
import panelAdmin from "../..";

const comments = async (page) => {
  let getUrl = page ? panelAdmin.values.apiString.COMMENT + "?page=" + page : panelAdmin.values.apiString.COMMENT;

  return axios
    .get(getUrl)
    .then((res) => {
      console.log({ res });
      return res?.data;
    })
    .catch((error) => {
      console.log({ error });
      // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      // else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return false;
    });
};

export default comments;
