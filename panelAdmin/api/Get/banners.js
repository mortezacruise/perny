import axios from "../axios-orders";
import panelAdmin from "../..";

const banners = async (page) => {
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString;
  const axiosData = page ? strings.BANNER + "/p/" + page : strings.BANNER;

  return axios
    .get(axiosData)
    .then((banners) => {
      console.log({ banners });
      return banners;
    })
    .catch((error) => {
      console.log({ error });
      // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return false;
    });
};

export default banners;
