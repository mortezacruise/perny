import axios from "../axios-orders";
import panelAdmin from "../..";

const gallery = async ({ page }) => {
  // ////console.log({ page });

  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString;
  return axios
    .get(strings.IMAGE + "/p/" + page)
    .then((gallery) => {
      // ////console.log({ gallery });
      return gallery;
    })
    .catch((error) => {
      ////console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    });
};

export default gallery;
