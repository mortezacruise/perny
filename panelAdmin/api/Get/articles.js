import axios from "../axios-orders";
import panelAdmin from "../..";

const articles = async (page) => {
  const strings = panelAdmin.values.apiString;
  let getUrl = page ? strings.BLOG + "?page=" + page : strings.BLOG;

  return axios
    .get(getUrl)
    .then((res) => {
      console.log({ res });
      return res?.data;
    })
    .catch((error) => {
      console.log({ error });
      // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      // else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return false;
    });
};

export default articles;
