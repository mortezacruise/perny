import axios from "../axios-orders";
import panelAdmin from "../..";

const blogCategory = async (page) => {
  const strings = panelAdmin.values.apiString;
  let getUrl = page ? strings.BLOG_CATEGORY + "/p/" + page : strings.BLOG_CATEGORY;
  console.log("omad blog category");

  return axios
    .get(getUrl)
    .then((category) => {
      console.log({ category });
      return category?.data;
    })
    .catch((error) => {
      console.log({ error });
      // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      // else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return false;
    });
};

export default blogCategory;
