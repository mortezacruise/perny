import axios from "../../../utils/axiosBase";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const transactions = async (returnData, page, loading) => {
  let getUrl = page ? Strings.ApiString.TRANSACTION + "/" + page : Strings.ApiString.TRANSACTION;
  return axios
    .get(getUrl)
    .then((transactions) => {
      ////console.log({ transactions });
      returnData(transactions.data);
      loading(false);
      return true;
    })
    .catch((error) => {
      ////console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return false;
    });
};

export default transactions;
