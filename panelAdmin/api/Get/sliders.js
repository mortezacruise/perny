import axios from "../axios-orders";
import panelAdmin from "../..";

const sliders = async (page) => {
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString;
  const axiosData = page ? strings.SLIDER + "/p/" + page : strings.SLIDER;
  return axios
    .get(axiosData)
    .then((sliders) => {
      console.log({ sliders });
      return sliders;
    })
    .catch((error) => {
      console.log({ error });
      return false;
    });
};

export default sliders;
