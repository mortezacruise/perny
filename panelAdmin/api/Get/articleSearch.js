import axios from "../axios-orders";
import panelAdmin from "../..";

const articleSearch = async (param, page = 1) => {
  const strings = panelAdmin.values.apiString.BLOG + "?page=" + page + "/&search=" + param;
  return axios
    .get(strings)
    .then((articleSearch) => {
      console.log({ articleSearch });

      return articleSearch?.data;
    })
    .catch((error) => {
      console.log({ error });
      return false;
    });
};

export default articleSearch;
