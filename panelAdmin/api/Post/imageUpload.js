import axios from "../axios-orders";
import axios2 from "axios";

import panelAdmin from "../..";

const imageUpload = async (files, setLoading, setState, imageName) => {
  const toastify = panelAdmin.utils.toastify;
  ////console.log({ files, setLoading, setState, imageName });

  setLoading(true);
  // ============================================= const
  const CancelToken = axios2.CancelToken;
  const source = CancelToken.source();
  const settings = {
    onUploadProgress: (progressEvent) => {
      let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);

      setState((prev) => ({ ...prev, progressPercentImage: percentCompleted }));
    },
    cancelToken: source.token,
  };
  const URL = panelAdmin.values.apiString.UPLOAD;
  const formData = new FormData();
  formData.append("imageName", imageName);
  // formData.append("imageType", type);
  formData.append("image", files);

  //=============================================== axios
  return axios
    .post(URL, formData, settings)
    .then((Response) => {
      ////console.log({ Response });
      setLoading(false);
      return Response.data;
    })
    .catch((error) => {
      ////console.log({ error });
      setLoading(false);
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return false;
    });
};
export default imageUpload;
