import imageUpload from "./imageUpload";
import category from "./category";
import product from "./product";
import banner from "./banner";
import slider from "./slider";
import blogCategory from "./blogCategory";
import article from "./article";
import comment from "./comment";

const post = {
  imageUpload,
  category,
  product,
  banner,
  slider,
  blogCategory,
  article,
  comment,
};
export default post;
