import axios from "../axios-orders";
import panelAdmin from "../..";

const article = async (param, setLoading) => {
  // setLoading(true);
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString;
  let URL = strings.BLOG;
  return axios
    .post(URL, param)
    .then((Response) => {
      console.log({ Response });
      // setLoading(false);

      toastify("با موفقیت ثبت شد", "success");
      return true;
    })
    .catch((error) => {
      console.log({ error });

      return false;
    });
};
export default article;
