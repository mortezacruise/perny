import panelAdmin from "../..";
import globalUtils from "../../../globalUtils";
const comment = async (param) => {
  const axios = globalUtils.axiosBase;
  const toastify = panelAdmin.utils.toastify;
  const URL = panelAdmin.values.apiString.COMMENT;
  return axios
    .delete(URL + "/" + param)
    .then((Response) => {
      console.log({ Response });
      toastify("با موفقیت حذف شد", "success");
      return true;
    })
    .catch((error) => {
      console.log({ error });
      return false;
    });
};
export default comment;
