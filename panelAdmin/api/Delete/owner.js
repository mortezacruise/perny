import panelAdmin from "../..";
import globalUtils from "../../../globalUtils";
// import Axios from "axios";
const owner = async (param) => {
  const axios = globalUtils.axiosBase;
  const toastify = panelAdmin.utils.toastify;
  const URL = panelAdmin.values.apiString.OWNERS;
  ////console.log({ URL });
  return axios
    .delete(URL + "/" + param)
    .then((Response) => {
      ////console.log({ Response });
      if (Response.data);
      toastify("با موفقیت حذف شد", "success");
      return true;
    })
    .catch((error) => {
      ////console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

      return false;
    });
};
export default owner;
