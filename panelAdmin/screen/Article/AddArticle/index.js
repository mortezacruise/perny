import React, { useRef, useEffect, useState, Fragment } from "react";
import { post, put } from "../../../api";
import FormInputArticle from "./FormInputArticle";
import panelAdmin from "../../..";
import Gallery from "../../../../pages/panelAdmin/gallery";
import LoadingDot1 from "../../../component/UI/Loadings/LoadingDot1";
import ModalOption from "./DependentComponent/ModalOption";
import EditData from "./DependentComponent/EditData";

const AddArticle = (props) => {
  const { editData, setEdit, propsHideModal } = props;
  const states = panelAdmin.utils.consts.states;
  const onChanges = panelAdmin.utils.onChanges;
  const [data, setData] = useState({ ...states.addArticle });
  const [state, setState] = useState({ progressPercentImage: null, remove: { value: "", name: "" } });
  const [Loading, setLoading] = useState(false);
  const [submitLoading, setSubmitLoading] = useState(false);
  const [modalDetails, setModalDetails] = useState({
    show: false,
    kindOf: null,
    name: null,
    removeId: null,
    editId: null,
    editData: null,
  });
  const [imageAccepted, setImageAccepted] = useState();
  const [checkSubmitted, setCheckSubmitted] = useState(false);
  // ========================================================= change edit data structure for state
  useEffect(() => {
    EditData({ editData, stateArray, inputChangedHandler });
  }, [editData]);
  // ========================================================= modal

  // ================================== modal close
  const onHideModal = () => {
    setModalDetails({ ...modalDetails, editData: null, show: false, kindOf: false, removeId: null });
  };
  // ================================== modal open
  const onShowModal = (event) => {
    setModalDetails({ ...modalDetails, show: true, kindOf: event.kindOf, name: event?.name, editData: event?.editData, removeId: event?.removeId });
  };
  // ================================== handel modal end work
  const modalRequest = () => {
    onHideModal();
  };
  // ========================================================= END modal
  const removeHandel = (value, name) => {
    onShowModal({ kindOf: "question" });
    setState({ ...state, remove: { value, name } });
  };
  // ========================================================= accepted image for add data
  const acceptedImage = ({ index, data }) => {
    let images = data.image.value;
    imageAccepted === images ? setImageAccepted(null) : setImageAccepted(images);
  };
  const acceptedImageFinal = () => {
    onHideModal();
    inputChangedHandler({ name: "thumbnail", value: imageAccepted });
  };
  // ========================================================= End accepted image for add data

  // ========================================================= submitted api response data
  const _onSubmitted = async (e) => panelAdmin.utils.operation.submitted({ setSubmitLoading, data, editData, put: put.article, post: post.article, setData, states: states.addArticle, setEdit, propsHideModal, setCheckSubmitted, checkSubmitted });

  const checkValid = () => panelAdmin.utils.operation.formTouchChange({ data, setData });

  // ========================================================= End submitted api response data
  // ========================================================= changed data with input type
  const inputChangedHandler = async (event) => await onChanges.globalChange({ event, data, setData, setState, setLoading, imageType: "thumbnail" });

  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  let form = <FormInputArticle removeHandel={removeHandel} _onSubmited={_onSubmitted} stateArray={stateArray} data={data} state={state} setData={setData} Loading={Loading} setLoading={setLoading} inputChangedHandler={inputChangedHandler} checkSubmited={checkSubmitted} showModal={onShowModal} />;
  return (
    <div className="countainer-main">
      <ModalOption {...{ inputChangedHandler, modalRequest, onHideModal, modalDetails, imageAccepted, acceptedImageFinal, acceptedImage, Gallery }} />
      <div style={{ marginTop: "20px" }} className="form-countainer">
        <div className="row-give-information">
          {form}
          <div className="btns-container">
            <button className="btns btns-primary" disabled={submitLoading} onClick={data.formIsValid ? _onSubmitted : checkValid}>
              {submitLoading ? <LoadingDot1 width="1.5rem" height="1.5rem" /> : editData ? data.formIsValid ? "ثبت تغییرات" : "تغیراتی مشاهده نمی شود" : data.formIsValid ? "افزودن" : "اطلاعات کامل نمی باشد"}
            </button>

            <button className="btns btns-warning" onClick={propsHideModal}>
              {"بستن"}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddArticle;
