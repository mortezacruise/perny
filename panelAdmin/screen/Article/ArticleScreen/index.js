import React, { useState } from "react";
import panelAdmin from "../../..";
import AddArticle from "../AddArticle";
import ModalStructure from "../../../component/UI/Modals/ModalStructure";
import ShowCardInformation from "../../../component/cards/ShowCardInformation";
import { Button } from "react-bootstrap";
import PaginationM from "../../../component/UI/PaginationM";
import InputVsIcon from "../../../component/UI/InputVsIcon";
const ArticleScreen = (props) => {
  const { apiPageFetch, onDataSearch, requestData, loadingApi } = props;
  const [removeLoading, setRemoveLoading] = useState(false);
  const [modalDetails, setModalDetails] = useState({
    show: false,
    kindOf: null,
    data: null,
    name: null,
    removeId: null,
    editId: null,
    editData: null,
  });
  console.log({ modalDetails });

  // ========================================================= remove data with data id
  const reqApiRemove = async (id) => {
    setRemoveLoading(true);
    if (await panelAdmin.api.deletes.article(id)) {
      apiPageFetch(requestData.page);
      onHideModal();
    }
    setRemoveLoading(false);
  };
  // ========================================================= End remove data with data id
  // ========================================================= modal
  // ================================== modal close
  const onHideModal = () => {
    setModalDetails({ ...modalDetails, show: false, kindOf: false, removeId: null, editData: null });
  };
  // ================================== modal open
  const onShowModal = (event) => {
    setModalDetails({ ...modalDetails, show: true, ...event });
  };
  // ================================== handel modal end work
  const modalRequest = async (bool) => {
    if (bool) reqApiRemove(modalDetails.removeId);
    else onHideModal();
  };
  // ========================================================= END modal
  const card = panelAdmin.utils.consts.card;
  const optionClick = ({ _id, mission, index }) => {
    ////console.log({ _id, mission });
    switch (mission) {
      case "remove":
        onShowModal({ kindOf: "question", removeId: _id });
        break;
      case "edit":
        onShowModal({ kindOf: "component", editData: requestData.docs[index] });
        break;
      default:
        break;
    }
  };
  const searchData = (e) => {
    onDataSearch("", e.target.value);
  };

  return (
    <div className="gallery">
      <div className="gallery-header-wrapper">
        <div className="gallery-header">
          <InputVsIcon name="genreTitle" icon="far fa-search" placeholder="جستجو..." onChange={searchData} dir="ltr" />
          <Button onClick={() => onShowModal({ kindOf: "component" })} className="">
            {"افزودن مقاله"}
          </Button>
        </div>
        <ModalStructure {...{ modalRequest, reqApiRemove, onHideModal, modalDetails }} loading={removeLoading}>
          {modalDetails.kindOf === "component" && <AddArticle propsHideModal={onHideModal} setEdit={apiPageFetch} editData={modalDetails?.editData} modalAccept={modalRequest} />}
          {modalDetails.kindOf === "showTitle" && <p>{modalDetails.showData}</p>}
        </ModalStructure>
        <ShowCardInformation data={card.article(requestData?.docs)} onClick={null} optionClick={optionClick} options={{ remove: true, edit: true }} />
        {/* <ArticleTableElement requestData={requestData} handelPage={apiPageFetch} tableOnclick={tableOnclick} handelChange={searchData} /> */}
        <PaginationM limited={"3"} pages={requestData?.pages} activePage={requestData?.page} onClick={apiPageFetch} />
      </div>
    </div>
  );
};

export default ArticleScreen;
