import React, { useRef } from "react";
import Pagination from "../../../../component/UI/PaginationM";
import InputVsIcon from "../../../../component/UI/InputVsIcon";
import panelAdmin from "../../../..";
// import IsNull from "../../../../components/UI/IsNull";
import ReportApiComponent from "../../../../component/UI/ReportApiComponent";
import TabelBasic from "../../../../component/UI/Tables";
const ArticleTableElement = (props) => {
  const { requestData, handelPage, tableOnclick, handelChange } = props;
  let title = useRef(null);
  let componentReport = null;
  if (!requestData) {
    title.current = "در حال بارگزاری اطلاعات ...";
    componentReport = <ReportApiComponent title={title.current} />;
  } else if (requestData) {
    if (!requestData?.docs?.length) {
      componentReport = <ReportApiComponent title={title.current} />;
      title.current = "اطلاعاتی یافت نشد";
    }
    // else if (requestData?.docs?.length > 0) title.current = "کل اطلاعات";
  }
  return (
    <div className="countainer-main centerAll ">
      <div className="elemnts-box  boxShadow tableMainStyle">
        <TabelBasic
          subTitle={
            <div className="disColum">
              <h4
                style={{
                  color: "black",
                  paddingBottom: "0.2em",
                  fontSize: "1.5em",
                }}
              ></h4>
              {requestData ? <span style={{ fontSize: "0.9em" }}>{`   تعداد کل : ${requestData.total}  `}</span> : " "}
            </div>
          }
          imgStyle={{ width: "2em", height: "2em", borderRadius: "0.4em" }}
          tbody={panelAdmin.utils.consts.table.articles(requestData ? requestData.docs : []).tbody}
          thead={panelAdmin.utils.consts.table.articles(requestData ? requestData.docs : []).thead}
          onClick={tableOnclick}
          apiPage={{ page: requestData?.page, limit: requestData?.limit }}
          inputRef
          btnHead={{ title: "افزودن" }}
          searchHead={InputVsIcon({
            name: "genreTitle",
            icon: "far fa-search",
            placeholder: "جستجو...",
            onChange: handelChange,
            dir: "ltr",
          })}
        />
        {componentReport}

        {requestData && requestData.pages >= 2 && <Pagination limited={"3"} pages={requestData ? requestData.pages : ""} activePage={requestData ? requestData.page : ""} onClick={handelPage} />}
      </div>
    </div>
  );
};

export default ArticleTableElement;
