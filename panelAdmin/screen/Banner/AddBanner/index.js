import React, { useEffect, useState } from "react";
import { post, put } from "../../../api";
import FormInputBanner from "./FormInputBanner";
import panelAdmin from "../../..";
import Gallery from "../../../../pages/panelAdmin/gallery";
import LoadingDot1 from "../../../component/UI/Loadings/LoadingDot1";
import ModalOption from "./DependentComponent/ModalOption";
import EditData from "./DependentComponent/EditData";
const AddBanner = (props) => {
  const { editData, setEdit, propsHideModal } = props;
  const states = panelAdmin.utils.consts.states;
  const onChanges = panelAdmin.utils.onChanges;
  const [data, setData] = useState({ ...states.addBanner });
  const [state, setState] = useState({ progressPercentImage: null, remove: { value: "", name: "" } });
  const [Loading, setLoading] = useState(false);
  const [submitLoading, setSubmitLoading] = useState(false);
  const [modalDetails, setModalDetails] = useState({
    show: false,
    kindOf: null,
    name: null,
    removeId: null,
    editId: null,
    editData: [],
  });
  const [imageAccepted, setImageAccepted] = useState();
  const [checkSubmitted, setCheckSubmitted] = useState(false);
  const [staticTitle, setStaticTitle] = useState(false);
  const [removeStateData, setRemoveStateData] = useState("");
  const [acceptedData, setAcceptedData] = useState("");
  // ========================================================= change edit data structure for state
  useEffect(() => {
    EditData({ editData, stateArray, inputChangedHandler, setStaticTitle });
  }, [editData]);
  // ========================================================= modal

  // ================================== modal close
  const onHideModal = () => {
    setModalDetails({ ...modalDetails, show: false, kindOf: false, removeId: null });
  };
  // ================================== modal open
  const onShowModal = (event) => {
    setModalDetails({ ...modalDetails, show: true, kindOf: event.kindOf, name: event?.name, editData: event?.editData, removeId: event?.removeId });
  };
  // ================================== handel modal end work
  const modalRequest = async (bool) => {
    if (bool) inputChangedHandler({ name: state.remove.name, value: state.remove.value });
    onHideModal();
  };
  // ========================================================= END modal
  const removeHandel = (value, name) => {
    onShowModal({ kindOf: "question" });
    setState({ ...state, remove: { value, name } });
  };
  // ========================================================= accepted image for add data
  const acceptedImage = ({ index, data }) => {
    let images = data.image.value;
    imageAccepted === images ? setImageAccepted(null) : setImageAccepted(images);
  };
  const acceptedImageFinal = () => {
    onHideModal();
    inputChangedHandler({ name: "image", value: imageAccepted });
  };
  // ========================================================= End accepted image for add data
  // ============================= submited
  const _onSubmitted = async (e) => panelAdmin.utils.operation.submitted({ removeKey: removeStateData, editKey: { beforeKey: acceptedData, afterKey: "parent" }, setSubmitLoading, data, editData, put: put.banner, post: post.banner, setData, states: states.addBanner, setEdit, propsHideModal, setCheckSubmitted, checkSubmitted });

  const checkValid = () => panelAdmin.utils.operation.formTouchChange({ data, setData });

  // ========================= End submited =================

  const inputChangedHandler = async (event, initial, array) => {
    let validName;
    let acceptedData;
    console.log({ event, initial, array });

    if (initial || array) {
      if (event.value === "Category" || (array ? array.value === "Category" : "")) {
        validName = "product";
        acceptedData = "category";
      } else {
        validName = "category";
        acceptedData = "product";
      }
      setRemoveStateData(validName);
      setAcceptedData(acceptedData);
    }
    // console.log({ validName });

    await onChanges.globalChange({ event, data, setData, setState, setLoading, imageType: "flag", validName });
  };

  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  let form = <FormInputBanner {...{ removeHandel, stateArray, data, state, setData, Loading, setLoading, inputChangedHandler, checkSubmitted, staticTitle }} showModal={onShowModal} />;
  return (
    <div className="countainer-main  ">
      <ModalOption {...{ inputChangedHandler, modalRequest, onHideModal, modalDetails, imageAccepted, acceptedImageFinal, acceptedImage, Gallery }} />
      <div className="form-countainer">
        <div className="row-give-information">
          {form}
          <div className="btns-container">
            <button className="btns btns-primary" disabled={submitLoading} onClick={data.formIsValid ? _onSubmitted : checkValid}>
              {submitLoading ? <LoadingDot1 width="1.5rem" height="1.5rem" /> : editData ? data.formIsValid ? "ثبت تغییرات" : "تغیراتی مشاهده نمی شود" : data.formIsValid ? "افزودن" : "اطلاعات کامل نمی باشد"}
            </button>
            {editData ? (
              <button className="btns btns-warning" onClick={propsHideModal}>
                {"بستن"}
              </button>
            ) : (
              ""
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddBanner;
