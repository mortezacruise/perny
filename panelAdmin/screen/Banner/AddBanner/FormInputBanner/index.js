import React, { useState, useEffect } from "react";
import Inputs from "../../../../component/UI/Inputs/Input";
import panelAdmin from "../../../..";
import { ButtonGroup, Button } from "react-bootstrap";
const FormInputBanner = (props) => {
  const { stateArray, removeHandel, state, _onSubmited, inputChangedHandler, checkSubmitted, showModal, staticTitle } = props;
  const [Category, setCategory] = useState([]);
  const [Product, setProduct] = useState([]);

  const accept = (event) => {
    inputChangedHandler(event);
  };

  useEffect(() => {
    getApi();
  }, []);

  const reqProducts = async () => await panelAdmin.api.get.products();
  const reqCategory = async () => await panelAdmin.api.get.categories();

  const getApi = async () => {
    const reqProductsFunc = await reqProducts();
    const reqCategoriesFunc = await reqCategory();

    if (reqProductsFunc) setProduct(reqProductsFunc?.data);
    else if (reqProductsFunc) setProduct(reqProductsFunc?.data);
    else panelAdmin.utils.toastify("خطا در گرفتن فروشندگان ", "error");

    if (await reqCategoriesFunc) setCategory(reqCategoriesFunc?.data);
    else if (await reqCategoriesFunc) setCategory(reqCategoriesFunc?.data);
    else panelAdmin.utils.toastify("خطا در گرفتن دسته بندی ", "error");
  };

  let ProductData = [];
  for (let index in Product) ProductData.push({ value: Product[index]._id, title: Product[index].name, description: Product[index].realPrice, image: Product[index].image });
  // =========================== FOR DROP DOWN CATEGORY ===========================
  let categoryData = [];
  for (let index in Category) categoryData.push({ value: Category[index]._id, title: Category[index].name, description: Category[index].titleEn });

  return (
    <form>
      {stateArray.map((formElement) => {
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;
        let progress, staticTitleValue, display;
        const inputClasses = ["InputElement"];
        if (formElement.id === staticTitle.name) staticTitleValue = staticTitle.value;
        if (invalid && shouldValidate && touched) inputClasses.push("Invalid");
        let FormManagement = panelAdmin.utils.operation.FormManagement({ basicData: stateArray, formElement, showModal, inputChangedHandler, accept, removeHandel, staticTitle, categoryData, ProductData });
        let parentType = stateArray[0].config.value;
        if (formElement.id === "category") parentType === "Category" ? (display = "") : (display = "none");
        if (formElement.id === "product") parentType === "Product" ? (display = "") : (display = "none");

        let form = <Inputs {...{ display, invalid, shouldValidate, touched, progress, checkSubmitted }} {...FormManagement} />;
        if (formElement.id === "parentType") {
          form = (
            <div className={"Input"}>
              <label className={"Label"}>{formElement.config.label}</label>
              <ButtonGroup style={{ direction: "ltr", width: "100%" }} aria-label="Basic example">
                {formElement.config.childValue.map((child, index) => {
                  return (
                    <Button
                      disabled={""}
                      onClick={() => inputChangedHandler({ value: child.value, name: formElement.id }, { initialState: true })}
                      style={{
                        backgroundColor: formElement.config.value === child.value ? "#07a7e3" : "",
                      }}
                      key={index}
                      variant="secondary"
                    >
                      {child.name}{" "}
                    </Button>
                  );
                })}
              </ButtonGroup>
            </div>
          );
        }
        return form;
      })}
    </form>
  );
};

export default FormInputBanner;
