import React from "react";

const EditData = (props) => {
  const { editData, stateArray, inputChangedHandler, setStaticTitle } = props;
  let arrayData = [];
  let parentType;

  if (editData?.parentType === "Product") parentType = "product";
  else if (editData?.parentType === "Category") parentType = "category";
  if (editData)
    for (const key in editData)
      for (let index = 0; index < stateArray.length; index++) {
        if (stateArray[index].id === key || key === "parent") {
          if (key === "parent" && stateArray[index].id === parentType) {
            if (editData[key]) arrayData.push({ name: stateArray[index].id, value: editData[key]?._id });
          } else if (key !== "parent") arrayData.push({ name: key, value: editData[key] ? editData[key] : editData[key] ? editData[key] : "" });
        }
      }
  console.log({ arrayData });

  if (arrayData.length > 0) inputChangedHandler(arrayData, false, { value: editData.parentType });
};

export default EditData;
