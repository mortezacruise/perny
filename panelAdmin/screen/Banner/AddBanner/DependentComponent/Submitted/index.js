import React from "react";
import updateObject from "../../../../../utils/updateObject";

const Submitted = async (props) => {
  const { setSubmitLoading, data, editData, put, post, setData, states, setEdit, propsHideModal, setCheckSubmitted, checkSubmitted } = props;
  setSubmitLoading(true);
  setCheckSubmitted(!checkSubmitted);
  const formData = {};
  for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
  const initialStateOwner = updateObject(states.addBanner.Form["owner"], { value: [] });
  const updatedForm = updateObject(states.addBanner.Form, { ["owner"]: initialStateOwner });
  if (editData) {
    if (await put.banner({ id: editData._id, data: formData })) {
      setEdit();
      propsHideModal();
    }
  } else if (await post.banner(formData)) setData({ Form: updatedForm, formIsValid: false });

  setSubmitLoading(false);
};

export default Submitted;
