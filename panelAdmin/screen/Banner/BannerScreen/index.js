import React, { useEffect, useState, Fragment, useRef } from "react";
import SpinnerRotate from "../../../component/UI/Loadings/SpinnerRotate";
import ShowCardInformation from "../../../component/cards/ShowCardInformation";
import panelAdmin from "../../..";
import utils from "../../../utils";
import PaginationM from "../../../component/UI/PaginationM";
import ModalStructure from "../../../component/UI/Modals/ModalStructure";
import AddBanner from "../AddBanner";

const BannerScreen = (props) => {
  const { apiPageFetch, onDataChange, filters, acceptedCardInfo, requestData, handelPage } = props;
  console.log({ requestData });

  const [state, setState] = useState({ remove: { index: "", name: "" }, genreTitle: "" });
  const inputRef = useRef(null);
  const [modalDetails, setModalDetails] = useState({
    show: false,
    kindOf: null,
    data: { src: false, type: false, name: false },
    name: null,
    removeId: null,
    editId: null,
    editData: [],
  });
  const [removeLoading, setRemoveLoading] = useState(false);

  // ========================================================= remove data with data id
  const reqApiRemove = async (id) => {
    setRemoveLoading(true);

    if (await panelAdmin.api.deletes.banner(id)) {
      apiPageFetch(requestData.page);
      onHideModal();
    }
    setRemoveLoading(false);
  };
  // ========================================================= modal
  // ================================== modal close
  const onHideModal = () => {
    setModalDetails({ ...modalDetails, show: false, kindOf: false, removeId: null });
  };
  // ================================== modal open
  const onShowModal = (event) => {
    setModalDetails({ ...modalDetails, show: true, kindOf: event.kindOf, name: event?.name, editData: event?.editData, removeId: event?.removeId });
  };
  // ================================== handel modal end work
  const modalRequest = async (bool) => {
    if (bool) reqApiRemove(modalDetails.removeId);
    else onHideModal();
  };
  // ========================================================= END modal
  const optionClick = ({ _id, mission, index }) => {
    //console.log({ _id, mission });
    switch (mission) {
      case "remove":
        onShowModal({ kindOf: "question", removeId: _id });
        break;
      case "edit":
        onShowModal({ kindOf: "component", editData: requestData.docs[index] });
        break;
      default:
        break;
    }
  };

  const card = panelAdmin.utils.consts.card;

  const showDataElement = <ShowCardInformation data={card.banner(requestData?.docs)} onClick={null} optionClick={optionClick} options={{ remove: true }} />;
  // const showDataElement = <Table tableStructure={utils.json.table.slider} data={requestData} />;
  return (
    <React.Fragment>
      <ModalStructure modalRequest={modalRequest} reqApiRemove={reqApiRemove} onHideModal={onHideModal} modalDetails={modalDetails} loading={removeLoading}>
        {modalDetails.kindOf === "component" && <AddBanner propsHideModal={onHideModal} setEdit={apiPageFetch} editData={modalDetails?.editData} modalAccept={modalRequest} />}
      </ModalStructure>
      <div className="flex1">
        {showDataElement}

        <PaginationM limited={"4"} pages={requestData?.pages} activePage={requestData?.page} onClick={handelPage} />
      </div>{" "}
      {/* <CountryElement Country={CountrySearch ? CountrySearch : Country} handelPage={_handelPage} tableOnclick={_tableOnclick} handelchange={handelchange} /> */}
    </React.Fragment>
  );
};

export default BannerScreen;
