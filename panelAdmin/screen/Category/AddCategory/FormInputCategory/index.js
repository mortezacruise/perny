import React, { useState } from "react";
import Inputs from "../../../../component/UI/Inputs/Input";
import panelAdmin from "../../../..";
const FormInputCategory = (props) => {
  const { stateArray, removeHandel, state, _onSubmited, inputChangedHandler, checkSubmitted, showModal, staticTitle } = props;

  return (
    <form onSubmit={(e) => e.preventDefault()}>
      {stateArray.map((formElement) => {
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;
        let changed, accepted, progress, disabled;
        let value = formElement.config.value;
        disabled = false;
        const inputClasses = ["InputElement"];
        if (invalid && shouldValidate && touched) inputClasses.push("Invalid");
        let FormManagement = panelAdmin.utils.operation.FormManagement({ formElement, showModal, inputChangedHandler, removeHandel, staticTitle });
        let form = <Inputs invalid={invalid} shouldValidate={shouldValidate} touched={touched} progress={progress} checkSubmitted={checkSubmitted} {...FormManagement} />;

        return form;
      })}
    </form>
  );
};

export default FormInputCategory;
