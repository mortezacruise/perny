import React, { Fragment } from "react";
import ModalStructure from "../../../../../component/UI/Modals/ModalStructure";
import Gallery from "../../../../../../pages/panelAdmin/gallery";

const ModalOptionCategory = (props) => {
  const { inputChangedHandler, modalRequest, onHideModal, modalDetails, imageAccepted, acceptedImageFinal, acceptedImage } = props;
  return (
    <ModalStructure modalRequest={modalRequest} reqApiRemove={null} onHideModal={onHideModal} modalDetails={modalDetails}>
      {modalDetails.kindOf === "showGallery" && (
        <Fragment>
          <Gallery clickedParent={(value) => inputChangedHandler({ name: modalDetails.name, value })} acceptedCardInfo={{ handelAcceptedImage: acceptedImage, acceptedCard: imageAccepted }} />
          <div className="btns-container">
            <button disabled={!imageAccepted} className="btns btns-primary" onClick={(e) => acceptedImageFinal(e)}>
              تایید{" "}
            </button>
            <button className="btns btns-warning" onClick={onHideModal}>
              بستن{" "}
            </button>
          </div>
        </Fragment>
      )}
    </ModalStructure>
  );
};

export default ModalOptionCategory;
