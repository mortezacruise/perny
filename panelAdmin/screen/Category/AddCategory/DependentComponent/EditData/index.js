import React from "react";

const EditData = (props) => {
  const { editData, stateArray, inputChangedHandler, setStaticTitle } = props;
  let arrayData = [];
  if (editData)
    for (const key in editData)
      for (let index = 0; index < stateArray.length; index++) {
        if (stateArray[index].id === key)
          if (key === "category") arrayData.push({ name: key, value: editData[key] ? editData[key]._id : "" });
          else if (key === "parent") {
            if (editData[key]) setStaticTitle({ value: editData[key] ? editData[key].title : "", name: key });
            arrayData.push({ name: key, value: editData[key] ? editData[key]._id : "" });
          } else arrayData.push({ name: key, value: editData[key] ? editData[key] : editData[key] ? editData[key] : "" });
      }
  ////console.log({ arrayData });

  if (arrayData.length > 0) inputChangedHandler(arrayData, false, { value: editData.parentType });
};

export default EditData;
