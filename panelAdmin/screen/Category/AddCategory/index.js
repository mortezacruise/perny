import React, { useRef, useEffect, useState, Fragment } from "react";
import { post, put } from "../../../api";
import FormInputCategory from "./FormInputCategory";
import panelAdmin from "../../..";
import LoadingDot1 from "../../../component/UI/Loadings/LoadingDot1";
import Submitted from "./DependentComponent/Submitted";
import EditData from "./DependentComponent/EditData";
import ModalOptionCategory from "./DependentComponent/ModalOptionCategory";

const AddCategory = (props) => {
  const { editData, setEdit, propsHideModal } = props;
  const states = panelAdmin.utils.consts.states;
  const onChanges = panelAdmin.utils.onChanges;
  const [data, setData] = useState({ ...states.addCategory });
  const [state, setState] = useState({ progressPercentImage: null, remove: { value: "", name: "" } });
  const [Loading, setLoading] = useState(false);
  const [submitLoading, setSubmitLoading] = useState(false);
  const [modalDetails, setModalDetails] = useState({
    show: false,
    kindOf: null,
    data: { src: false, type: false, name: false },
    name: null,
    removeId: null,
    editId: null,
    editData: [],
  });
  const [imageAccepted, setImageAccepted] = useState();
  const [checkSubmitted, setCheckSubmitted] = useState(false);
  const [staticTitle, setStaticTitle] = useState(false);
  // ========================================================= change edit data structure for state
  useEffect(() => {
    EditData({ editData, stateArray, inputChangedHandler, setStaticTitle });
  }, [editData]);
  // ========================================================= modal

  // ================================== modal close
  const onHideModal = () => {
    setModalDetails({ ...modalDetails, show: false, kindOf: false, removeId: null });
  };
  // ================================== modal open
  const onShowModal = (event) => {
    setModalDetails({ ...modalDetails, show: true, kindOf: event.kindOf, name: event?.name, editData: event?.editData, removeId: event?.removeId });
  };
  // ================================== handel modal end work
  const modalRequest = async (bool) => {
    if (bool) inputChangedHandler({ name: state.remove.name, value: state.remove.value });
    onHideModal();
  };
  // ========================================================= END modal
  const removeHandel = (value, name) => {
    onShowModal({ kindOf: "question" });
    setState({ ...state, remove: { value, name } });
  };
  // ========================================================= accepted image for add data
  const acceptedImage = ({ index, data }) => {
    let images = data.image.value;
    imageAccepted === images ? setImageAccepted(null) : setImageAccepted(images);
  };
  const acceptedImageFinal = () => {
    onHideModal();
    inputChangedHandler({ name: "image", value: imageAccepted });
  };
  // ========================================================= End accepted image for add data
  // ============================= submited
  const _onSubmitted = async (e) => panelAdmin.utils.operation.submitted({ setSubmitLoading, data, editData, put: put.category, post: post.category, setData, states: states.addCategory, setEdit, propsHideModal, setCheckSubmitted, checkSubmitted });

  const checkValid = () => panelAdmin.utils.operation.formTouchChange({ data, setData });

  // ========================= End submited =================
  const inputChangedHandler = async (event) => await onChanges.globalChange({ event, data, setData, setState, setLoading, imageType: "" });

  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  let form = <FormInputCategory {...{ removeHandel, stateArray, data, state, setData, Loading, setLoading, inputChangedHandler, checkSubmitted, staticTitle }} showModal={onShowModal} />;
  return (
    <div className="countainer-main">
      <ModalOptionCategory {...{ inputChangedHandler, modalRequest, onHideModal, modalDetails, imageAccepted, acceptedImageFinal, acceptedImage }} />
      <div className="form-countainer">
        <div className="row-give-information">
          {form}
          <div className="btns-container">
            <button className="btns btns-primary" disabled={submitLoading} onClick={data.formIsValid ? _onSubmitted : checkValid}>
              {submitLoading ? <LoadingDot1 width="1.5rem" height="1.5rem" /> : editData ? data.formIsValid ? "ثبت تغییرات" : "تغیراتی مشاهده نمی شود" : data.formIsValid ? "افزودن" : "اطلاعات کامل نمی باشد"}
            </button>{" "}
            {editData ? (
              <button className="btns btns-warning" onClick={propsHideModal}>
                {"بستن"}
              </button>
            ) : (
              ""
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddCategory;
