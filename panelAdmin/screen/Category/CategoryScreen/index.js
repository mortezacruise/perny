import React, { useEffect, useState, Fragment, useRef } from "react";
import ShowCardInformation from "../../../component/cards/ShowCardInformation";
import panelAdmin from "../../..";
import PaginationM from "../../../component/UI/PaginationM";
import ModalStructure from "../../../component/UI/Modals/ModalStructure";
import AddCategory from "../AddCategory";

const CategoryScreen = (props) => {
  const { requestData, apiPageFetch } = props;
  const [modalDetails, setModalDetails] = useState({
    show: false,
    kindOf: null,
    name: null,
    removeId: null,
    editId: null,
    editData: [],
  });
  const [removeLoading, setRemoveLoading] = useState(false);
  // const CurrentPage = info?.page || "1";
  const strings = panelAdmin.values.apiString;
  // const URL = strings.IMAGE + "/" + CurrentPage;

  // ========================================================= remove data with data id
  const reqApiRemove = async (id) => {
    setRemoveLoading(true);
    if (await panelAdmin.api.deletes.category(id)) {
      apiPageFetch(requestData.page);
      onHideModal();
    }
    setRemoveLoading(false);
  };
  // ========================================================= modal
  // ================================== modal close
  const onHideModal = () => {
    setModalDetails({ ...modalDetails, show: false, kindOf: false, removeId: null });
  };
  // ================================== modal open
  const onShowModal = (event) => {
    setModalDetails({ ...modalDetails, show: true, kindOf: event.kindOf, name: event?.name, editData: event?.editData, removeId: event?.removeId });
  };
  // ================================== handel modal end work
  const modalRequest = async (bool) => {
    if (bool) reqApiRemove(modalDetails.removeId);
    else onHideModal();
  };
  // ========================================================= END modal
  const card = panelAdmin.utils.consts.card;
  const optionClick = ({ _id, mission, index }) => {
    ////console.log({ _id, mission });
    switch (mission) {
      case "remove":
        onShowModal({ kindOf: "question", removeId: _id });
        break;
      case "edit":
        onShowModal({ kindOf: "component", editData: requestData.docs[index] });
        break;
      default:
        break;
    }
  };
  const showDataElement = <ShowCardInformation data={card.category(requestData.docs)} onClick={null} optionClick={optionClick} options={{ remove: true, edit: true }} />;
  // const showDataElement = <Table tableStructure={utils.json.table.category} data={requestData} />;
  return (
    <React.Fragment>
      <ModalStructure modalRequest={modalRequest} reqApiRemove={reqApiRemove} onHideModal={onHideModal} modalDetails={modalDetails} loading={removeLoading}>
        {modalDetails.kindOf === "component" && <AddCategory propsHideModal={onHideModal} setEdit={apiPageFetch} editData={modalDetails?.editData} modalAccept={modalRequest} />}
      </ModalStructure>
      <div className="flex1">
        {showDataElement}
        <PaginationM limited={"4"} pages={requestData?.pages} activePage={requestData?.page} onClick={apiPageFetch} />
      </div>
    </React.Fragment>
  );
};

export default CategoryScreen;
