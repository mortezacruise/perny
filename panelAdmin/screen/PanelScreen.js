import React, { useState, useEffect } from "react";
import SideMenu from "../component/SideMenu";
import Header from "../component/Header";
import BackgrandCover from "../component/UI/BackgrandCover";
import { ToastContainer } from "react-toastify";
import reducer from "../../_context/reducer";
import WithErrorHandler from "../utils/adminHoc/WithErrorHandler";
import panelAdmin from "..";
import { useRouter } from "next/router";

const OptionReducer = reducer.panelAdminReducer.optionReducerProvider;
const PanelScreen = (props) => {
  const [sidebarToggle, setSidebarToggle] = useState(false);
  const router = useRouter();
  const _handelSidebarToggle = () => {
    setSidebarToggle(!sidebarToggle);
  };
  useEffect(() => {
    // if (!panelAdmin.utils.authorization()) router.push("/panelAdmin/login");
    // else
    if (router.asPath === "/panelAdmin" || router.asPath === "/panelAdmin/") router.push("/panelAdmin/dashboard");
  });
  return (
    <OptionReducer>
      <div className={`panelAdmin-wrapper  ${sidebarToggle ? "fadeIn" : "fadeOut"}`}>
        <div style={{ direction: "rtl", display: "flex" }}>
          <SideMenu />
          <div className="panelAdmin-container">
            <Header _handelSidebarToggle={_handelSidebarToggle} />
            <div className="panelAdmin-content">{props.children}</div>
          </div>
        </div>
        <BackgrandCover fadeIn={sidebarToggle} onClick={_handelSidebarToggle} />
        <ToastContainer rtl />
      </div>
    </OptionReducer>
  );
};

export default WithErrorHandler(PanelScreen);
