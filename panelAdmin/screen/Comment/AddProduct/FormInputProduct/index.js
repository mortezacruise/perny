import React, { useState, useEffect } from "react";
import Inputs from "../../../../component/UI/Inputs/Input";
import { get } from "../../../../api";
import panelAdmin from "../../../..";
const FormInputProduct = (props) => {
  const { stateArray, removeHandel, state, _onSubmited, inputChangedHandler, checkSubmited, showModal } = props;
  const [Category, setCategory] = useState();

  const accept = (event) => inputChangedHandler(event);

  useEffect(() => {
    getCategoryApi();
  }, []);
  const getCategoryApi = async () => {
    const catData = await get.categories();
    setCategory(catData.data);
  };

  // =========================================================================== SEARCH STRUCTURE FOR DROP DOWN

  // =========================== FOR DROP DOWN CATEGORY ===========================
  let categoryData = [];
  if (Category)
    for (const index in Category)
      categoryData.push({
        value: Category[index]._id,
        title: Category[index].name,
        image: Category[index].image,
      });
  // =========================================================================== FORM
  return (
    <form onSubmit={(e) => e.preventDefault()}>
      {stateArray.map((formElement, index) => {
        // ================================= VARIABLE VALUES
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;
        const inputClasses = ["InputElement"];
        // ================================= CHANGE ELEMENT AMOUNTS
        if (invalid && shouldValidate && touched) inputClasses.push("Invalid");
        // ================================= INPUTS
        let FormManagement = panelAdmin.utils.operation.FormManagement({ categoryData, formElement, showModal, inputChangedHandler, accept, removeHandel });
        let form = <Inputs invalid={invalid} shouldValidate={shouldValidate} touched={touched} {...FormManagement} />;

        return form;
      })}
    </form>
  );
};

export default FormInputProduct;
