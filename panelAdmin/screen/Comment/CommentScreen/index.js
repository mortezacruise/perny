import React, { useState, useEffect } from "react";
import panelAdmin from "../../..";
import CommentTableElement from "./CommentTableElement";
// import AddComment from "../AddComment";
import ModalStructure from "../../../component/UI/Modals/ModalStructure";
const CommentScreen = (props) => {
  const { apiPageFetch, onDataSearch, requestData, loadingApi } = props;
  const [removeLoading, setRemoveLoading] = useState(false);
  const [modalDetails, setModalDetails] = useState({
    show: false,
    kindOf: null,
    data: null,
    name: null,
    removeId: null,
    editId: null,
    editData: [],
  });
  // ========================================================= remove data with data id
  const reqApiRemove = async (id) => {
    setRemoveLoading(true);
    if (await panelAdmin.api.deletes.comment(id)) {
      apiPageFetch(requestData.page);
      onHideModal();
    }
    setRemoveLoading(false);
  };
  // ========================================================= End remove data with data id
  // ========================================================= modal
  // ================================== modal close
  const onHideModal = () => {
    setModalDetails({ ...modalDetails, show: false, kindOf: false, removeId: null });
  };
  // ================================== modal open
  const onShowModal = (event) => {
    setModalDetails({ ...modalDetails, show: true, ...event });
  };
  // ================================== handel modal end work
  const modalRequest = async (bool) => {
    if (bool) reqApiRemove(modalDetails.removeId);
    else onHideModal();
  };
  // ========================================================= END modal
  const acceptedComment = (id) => {
    panelAdmin.api.patch.comment(id);
  };
  // ========================================================= table handel icon click
  const tableOnclick = (index, kindOf) => {
    console.log({ index, kindOf });
    switch (kindOf) {
      case "remove":
        onShowModal({ kindOf: "question", removeId: requestData.docs[index]._id });
        break;
      case "edit":
        onShowModal({ kindOf: "component", editData: requestData.docs[index] });
        break;
      case "showModal":
        onShowModal({ kindOf: "showTitle", showData: requestData.docs[index] });
        break;
      case "accept":
        acceptedComment({ id: requestData.docs[index]._id });
        // onShowModal({ kindOf: "showTitle", showData: requestData.docs[index] });
        break;
      default:
        break;
    }
  };
  // ========================================================= End table handel click
  const searchData = (e) => {
    onDataSearch("", e.target.value);
  };
  return (
    <React.Fragment>
      <ModalStructure {...{ modalRequest, reqApiRemove, onHideModal, modalDetails }} loading={removeLoading}>
        {modalDetails.kindOf === "showTitle" && (
          <div>
            <span>{"متن نظر :"}</span>
            <hr />
            <p style={{ fontSize: "0.8rem", color: "#5f5d5dcc" }}> {modalDetails.showData.content}</p>
          </div>
        )}
      </ModalStructure>
      <CommentTableElement requestData={requestData} handelPage={apiPageFetch} tableOnclick={tableOnclick} handelChange={searchData} />
    </React.Fragment>
  );
};

export default CommentScreen;
