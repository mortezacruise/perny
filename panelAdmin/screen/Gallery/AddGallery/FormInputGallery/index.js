import React, { useState } from "react";
import Inputs from "../../../../component/UI/Inputs/Input";
import panelAdmin from "../../../..";
const FormInputGallery = (props) => {
  const { stateArray, removeHandel, state, _onSubmited, inputChangedHandler, checkSubmited } = props;

  const dropDown = panelAdmin.utils.consts.galleryConstants();
  let dropDownData = [];
  for (const index in dropDown)
    dropDownData.push({
      value: dropDown[index].value,
      title: dropDown[index].title,
    });

  return (
    <form onSubmit={(e) => e.preventDefault()}>
      {stateArray.map((formElement) => {
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;
        let changed, accepted, progress;

        const inputClasses = ["InputElement"];
        if (invalid && shouldValidate && touched) inputClasses.push("Invalid");
        else if (formElement.id === "image") {
          progress = state.progressPercentImage;
        }
        changed = (e) =>
          inputChangedHandler({
            value: e.currentTarget.value,
            name: formElement.id,
            type: e.currentTarget.type,
            files: e.currentTarget.files,
          });
        accepted = (value) => inputChangedHandler({ value: value, name: formElement.id });

        let form = (
          <Inputs
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            invalid={invalid}
            shouldValidate={shouldValidate}
            touched={touched}
            changed={changed}
            accepted={accepted}
            removeHandel={(index) => removeHandel(index, formElement.id)}
            label={formElement.config.label}
            progress={progress}
            checkSubmited={checkSubmited}
            dropDownData={dropDownData}
          />
        );

        return form;
      })}
    </form>
  );
};

export default FormInputGallery;
