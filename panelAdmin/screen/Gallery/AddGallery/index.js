import React, { useRef, useEffect, useState, Fragment } from "react";
import ModalBox from "../../../component/UI/Modals/ModalBox";
import ModalTrueFalse from "../../../component/UI/Modals/ModalTrueFalse";
import FormInputGallery from "./FormInputGallery";
import { post } from "../../../api";
import panelAdmin from "../../..";
const AddGallery = ({ onHideModal, getApi, data, setData }) => {
  const states = panelAdmin.utils.consts.states;
  const onChanges = panelAdmin.utils.onChanges;
  // const [data, setData] = useState({ ...states.addGallery });
  const [state, setState] = useState({
    progressPercentImage: null,
    progressPercentSongs: null,
    remove: { value: "", name: "" },
  });
  const [Loading, setLoading] = useState(false);
  const [Modal, setModal] = useState({
    show: false,
  });
  const [checkSubmited, setCheckSubmited] = useState(false);
  // ======================================== modal

  // ========================= End modal =================
  // ============================= submited
  const _onSubmited = async (e) => {
    setCheckSubmited(!checkSubmited);
    e.preventDefault();
    const formData = {};
    for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
    // const initialStateTermsOfUse = updateObject(states.addGallery.Form["termsOfUse"], { value: [] });
    // const initialStateFeatures = updateObject(states.addGallery.Form["features"], { value: [] });
    // const updatedForm = updateObject(states.addGallery.Form, { ["TermsOfUse"]: initialStateTermsOfUse, ["features"]: initialStateFeatures });
    // if (await post.gallery(formData))
    onHideModal();
    setData({ ...states.addGallery });
  };
  // ========================= End submited =================
  // ============================= remove
  const __returnPrevstep = (value) => {
    // onHideModal();
    setState({ ...state, remove: { value: "", name: "" } });
    if (value)
      inputChangedHandler({
        name: state.remove.name,
        value: state.remove.value,
      });
  };
  const removeHandel = (value, name) => {
    // onShowlModal();
    setState({ ...state, remove: { value, name } });
  };
  // =========================== End remove  ====================
  const inputChangedHandler = async (event) => {
    await onChanges.globalChange({
      event,
      data,
      setData,
      setState,
      setLoading,
      fileName: data.Form.imageName.value,
    });
  };
  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  let form = <FormInputGallery removeHandel={removeHandel} stateArray={stateArray} data={data} state={state} setData={setData} Loading={Loading} setLoading={setLoading} inputChangedHandler={inputChangedHandler} checkSubmited={checkSubmited} />;
  return (
    <div className=" centerAll ">
      <ModalBox onHideModal={onHideModal} showModal={Modal.show}>
        <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />
      </ModalBox>
      <div className="form-countainer shadowUnset">
        {/* <div className="form-subtitle">افزودن عکس جدید</div> */}
        <div className="row-give-information">
          {form}
          {/* <div className="btns-container">
            <button
              className="btns btns-primary"
              disabled={!data.formIsValid}
              onClick={_onSubmited}
            >
              افزودن{" "}
            </button>
          </div> */}
        </div>
      </div>
    </div>
  );
};

export default AddGallery;
