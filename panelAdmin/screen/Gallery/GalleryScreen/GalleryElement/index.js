import React, { useState } from "react";
import Pageination from "../../../../component/UI/Pagination";
import DropdownBoot from "../../../../component/UI/Inputs/DropdownBoot";
import panelAdmin from "../../../..";
import IsNull from "../../../../component/UI/IsNull";

const constants = panelAdmin.values.constants;

const GalleryElement = (props) => {
  const { Gallery, handelPage, imgaeActived, clickedImage, showModal, state, setState } = props;

  const dropDown = [
    { value: constants.ALBUM, title: "آلبوم ها" },
    { value: constants.PLAY_LIST, title: " لیست پخش" },
    { value: constants.FLAG, title: "کشور ها " },
    { value: constants.SONG, title: "آهنگ ها" },
    { value: constants.ARTIST, title: "خواننده ها" },
    { value: constants.MOOD, title: "حالت" },
  ];
  let dropDownData = [];
  for (const index in dropDown)
    dropDownData.push({
      value: dropDown[index].value,
      title: dropDown[index].title,
    });

  const clickedDropDone = (value, title) => {
    setState({ ...state, valueEn: value, valueFa: title });
  };
  return (
    <div className={`${clickedImage ? "" : "countainer-main  "}`}>
      <div className="main-head">
        <div> {"عکس های سایت"}</div>
        <div>
          {" "}
          <div className="subtitle-elements">
            <div onClick={() => showModal({ work: "add" })} className="btns btns-add addImageBtn centerAll ">
              {"بارگزاری عکس"}
            </div>
            <div className="btns-container">
              <DropdownBoot dropDownData={dropDownData} accepted={clickedDropDone} value={state.valueFa} />
            </div>
          </div>
        </div>
      </div>
      <div className="show-image-wrapper row ">
        {Gallery && Gallery.docs && Gallery.docs.length ? (
          Gallery.docs.map((gallery, index) => {
            ////console.log({ child: gallery });

            return (
              <div key={"gallery-" + index} className="card-image-gallery  col-md-3 col-lg-3 col-5 mx-1">
                <img className={imgaeActived && imgaeActived.includes(gallery ? gallery.href : "") ? "actived" : ""} onClick={clickedImage ? () => clickedImage(gallery.href) : null} key={gallery.title} src={gallery.href} alt={gallery.title} />
                <div className="galleryTitle">
                  {" "}
                  <p>{gallery.title}</p>
                </div>
              </div>
            );
          })
        ) : (
          <IsNull title={"خالی میباشد"} />
        )}
      </div>
      <img />

      {Gallery && Gallery.pages >= 2 && <Pageination limited={"3"} pages={Gallery ? Gallery.pages : ""} activePage={Gallery ? Gallery.page : ""} onClick={handelPage} />}
    </div>
  );
};

export default GalleryElement;
