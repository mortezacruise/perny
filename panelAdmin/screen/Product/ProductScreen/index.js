import React, { useState } from "react";
import panelAdmin from "../../..";
import ProductTableElement from "./ProductTableElement";
import AddProduct from "../AddProduct";
import ModalStructure from "../../../component/UI/Modals/ModalStructure";
import tableClick from "./DependentComponent/tableClick";
const ProductScreen = (props) => {
  const { apiPageFetch, onDataSearch, requestData } = props;
  const [removeLoading, setRemoveLoading] = useState(false);
  const [modalDetails, setModalDetails] = useState({
    show: false,
    kindOf: null,
    data: null,
    name: null,
    removeId: null,
    editId: null,
    editData: [],
  });
  // ========================================================= remove data with data id
  const reqApiRemove = async (id) => {
    setRemoveLoading(true);
    if (await panelAdmin.api.deletes.product(id)) {
      apiPageFetch(requestData.page);
      onHideModal();
    }
    setRemoveLoading(false);
  };
  // ========================================================= disable data with data id
  const disableResData = async ({ data }) => {
    if (await panelAdmin.api.put.product({ data: { ...data, ["isDisable"]: !data.isDisable }, id: data._id })) apiPageFetch(requestData.page);
  };
  // ========================================================= End remove data with data id
  // ========================================================= modal
  // ================================== modal close
  const onHideModal = () => setModalDetails({ ...modalDetails, show: false, kindOf: false, removeId: null });
  // ================================== modal open
  const onShowModal = (event) => setModalDetails({ ...modalDetails, show: true, kindOf: event.kindOf, name: event?.name, editData: event?.editData, removeId: event?.removeId });
  // ================================== handel modal end work
  const modalRequest = async (bool) => (bool ? reqApiRemove(modalDetails.removeId) : onHideModal());
  // ========================================================= END modal
  // ========================================================= table handel icon click
  const tableOnclick = (index, kindOf) => tableClick({ index, kindOf, requestData, onShowModal, disableResData });
  // ========================================================= End table handel click
  const searchData = (e) => onDataSearch("", e.target.value);
  return (
    <React.Fragment>
      <ModalStructure {...{ modalRequest, reqApiRemove, onHideModal, modalDetails }} loading={removeLoading}>
        {modalDetails.kindOf === "component" && <AddProduct propsHideModal={onHideModal} setEdit={apiPageFetch} editData={modalDetails?.editData} modalAccept={modalRequest} />}
      </ModalStructure>
      <ProductTableElement requestData={requestData} handelPage={apiPageFetch} tableOnclick={tableOnclick} handelChange={searchData} />
    </React.Fragment>
  );
};

export default ProductScreen;
