const tableClick = ({ index, kindOf, requestData, onShowModal, disableResData }) => {
  switch (kindOf) {
    case "remove":
      onShowModal({ kindOf: "question", removeId: requestData.docs[index]._id });
      break;
    case "edit":
      onShowModal({ kindOf: "component", editData: requestData.docs[index] });
      break;
    case "disable":
      disableResData({ data: requestData.docs[index] });
      break;
    default:
      break;
  }
};

export default tableClick;
