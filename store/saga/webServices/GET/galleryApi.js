import axios from "axios";
import { put } from "redux-saga/effects";
import actions from "../../../actions";

export function* galleryData({ data }) {
  try {
    const res = yield axios.get(`https://rimtal.com/api/v1/admin/gallery/${data.type}/${data.page} `);
    ////console.log({ res });

    yield put(actions.reduxActions.setGalleryData(res.data)); //********** Level 5 **********//
  } catch (err) {
    if (!err.response) yield put(actions.reduxActions.setFailure(1090));
    yield put(actions.reduxActions.setFailure(err.response.data.CODE)); //********** Level 6 **********//
  }
}
