import axios from "axios";
import { put } from "redux-saga/effects";
import actions from "../../../actions";

export function* homeData() {
  ////console.log("home get khord");

  try {
    const res = yield axios.get("https://rimtal.com/api/v1/home");
    ////console.log({ res });

    yield put(actions.reduxActions.setHomeData(res.data)); //********** Level 5 **********//
  } catch (err) {
    if (!err.response) yield put(actions.reduxActions.setFailure(1090));
    yield put(actions.reduxActions.setFailure(err.response.data.CODE)); //********** Level 6 **********//
  }
}
