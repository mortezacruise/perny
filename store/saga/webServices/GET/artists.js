import axios from "axios";
import { put } from "redux-saga/effects";
import actions from "../../../actions";
import panelAdmin from "../../../../panelAdmin";

export function* artistData({ page }) {
  try {
    const res = yield panelAdmin.api.get.artists({ page });
    ////console.log({ resArtistData: res });
    yield put(actions.reduxActions.setArtistData(res.data));
  } catch (err) {
    ////console.log({ errArtistData: err });
    if (!err.response) yield put(actions.reduxActions.setFailure(1090));
    yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  }
}

export function* artistSearchData({ page, title }) {
  try {
    const res = yield panelAdmin.api.get.artistSearch({ page, title });
    ////console.log({ resArtistSearchData: res });

    yield put(actions.reduxActions.setSearchArtistData(res.data));
  } catch (err) {
    ////console.log({ errArtistSearchData: err });

    if (!err.response) yield put(actions.reduxActions.setFailure(1090));
    yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
