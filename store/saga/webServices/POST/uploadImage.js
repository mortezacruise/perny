import axios from "axios";
import { put } from "redux-saga/effects";
import actions from "../../../actions";
import panelAdmin from "../../../../panelAdmin";

export function* uploadImage(props) {
  ////console.log("uploaaaaaaaaad omad");

  const { imageName, type, files } = props;
  ////console.log({ uploadUploadImage: props });

  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.strings;
  const settings = {
    onUploadProgress: (progressEvent) => {
      let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);

      // setState((prev) => ({ ...prev, progressPercentImage: percentCompleted }));
      // yield put(actions.reduxActions.uploadImageProcess());
      ////console.log(percentCompleted);
    },
    cancelToken: source.token,
  };
  const URL = strings.ApiString.IMAGE_UPLOAD;
  const formData = new FormData();
  formData.append("imageName", imageName);
  formData.append("imageType", type);
  formData.append("image", files);

  try {
    const res = yield axios.post(URL, formData, settings);
    return res;
    // yield put(actions.reduxActions.setUploadImage(res.data));
  } catch (err) {
    ////console.log({ err });

    if (!err.response) yield put(actions.reduxActions.setFailure(1090));
    // yield put(actions.reduxActions.setFailure(err.response.data.CODE));
  }
}
