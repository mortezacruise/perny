import atRedux from "../../actionTypes/redux";

export function increment() {
  return { type: atRedux.INCREMENT };
}
// // =================================================== NAVBAR
// export function setPageName(data) {
//   return { type: atRedux.SET_PAGE_NAME, data };
// }
// =================================================== HOME
export function setHomeData(data) {
  return { type: atRedux.SET_HOME_DATA, data };
}
// =================================================== ERROR
export function setFailure(error) {
  return { type: atRedux.ADD_FAILURE, error };
}
// // =================================================== UPLOAD
// export function setUploadImage(data) {
//   return { type: atRedux.SET_UPLOAD_IMAGE, data };
// }
// // =================================================== GALLERY
// export function setGalleryData(data) {
//   return { type: atRedux.SET_GALLERY_DATA, data };
// }
// export function changeAddGalleryData(data) {
//   return { type: atRedux.CHANGE_ADD_GALLERY_DATA, data };
// }
// // =================================================== END GALLERY
// // =================================================== ARTIST
// export function setArtistData(data) {
//   return { type: atRedux.SET_ARTIST_DATA, data };
// }
// export function setSearchArtistData(data) {
//   return { type: atRedux.SET_SEARCH_ARTIST_DATA, data };
// }
// export function startGetArtist(data) {
//   return { type: atRedux.START_ARTIST_DATA, data };
// }
// export function startSearchArtist(data) {
//   return { type: atRedux.START_SEARCH_ARTIST_DATA, data };
// }
// // ================================================= END ARTIST
