import { applyMiddleware, createStore } from "redux";
import createSagaMiddleware from "redux-saga";

import rootReducer from "./reducer";
import * as watchers from "./saga";
import panelAdmin from "../panelAdmin";
const allWatchers = { ...watchers, ...panelAdmin.saga };
////console.log({ allWatchers });

const bindMiddleware = (middleware) => {
  if (process.env.NODE_ENV !== "production") {
    const { composeWithDevTools } = require("redux-devtools-extension");
    return composeWithDevTools(applyMiddleware(...middleware));
  }
  return applyMiddleware(...middleware);
}; //for redux devTools

function configureStore() {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(rootReducer, bindMiddleware([sagaMiddleware]));

  for (let watcher in allWatchers) {
    store.sagaTask = sagaMiddleware.run(allWatchers[watcher]);
  }

  return store;
}

export default configureStore;
