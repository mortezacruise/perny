import useApiRequest from "./useApiRequest";
import useOnScreen from "./useOnScreen";

const lib = { useApiRequest, useOnScreen };
export default lib;
