import contextParent from "../../";
// ========================================================  REDUCER

export const optionReducer = (state, action) => {
  switch (action.type) {
    case "SET_PAGE_NAME":
      return { ...state, pageName: action.payload };
    default:
      return state;
  }
};
// ========================================================  DISPATCH
const changePageName = (dispatch) => (title) => {
  ////console.log({ dispatch, title });

  dispatch({ type: "SET_PAGE_NAME", payload: title });
};

export const { Provider, Context } = contextParent(
  optionReducer,
  {
    changePageName,
  },
  {
    pageName: "",
  }
);
