import axios from '../userBaseUrl';
import website from '../..';

const product = (param) => {
  const URL = `${website.values.apiString.PRODUCT + '/' + param.id}`;

  return axios
    .get(URL)
    .then((res) => {
      console.log({ res });

      return res.data;
    })
    .catch((er) => {
      console.log({ er });

      return false;
    });
};

export default product;
