import axios from "../userBaseUrl";
import website from "../..";

const homeScreen = () => {
  const URL = website.values.apiString.HOME_SCREEN;

  return axios
    .get(URL)
    .then((res) => res)
    .catch((er) => false);
};

export default homeScreen;
