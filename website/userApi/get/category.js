import axios from '../userBaseUrl';
import website from '../..';

const category = (param) => {
  const URL = `${website.values.apiString.CATEGORY + '/' + param.id}`;

  return axios
    .get(URL)
    .then((res) => {
      console.log({ res });

      return res.data;
    })
    .catch((er) => {
      console.log({ er });

      return false;
    });
};

export default category;
