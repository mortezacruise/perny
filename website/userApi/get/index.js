import homeScreen from './homeScreen';
import category from './category';
import product from './product';

const get = { homeScreen, category, product };
export default get;
