import React, { Fragment } from "react";
import HeaderMain from "../baseComponent/HeaderMain/HeaderMainCard";
import Footer from "../baseComponent/Footer/FooterCard";

const WebsiteScreen = (props) => {
  return (
    <Fragment>
      <HeaderMain />
      {props.children}
      <Footer />
    </Fragment>
  );
};

export default WebsiteScreen;
