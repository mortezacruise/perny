const MainAreaCard = () => {
  return (
    <section className="articles-main-area">
      <ul>
        <li>
          <a href="">خانه</a>
        </li>
      </ul>
      <hr />
      <div className="article-area">
        <div className="article-top-section">
          <h1>تاثیرات مثبت لبنیات بر روی بدن انسان</h1>
          <div className="time-date">
            <span>
              <i></i>
            </span>
            <span className="date"></span>
            <span>|</span>
            <span className="time"></span>
          </div>
          <div className="time-for-reading">
            <span>زمان مورد نیاز برای مطالعه:</span>
            <span>۱ دقیقه</span>
            <span>
              <i></i>
            </span>
          </div>
        </div>
        <div className="article-main-content">
          <img src="" alt="" />
          <p></p>
        </div>
        <div className="article-footer-section">
          <div className="rating">
            <span>
              <i>stars</i>
            </span>
            <span>
              <i>stars</i>
            </span>
            <span>
              <i>stars</i>
            </span>
            <span>
              <i>stars</i>
            </span>
            <span>
              <i>stars</i>
            </span>
            <p className="rate">
              <span>۴</span>
              <span>/</span>
              <span>۵</span>
              <p>
                از مجموع <span>۷</span>
                <span>رای</span>
              </p>
            </p>
          </div>
          <div className="share">
            <span className="comments">
              <i></i>
              <span>۵</span>
            </span>

            <div className="socials">
              <i></i>
              <i></i>
              <i></i>
              <i></i>
              <i></i>
            </div>
          </div>
        </div>
        <div className="article-category">
          <p>دسته بندی:</p>
          <span>اخبار</span>
        </div>
      </div>
      <div className="comments-area">
        <div className="add-comment-area">
          <h2>دیدگاه شما</h2>
        </div>
        <div className="proved-comments">
          <h4>
            ۴<span>دیدگاه</span>
          </h4>
        </div>
      </div>
    </section>
  );
};

export default MainAreaCard;
