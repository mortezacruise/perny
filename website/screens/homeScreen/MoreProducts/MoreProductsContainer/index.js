import React from "react";
import MoreProductsCard from "../MoreProductsCard";

const MoreProductsContainer = ({ products, banner }) => {
  const renderData = () => products.map((data, index) => <MoreProductsCard {...data} key={`moreProduct-${index}`} />);
  return (
    <section className="more-products">
      <div className="right-side">
        <img src={banner[0]?.image} alt={banner[0]?.parent?.name} />
      </div>
      <div className="left-side">
        <ul>{renderData()}</ul>
      </div>
    </section>
  );
};

export default MoreProductsContainer;
