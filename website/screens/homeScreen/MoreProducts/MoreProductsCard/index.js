import React from 'react';
import Link from 'next/link';

const MoreProductsCard = (props) => {
  const { image, name, _id } = props;
  return (
    <React.Fragment>
      <li>
        <figure>
          <img src={image} alt="as" />

          <figcaption>
            <Link href={`/product/[id]`} as={`/product/${_id}`}>
              <a className="details-btn">{'جزئیات'}</a>
            </Link>
          </figcaption>
        </figure>
      </li>
    </React.Fragment>
  );
};

export default MoreProductsCard;
