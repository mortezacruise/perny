import SliderContainer from "../../baseComponent/Slider/SliderContainer";
import CategoriesContainer from "./Categories/CategoriesContainer";
import ShippingDetailsCard from "./ShippingDetails/ShippingDetailsCard";
import MoreProductsContainer from "./MoreProducts/MoreProductsContainer";

const HomeScreen = ({ resData }) => {
  return (
    <div>
      <div className="base-container">
        <SliderContainer data={resData?.sliders} />
        <CategoriesContainer data={resData?.categories} />
        <ShippingDetailsCard />
        <MoreProductsContainer products={resData?.products} banner={resData?.banner} />
      </div>
    </div>
  );
};

export default HomeScreen;
