import React from 'react';
import CategoriesCard from '../CategoriesCard';
import AwesomeScroll from '../../../../../components/AwesomeScroll';

const CategoriesContainer = ({ data }) => {
  const renderData = () =>
    data?.map((info, index) => (
      <CategoriesCard {...info} key={`categories-container-${index}`} />
    ));
  return (
    <section className="product-categories">
      <AwesomeScroll>{renderData()}</AwesomeScroll>
    </section>
  );
};

export default CategoriesContainer;
