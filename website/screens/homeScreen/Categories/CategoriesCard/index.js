import React from 'react';
import Link from 'next/link';

const CategoriesCard = (props) => {
  const { _id, image, name } = props;

  return (
    <React.Fragment>
      <li>
        <figure>
          <img src={image} alt="" />
          <figcaption>
            <h2 className="title">{name}</h2>
            <Link href={`/category/[id]`} as={`/category/${_id}`}>
              <a className="details-btn">{'جزئیات'}</a>
            </Link>
          </figcaption>
        </figure>
      </li>
    </React.Fragment>
  );
};

export default CategoriesCard;
