import React, { useState } from 'react';
import ProductDesc from '../ProductDesc';
import ProductReview from '../ProductReview';

const ProductNav = () => {
  const [show, setShow] = useState(false);

  const showHandler = (e) => {
    setShow(e);
  };
  return (
    <div className="nav-wrapper">
      <div className="product-tab">
        <ul className="product-nav">
          <li>
            <button
              className={!show ? 'active' : 'disable'}
              onClick={() => showHandler(false)}
            >
              توضیحات
            </button>
          </li>
          <li>
            <button
              className={show ? 'active' : 'disable'}
              onClick={() => showHandler(true)}
            >
              نظرات
            </button>
          </li>
        </ul>
      </div>
      <div className="desc-review-wrapper">
        <div
          className={show ? 'desc-wrapper no-height' : 'desc-wrapper'}
          // style={show ? { display: 'none' } : { display: 'block' }}
        >
          <ProductDesc />
        </div>
        <div
          className={!show ? 'review-wrapper no-height' : 'review-wrapper'}

          // style={!show ? { display: 'none' } : { display: 'block' }}
        >
          <ProductReview />
        </div>
      </div>
    </div>
  );
};
export default ProductNav;
