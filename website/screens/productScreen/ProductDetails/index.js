import React, { useState, useEffect } from 'react';
import ProductSlider from '../../../reusableComponent/ProductsSlider';
import globalUtils from '../../../../globalUtils';

const ProductDetails = ({ data }) => {
  console.log({ data });

  const [newP, setNewP] = useState(false);
  const set = () => {
    data.newPrice == 0 || data.newPrice == null
      ? setNewP(false)
      : setNewP(true);
  };
  useEffect(() => {
    set();
  }, []);

  return (
    <section className="product-wraper">
      <div className="right-side">
        <div className="right-side-inner">
          <h2>{data.name}</h2>
          <div className="rating-wrapper">
            <span className="fa fa-stack">
              <i className="fa fa-star off fa-stack-2x"></i>
            </span>
            <span className="fa fa-stack">
              <i className="fa fa-star fa-stack-2x"></i>
            </span>
            <span className="fa fa-stack">
              <i className="fa fa-star fa-stack-2x"></i>
            </span>
            <span className="fa fa-stack">
              <i className="fa fa-star fa-stack-2x"></i>
            </span>
            <span className="fa fa-stack">
              <i className="fa fa-star fa-stack-2x"></i>
            </span>
            <a href="" className="review-count">
              1 نظر
            </a>
            {/* <a href="" className="write-review">
              <i className="fa fa-pencil"></i>نوشتن نظر
            </a> */}
          </div>
          <ul className="product-detail-list">
            <li>
              <span>شرکت :</span>
              <a href="#" alt="something">
                با من
              </a>
            </li>
            <li>
              <span>کد محصول :</span>1234
            </li>
            <li>
              <span>امتیاز محصول :</span>50
            </li>
            <li>
              <span>موجودی :</span>
              موجود
            </li>
            <li>
              <span>وزن محصول :</span>
              {data.weight} {data.unit}
            </li>
          </ul>
          <ul className="product-price">
            <li>
              <span className={newP ? 'line-through' : ''}>
                {globalUtils.formatMoney(data.realPrice)} ریال
              </span>
            </li>
            {!newP ? (
              ''
            ) : (
              <li>
                <h3 className="special-price">
                  {globalUtils.formatMoney(data.newPrice)} ریال
                </h3>
              </li>
            )}
          </ul>
          <div className="add-wraper">
            <label htmlFor="qty">: تعداد</label>
            <input type="phone" placeholder="1" name="qty" />
            <button>اضافه به سبد خرید</button>
          </div>
        </div>
      </div>
      <div className="left-side">
        <ProductSlider data={data.image} />
      </div>
    </section>
  );
};
export default ProductDetails;
