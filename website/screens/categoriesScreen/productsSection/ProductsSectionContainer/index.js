import React from 'react';
import ProductsSectionCard from '../ProductsSectionCard';

const ProductsSectionContainer = ({ data }) => {
  const renderProductsData = () =>
    data?.map((product) => <ProductsSectionCard {...product} />);
  return (
    <section className="products-section">
      <ul>{renderProductsData()}</ul>
    </section>
  );
};

export default ProductsSectionContainer;
