import React from 'react';
import Link from 'next/link';
import globalUtils from '../../../../../globalUtils';
const ProductsSectionCard = (props) => {
  const { _id, image, name, realPrice } = props;
  const clickProduct = () => {
    console.log({ props });
    let keyData = {};
    for (const key in props) {
      // console.log({ keyData: keyData[key], key, props: props[key] });
      // keyData[key] + "&&" + props[key];
    }
    console.log({ keyData });
  };
  return (
    <React.Fragment>
      <li onClick={clickProduct}>
        <Link
          // href={`/product?productInfo=${props}`}
          href={`/product/[id]`}
          as={`/product/${_id}`}
        >
          <a>
            <figure>
              <img src={image} alt={name} />
              <figcaption>
                <h1>{name}</h1>
                <div className="price-wrapper">
                  <h4>
                    {globalUtils.formatMoney(realPrice)}
                    <span>{' ریال '}</span>
                  </h4>
                </div>
              </figcaption>
            </figure>
          </a>
        </Link>
      </li>
    </React.Fragment>
  );
};

export default ProductsSectionCard;
