import React, { useState } from 'react';
import CategoryPanel from '..';

const CategoryPanelContainer = ({ datas }) => {
  const [button, setButton] = useState(false);
  const onClickHandler = () => {
    setButton(!button);
  };

  const renderData = () => datas.map((data) => <CategoryPanel data={data} />);
  return (
    <div className="category-panel-section ">
      <h6> دسته بندی ها</h6>
      <ul className={!button ? '' : 'show-all'}>{renderData()}</ul>
      <button onClick={onClickHandler}>
        {!button ? '+نمایش بیشتر' : '−نمایش کمتر'}
      </button>
    </div>
  );
};

export default CategoryPanelContainer;
