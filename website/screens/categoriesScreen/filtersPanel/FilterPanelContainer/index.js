import React from 'react';
import FilterPanelCard from '../FilterPanelCard';

const FilterPanelContainer = () => {
  const datas = [
    { title: 'پربازدیدترین' },
    { title: 'پرفروش ترین' },
    { title: 'محبوب ترین' },
    { title: 'جدیدترین' },
    { title: 'ازان ترین' },
    { title: ' گران ترین ' },
    { title: 'سریع ترین ارسال' },
  ];

  const renderData = () => datas.map((data) => <FilterPanelCard {...data} />);
  return (
    <div className="filter-items">
      <h6 className="filters-title">فیلترها</h6>
      <ul>{renderData()}</ul>
    </div>
  );
};

export default FilterPanelContainer;
