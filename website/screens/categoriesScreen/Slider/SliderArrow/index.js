import React from "react";

const SliderArrow = ({ onClick }) => {
  const arrowStyles = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    zIndex: 2,
    top: "calc(50% - 15px)",
    width: 24,
    height: 24,
    cursor: "pointer",
    color: "#fff",
    fontSize: "25px",
    
    backgroundColor: "rgba(0,0,0, 0.4)",
    padding: "0.8em",
    borderRadius: "50%",
  };
  return (
    <div className="right-arrow" onClick={onClick} style={{ ...arrowStyles, right: 5 }}>
      <i className="fas fa-chevron-right"></i>
    </div>
  );
};

export default SliderArrow;
