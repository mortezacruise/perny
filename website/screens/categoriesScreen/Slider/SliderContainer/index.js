import React from 'react';
import { Carousel } from 'react-responsive-carousel';
import SliderCard from '../SliderCard';
import SliderArrow from '../SliderArrow';
import SliderArrowP from '../SliderArrowP';

// TODO => we should connect to api and get images
const SliderContainer = ({ data }) => {
  return (
    <div className="categories-slider-container my-4">
      <Carousel
        renderArrowNext={(onClickHandler, hasNext) =>
          hasNext && <SliderArrow onClick={onClickHandler} />
        }
        renderArrowPrev={(onClickHandler, hasPrev) =>
          hasPrev && <SliderArrowP onClick={onClickHandler} />
        }
        swipeable={true}
        transitionTime={1000}
        interval={5000}
        infiniteLoop={true}
        autoPlay={true}
        showThumbs={false}
        showStatus={false}
      >
        {data?.map((info, index) => {
          return <SliderCard image={info.image} key={`slider-card-${index}`} />;
        })}
      </Carousel>
    </div>
  );
};

export default SliderContainer;
