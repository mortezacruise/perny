import * as userApi from "./userApi";
import values from "./values";
const website = { userApi, values };
export default website;
