const HOME_SCREEN = '/homeScreen';
const CATEGORY = '/category';
const PRODUCT = '/product';
const apiString = {
  HOME_SCREEN,
  CATEGORY,
  PRODUCT,
};
export default apiString;
