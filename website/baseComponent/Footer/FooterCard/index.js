import React, { useState } from 'react';
import Link from 'next/link';

const Footer = () => {
  const [more, setMore] = useState(false);

  const showMore = () => {
    setMore(!more);
  };
  return (
    <footer className="base-container">
      <div className="footer-wrapper">
        <ul className="right-side">
          <li>
            <Link href="#">
              <a>درباره پرنی مارکت</a>
            </Link>
          </li>
          <li>
            <Link href="#">
              <a>درباره ما</a>
            </Link>
          </li>
          <li>
            <Link href="#">
              <a>ضوابط و شرایط استفاده</a>
            </Link>
          </li>
          <li>
            <Link href="#">
              <a>گواهینامه های پرنی</a>
            </Link>
          </li>
          <li>
            <Link href="#">
              <a>همکاری در تبلیغات</a>
            </Link>
          </li>
          <li>
            <Link href="#">
              <a>زندگی به سبک پرنی مارکت</a>
            </Link>
          </li>
        </ul>
        <div className="left-side">
          <p>
            فروشگاه آنلاین پرنی مارکت ، ایجاد خریدی رضایت بخش در فضای اینترنتی
            را رکن اصلی فعالیت های خود قرار داده<span> </span>
            <span className={more ? 'span-more show-more' : 'span-more'}>
              است . انواع کالاهای ضروری و مصرفی زندگی از مواد غذایی ، محصولات
              بهداشتی و آرایشی ، با یک کلیک در تمام نقاط ایران در اختیار شما است
              . کالاهای موجود در فروشگاه آنلاین پرنی مارکت از برند های معتبر در
              صنعت مواد غذایی ، آرایشی و بهداشتی به فروش می رسد . فروشگاه آنلاین
              پرنی مارکت با ایجاد تنوع در نوع کالا ، قیمت کالا و برند امکان
              انتخاب صحیح و آسان در خرید را به شما می دهد . ایجاد ساختار گارانتی
              و تعویض به موقع کالا و پشتیبانی مشتری ، از دیگر مزایای خرید از
              فروشگاه آنلاین پرنی مارکت محسوب می شود .
            </span>
            <div className="buttons">
              <button onClick={showMore} className="read-more">
                {more ? 'نمایش کمتر' : 'نمایش بیشتر'}
              </button>
            </div>
          </p>
        </div>
      </div>
    </footer>
  );
};
export default Footer;
