import React, { useRef, useState, useEffect } from 'react';
import Link from 'next/link';
import logo from '../../../public/images/logo.png';
const HeaderMain = () => {
  const [menuState, setMenuState] = useState(false);

  let menuBtn = useRef();
  let menu = useRef();
  const openMenu = () => {
    setMenuState(!menuState);
  };

  const closeMenu = () => {
    setMenuState(!menuState);
  };

  const bodyStyle = () => {
    if (menuState === true) {
      document.querySelector('body').style.overflowY = 'hidden';
    } else {
      document.querySelector('body').style.overflowY = 'auto';
    }
  };

  useEffect(() => {
    bodyStyle();
  }, [menuState]);

  return (
    <header className="base-container ">
      <div className="header-wrapper">
        <div
          className={menuState ? 'overlay over-show' : 'overlay over-unShow'}
        ></div>
        <div className="top-section">
          <div className="right-side-wrapper">
            <Link href="/">
              <a>
                <img src={logo} alt="perny-market" />
              </a>
            </Link>
          </div>
          <div className="left-side-wrapper">
            <div className="sign-up">
              <span>ورود/ عضویت</span>
              <span>
                <i className="fal fa-user-alt"></i>
              </span>
            </div>
            <div className="search">
              <form className="form-search">
                <i className="fal fa-search"></i>
                <input
                  className="form-search-input"
                  type="search"
                  placeholder=""
                  aria-label="Search"
                />
              </form>
            </div>
          </div>
        </div>
        <nav>
          <div className="small-device-wrapper">
            <button
              ref={(el) => (menuBtn = el)}
              onClick={openMenu}
              className="menu-btn"
            >
              <i className="far fa-bars"></i>
            </button>
            <button className="menu-btn">
              <i className="fal fa-search"></i>
            </button>
            <button className="menu-btn">
              <i className="fas fa-user-alt"></i>
            </button>
          </div>
          <ul
            ref={(el) => (menu = el)}
            className={menuState ? 'show' : 'unShow'}
          >
            <li>
              <i onClick={closeMenu} className="far fa-times"></i>
            </li>
            <li>
              <Link href="/">
                <a>
                  <h6>خانه</h6>
                </a>
              </Link>
            </li>
            <li>
              <h6>جدیدترین ها</h6>
            </li>
            <li>
              <h6>پیشنهادات ویژه</h6>
            </li>
            <li>
              <h6>تخفیف ها</h6>
            </li>
            <li>
              <h6>سوالات متداول</h6>
            </li>
            <li>
              <Link href="/articles">
                <a>
                  <h6>مقالات</h6>
                </a>
              </Link>
            </li>
            <li>
              <h6>درباره ما</h6>
            </li>
            <li>
              <h6>تماس با ما</h6>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
};

export default HeaderMain;
