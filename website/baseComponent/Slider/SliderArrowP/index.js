import React from "react";

const SliderArrowP = ({ onClick }) => {
  const arrowStyles = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    zIndex: 2,
    top: "calc(50% - 15px)",
    width: 24,
    height: 24,
    cursor: "pointer",
    color: "#fff",
    fontSize: "25px",
    backgroundColor: "rgba(0,0,0, 0.4)",
    padding: ".8em",
    borderRadius: "50%",
  };
  return (
    <div
      className="left-arrow"
      onClick={onClick}
      style={{ ...arrowStyles, left: 5 }}
    >
      <i className="fas fa-chevron-left"></i>
    </div>
  );
};

export default SliderArrowP;
