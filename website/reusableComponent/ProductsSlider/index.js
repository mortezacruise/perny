// import React from "react";
// import { CarouselProvider, Slider, ImageWithZoom, Slide, ButtonBack, ButtonNext } from "pure-react-carousel";
// import header from "../../public/images/header.jpg";

// import AliceCarousel from 'react-alice-carousel';
import { Carousel } from 'react-responsive-carousel';
import { useState } from 'react';

const ProductSlider = ({ data }) => {
  // const images = [header, header, header, header];
  console.log({ myImage: data });
  const [items, setItems] = useState(data);

  const handleOnDragStart = (e) => e.preventDefault();
  return (
    <Carousel>
      {data.map((img) => (
        <div style={{ direction: 'ltr' }}>
          <img src={img} />
        </div>
      ))}
    </Carousel>
    // <CarouselProvider
    //   naturalSlideWidth={400}
    //   naturalSlideHeight={400}
    //   totalSlides={4}
    //   className={'carousel'}
    // >
    //   <div style={{ direction: 'ltr' }}>
    //     <Slider>
    //       {/* {images.map((image, index) => (
    //         <Slide index={index}>
    //           <ImageWithZoom
    //             src={image}
    //             hasMasterSpinner={false}
    //           ></ImageWithZoom>
    //         </Slide>
    //       ))} */}
    //       <Slide>
    //         <ImageWithZoom src={data} hasMasterSpinner={false}></ImageWithZoom>
    //       </Slide>
    //     </Slider>
    //     {/* <ButtonBack className="back">
    //       <i className="fas fa-chevron-left"></i>
    //     </ButtonBack>
    //     <ButtonNext className="forward">
    //       <i className="fas fa-chevron-right"></i>
    //     </ButtonNext> */}
    //   </div>
    // </CarouselProvider>
  );
};

export default ProductSlider;
