import React from 'react';
import Footer from '../../baseComponent/Footer/FooterCard';
// import HeaderContainer from '../../baseComponent/Header/HeaderContainer';
import HeaderMain from '../../baseComponent/HeaderMain/HeaderMainCard';

const Layer = (props) => {
  return (
    <div>
      {/* <HeaderContainer /> */}
      <HeaderMain />
      {props.children}
      <Footer />
    </div>
  );
};

export default Layer;
