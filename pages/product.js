import React, { useState } from 'react';
import ProductDetails from '../website/screens/productScreen/ProductDetails';
import ProductNav from '../website/screens/productScreen/ProductNav';
import { useRouter } from 'next/router';
import website from '../website';
const ProductPage = ({ res }) => {
  const router = useRouter();
  // console.log({ router: router.query });
  const [resData, setResData] = useState(res || null);
  console.log({ ppppp: resData });

  return (
    <div className="base-container">
      <section className="product">
        <ProductDetails data={resData} />
        <ProductNav />
        {/* <ProductDesc />
        <ProductReview /> */}
      </section>
    </div>
  );
};

ProductPage.getInitialProps = async (props) => {
  console.log({ pppooopppp: props.ctx.query.productId });
  const res = await website.userApi.get.product({
    id: props.ctx.query.productId,
  });
  return { res: res };
};
ProductPage.websiteLayout = true;

export default ProductPage;
