import React, { useEffect, useState } from 'react';
import SliderContainer from '../../website/screens/categoriesScreen/Slider/SliderContainer';
import ProductsSectionContainer from '../../website/screens/categoriesScreen/productsSection/ProductsSectionContainer';

import website from '../../website';
import CategoryPanelContainer from '../../website/screens/categoriesScreen/CategoryPanel/CategoryPanelContainer';
import FilterPanelContainer from '../../website/screens/categoriesScreen/filtersPanel/FilterPanelContainer';

const Categories = ({ res }) => {
  const [resData, setResData] = useState(res || null);
  console.log({ coplete: res });

  // useEffect(() => {
  //   api();
  // }, []);
  // const api = async () => {
  //   const res = await website.userApi.get.category("5ee9d8e3586eca0cbc1f4c0e");
  // };
  return (
    <div className="base-container">
      <section className="categories-section">
        <div className="left-side-categories">
          <SliderContainer data={resData?.sliders} />
          <ProductsSectionContainer data={resData?.cat.products} />
        </div>
        <div className="right-side-categories">
          <CategoryPanelContainer datas={resData.allCat} />
          <FilterPanelContainer />
        </div>
      </section>
    </div>
  );
};
Categories.getInitialProps = async (props) => {
  console.log({ p: props.ctx.query.categoryId });
  const res = await website.userApi.get.category({
    id: props.ctx.query.id,
  });
  return { res: res };
};

Categories.websiteLayout = true;
export default Categories;
