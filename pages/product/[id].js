import axios from 'axios';

import React, { useState } from 'react';

import ProductNav from '../../website/screens/productScreen/ProductNav';
import { useRouter } from 'next/router';
import ProductDetails from '../../website/screens/productScreen/ProductDetails';

const ProductPage = ({ res }) => {
  const router = useRouter();
  // console.log({ router: router.query });
  const [resData, setResData] = useState(res || null);
  console.log({ ppppp: resData });

  return (
    <div className="base-container">
      <section className="product">
        <ProductDetails data={resData} />
        <ProductNav />
        {/* <ProductDesc />
        <ProductReview /> */}
      </section>
    </div>
  );
};

ProductPage.getInitialProps = async (props) => {
  const res = await axios.get(
    `https://pernymarket.ir/api/v1/product/${props.ctx.query.id}`
  );
  return { res: res.data };
};

ProductPage.websiteLayout = true;

export default ProductPage;
