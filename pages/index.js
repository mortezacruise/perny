import React, { useEffect } from "react";
import HomeScreen from "../website/screens/homeScreen";
import website from "../website";
// import axios from "axios";

const Home = ({ resData }) => {
  console.log({ resData });

  //   useEffect(() => {
  //     getData();
  //     console.log(getData());
  //   }, []);

  // const getData = async () => {
  //   const res = await axios
  //     .get(`https://pernymarket.ir/api/v1/banner`)
  //     .then((res) => ({ data: res }))
  //     .catch((er) => ({ error: er }));

  //   return;
  // };
  return <HomeScreen {...{ resData }} />;
};
Home.getInitialProps = async (props) => {
  const { store, isServer } = props.ctx;
  const res = await website.userApi.get.homeScreen();

  return { resData: res?.data, isServer };
};
Home.websiteLayout = true;
export default Home;
