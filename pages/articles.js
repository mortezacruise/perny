import ArticlesScreen from '../website/screens/articlesScreen';

const Articles = () => {
  return <ArticlesScreen />;
};
Articles.websiteLayout = true;
export default Articles;
