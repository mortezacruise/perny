import React, { useEffect, useContext } from "react";
import panelAdmin from "../../panelAdmin";
import AddProduct from "../../panelAdmin/screen/Product/AddProduct";
import reducer from "../../_context/reducer";
const Context = reducer.panelAdminReducer.optionReducerContext;

const addProduct = (props) => {
  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;

  useEffect(() => {
    dispatch.changePageName(panelAdmin.values.strings.ADD_PRODUCT);
  }, []);
  return <AddProduct />;
};

addProduct.getInitialProps = async (props) => {
  const { store, isServer } = props.ctx;
  return { isServer };
};
addProduct.panelAdminLayout = true;

export default addProduct;
