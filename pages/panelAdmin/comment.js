import React, { useEffect, useState, useContext, useRef } from "react";
import panelAdmin from "../../panelAdmin";
// import blogScreen from "../../panelAdmin/screen/blog/blogScreen";
import reducer from "../../_context/reducer";
import SpinnerRotate from "../../panelAdmin/component/UI/Loadings/SpinnerRotate";
import CommentScreen from "../../panelAdmin/screen/Comment/CommentScreen";
// import ArticleScreen from "../../panelAdmin/screen/Article/ArticleScreen";

const comment = (props) => {
  const strings = panelAdmin.values.apiString;
  const Context = reducer.panelAdminReducer.optionReducerContext;
  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;
  const { acceptedCardInfo, parentTrue } = props;
  const [state, setState] = useState(false);
  const [searchData, setSearchData] = useState(false);
  const [loadingApi, setLoadingApi] = useState(true);
  const CurrentPage = state?.page || "1";
  // const isSearchData = useRef(false);
  const [isSearchData, setIsSearchData] = useState(false);
  let searchTitle = useRef(null);
  useEffect(() => {
    dispatch.changePageName("پیام ها");
    apiPageFetch();
  }, []);

  const apiPageFetch = async (page) => {
    setLoadingApi(true);
    const res = await panelAdmin.api.get.comments(page || state?.page || "1");
    ////console.log({ res });
    setState(res);
    setLoadingApi(false);
  };

  const onDataSearch = async (page, value) => {
    // ////console.log({ value, page }, value && !page);
    if (value && !page) searchTitle.current = value;
    else if (!page) searchTitle.current = "";
    const resDataSearch = await panelAdmin.api.get.commentSearch(value || searchTitle.current, page || searchData?.page || "1");
    setSearchData(resDataSearch);
  };

  return (
    <>
      <CommentScreen requestData={searchData || state} onDataSearch={onDataSearch} acceptedCardInfo={acceptedCardInfo} apiPageFetch={searchData ? onDataSearch : apiPageFetch} />
      {/* {loadingApi ? (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      ) : (
        ""
      )} */}
    </>
  );
  return true;
};

comment.panelAdminLayout = true;

export default comment;
