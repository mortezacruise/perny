import React, { useEffect } from "react";
import { useRouter } from "next/router";

const PanelAdmin = (props) => {
  const router = useRouter();

  useEffect(() => {
    router.push("/panelAdmin/dashboard");
  }, []);

  // useEffect(() => {
  //   // The counter changed!
  // }, [router.query.counter]);
  return <div>Loading ...</div>;
};

export default PanelAdmin;
