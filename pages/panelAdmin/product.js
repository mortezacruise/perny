import React, { useEffect, useState, useContext, useRef } from "react";
import panelAdmin from "../../panelAdmin";
import ProductScreen from "../../panelAdmin/screen/Product/ProductScreen";
import reducer from "../../_context/reducer";
import SpinnerRotate from "../../panelAdmin/component/UI/Loadings/SpinnerRotate";

const product = (props) => {
  const strings = panelAdmin.values.apiString;
  const Context = reducer.panelAdminReducer.optionReducerContext;
  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;
  const { acceptedCardInfo, parentTrue } = props;
  const [state, setState] = useState(false);
  const [searchData, setSearchData] = useState(false);
  const [loadingApi, setLoadingApi] = useState(true);
  const CurrentPage = state?.page || "1";
  const [isSearchData, setIsSearchData] = useState(false);
  let searchTitle = useRef(null);
  useEffect(() => {
    dispatch.changePageName("محصولات");
    apiPageFetch();
  }, []);

  const apiPageFetch = async (page) => {
    setLoadingApi(true);
    const res = await panelAdmin.api.get.products(page || state?.page || "1");
    setState(res);
    setLoadingApi(false);
  };

  const onDataSearch = async (page, value) => {
    if (value && !page) searchTitle.current = value;
    else if (!page) searchTitle.current = "";
    const resDataSearch = await panelAdmin.api.get.productSearch(value || searchTitle.current, page || "1");
    setSearchData(resDataSearch);
  };

  return (
    <>
      <ProductScreen requestData={searchData || state} onDataSearch={onDataSearch} acceptedCardInfo={acceptedCardInfo} apiPageFetch={searchData ? onDataSearch : apiPageFetch} />
    </>
  );
  // return true;
};

product.panelAdminLayout = true;

export default product;
