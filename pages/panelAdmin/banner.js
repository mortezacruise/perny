import React, { useEffect, useContext, useState } from "react";
import reducer from "../../_context/reducer";
import panelAdmin from "../../panelAdmin";
import useSWR, { mutate, trigger } from "swr";
import useApiRequest from "../../lib/useApiRequest";
import globalUtils from "../../globalUtils";
import SpinnerRotate from "../../panelAdmin/component/UI/Loadings/SpinnerRotate";
import BannerScreen from "../../panelAdmin/screen/Banner/BannerScreen";

// const axios = globalUtils.axiosBase;
// const fetcher = (url) => axios(url).then((r) => r.json());

const banner = (props) => {
  const strings = panelAdmin.values.apiString;
  const Context = reducer.panelAdminReducer.optionReducerContext;
  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;
  const { acceptedCardInfo, parentTrue, isServer } = props;
  const [state, setState] = useState(false);
  const CurrentPage = state?.page || "1";
  const [loadingApi, setLoadingApi] = useState(true);
  // ======================================================== SWR
  // const { data } = useSWR(strings.IMAGE, fetcher, { initialData: resData });
  // const { data: image } = useApiRequest(strings.IMAGE + "/" + CurrentPage, { initialData: resData }, { refreshInterval: 0 });
  useEffect(() => {
    dispatch.changePageName("بنر");
    apiPageFetch();
  }, []);
  // //console.log({ data, resData });

  const apiPageFetch = async (page = 1) => {
    if (!page) return;
    setLoadingApi(true);
    const res = await panelAdmin.api.get.banners(page);
    // //console.log({ res });
    setState(res?.data);
    setLoadingApi(false);
  };

  return (
    <>
      <BannerScreen requestData={state} acceptedCardInfo={acceptedCardInfo} apiPageFetch={apiPageFetch} />
      {loadingApi ? (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      ) : (
        ""
      )}
    </>
  );
  // return true;
};
// ========================================= getInitialProps
// banner.getInitialProps = async (props) => {
//   const { banner, isServer } = props.ctx;
//   const res = await panelAdmin.api.get.banner({ page: "1" });
//   const resData = res.data;

//   return { resData, isServer };
// };
banner.panelAdminLayout = true;

export default banner;
