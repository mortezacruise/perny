import React, { useEffect, useContext } from "react";
import panelAdmin from "../../panelAdmin";
import reducer from "../../_context/reducer";
import AddBanner from "../../panelAdmin/screen/Banner/AddBanner";

const addBanner = (props) => {
  const Context = reducer.panelAdminReducer.optionReducerContext;

  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;

  useEffect(() => {
    dispatch.changePageName(panelAdmin.values.strings.ADD_BANNER);
  }, []);
  return <AddBanner />;
};
addBanner.panelAdminLayout = true;
// addBanner.getInitialProps = async (props) => {
//   const { store, isServer } = props.ctx;
//   return { isServer };
// };

export default addBanner;
