import React, { useEffect, useState, useContext, useRef } from "react";
import panelAdmin from "../../panelAdmin";
// import blogScreen from "../../panelAdmin/screen/blog/blogScreen";
import reducer from "../../_context/reducer";
import SpinnerRotate from "../../panelAdmin/component/UI/Loadings/SpinnerRotate";
import ArticleScreen from "../../panelAdmin/screen/Article/ArticleScreen";

const article = (props) => {
  const strings = panelAdmin.values.apiString;
  const Context = reducer.panelAdminReducer.optionReducerContext;
  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;
  const { acceptedCardInfo, parentTrue } = props;
  const [state, setState] = useState(false);
  const [searchData, setSearchData] = useState(false);
  const [loadingApi, setLoadingApi] = useState(true);
  const CurrentPage = state?.page || "1";
  // const isSearchData = useRef(false);
  const [isSearchData, setIsSearchData] = useState(false);
  let searchTitle = useRef(null);
  useEffect(() => {
    dispatch.changePageName("محصولات");
    apiPageFetch();
  }, []);

  const apiPageFetch = async (page) => {
    setLoadingApi(true);
    const res = await panelAdmin.api.get.articles(page || state?.page || "1");
    ////console.log({ res });
    setState(res);
    setLoadingApi(false);
  };

  const onDataSearch = async (page, value) => {
    // ////console.log({ value, page }, value && !page);
    if (value && !page) searchTitle.current = value;
    else if (!page) searchTitle.current = "";
    const resDataSearch = await panelAdmin.api.get.articleSearch(value || searchTitle.current, page || searchData?.page || "1");
    setSearchData(resDataSearch);
  };

  return (
    <>
      <ArticleScreen requestData={searchData || state} onDataSearch={onDataSearch} acceptedCardInfo={acceptedCardInfo} apiPageFetch={searchData ? onDataSearch : apiPageFetch} />
      {loadingApi ? (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      ) : (
        ""
      )}
    </>
  );
  // return true;
};

article.panelAdminLayout = true;

export default article;
