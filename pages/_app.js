import App from "next/app";
import Head from "next/head";
import "../public/styles/index.scss";

import { SWRConfig } from "swr";
import globalUtils from "../globalUtils";

class pernyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps({ ctx });
    }

    return { pageProps };
  }
  render() {
    const { Component, pageProps, store, router } = this.props;
    let body;
    // if (router.asPath.includes("/panelAdmin")) {
    //   body = (
    //     <PanelScreen router={router}>
    //       <Component {...pageProps} />
    //     </PanelScreen>
    //   );
    // }
    // else {
    //   body = (
    //     <WebsiteScreen>
    //       <Component {...pageProps} />
    //     </WebsiteScreen>
    //   );
    // }
    return (
      <div>
        <Head>
          <title>{"پرنی مارکت"}</title>
          <meta name="description" content="فروشگاه اینترنتی محصولات لبنی  ." />
          <meta name="keywords" content="ریمتال،rimtal,RIMTAL ,موسیقی و ,IPTV,موسیقی,پرداخت اجتماعی،پخش زنده،اپلیکیشن,سرگرمی," />
          <meta content="width=device-width, initial-scale=1" name="viewport" />
          <link href={"/styles/css/styles.css"} rel={"stylesheet"} />
        </Head>

        <div className="base-page">
          {globalUtils.hoc.routingHandle({ Component, pageProps, router })}
          {/* <SWRConfig value={{ refreshInterval: 0, fetcher: (...args) => axios(...args).then((r) => r.data) }}>
            {body} <ErrorHandler />
          </SWRConfig> */}
        </div>
      </div>
    );
  }
}

export default pernyApp;
