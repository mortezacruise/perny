import React, { useEffect, useRef } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import { WithUserAgentProps, withUserAgent } from "next-useragent";
const AwesomeScroll = (props) => {
  const { data, Row, ua, scrollBar } = props;

  // ////console.log({ data, Row });
  // ////console.log({ ua });

  const scrollRef = useRef(null);
  // ////console.log({ AwesomeScroll: data });

  useEffect(() => {
    if (scrollRef.current) {
      let slider;
      if (scrollBar) slider = scrollRef.current.children[0];
      else slider = scrollRef.current;
      console.log({ slider });

      let isDown = false;
      let startX;
      let scrollLeft;

      slider.addEventListener("mousedown", (e) => {
        isDown = true;
        // slider.classList.add("active");
        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
      });

      slider.addEventListener("mouseleave", (e) => {
        isDown = false;
        // slider.classList.remove("active");
        ////console.log("mouseleave");
      });

      slider.addEventListener("mouseup", () => {
        isDown = false;
        // slider.classList.remove("active");
      });

      slider.addEventListener("mousemove", (e) => {
        if (!isDown) return;
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const walk = x - startX; //scroll-fast
        // ////console.log({ x, walk }, (slider.scrollLeft = scrollLeft - walk));

        slider.scrollLeft = scrollLeft - walk;
      });
    }
  }, []);
  const _handelScrollTo = (road) => {
    console.log(road);
    console.log({ left: scrollRef.current.scrollLeft });
    console.log({ children: scrollRef.current.children[0].style.margin });

    if (road === "left")
      scrollRef.current.scrollTo({
        left: scrollRef.current.scrollLeft - scrollRef.current.children[0].clientWidth,
        behavior: "smooth",
      });
    if (road === "right")
      scrollRef.current.scrollTo({
        left: scrollRef.current.scrollLeft + scrollRef.current.children[0].clientWidth,
        behavior: "smooth",
      });
  };
  return (
    <div style={{ display: "flex", position: "relative" }}>
      <div className="slider-btn btn-right ">
        <span className="centerAll" onClick={() => _handelScrollTo("right")}>
          {"<"}
        </span>
      </div>
      {!scrollBar ? (
        <ul ref={scrollRef} className="awesome-scroll-container">
          {props.children}
        </ul>
      ) : ua && ua.isMobile ? (
        <ul className="awesome-scroll-container">{props.children}</ul>
      ) : (
        <ul ref={scrollRef} className="awesome-scroll-container">
          <PerfectScrollbar>{props.children}</PerfectScrollbar>
        </ul>
      )}
      <div className="slider-btn btn-left">
        <span className="centerAll" onClick={() => _handelScrollTo("left")}>
          {">"}
        </span>
      </div>
    </div>
  );
};
AwesomeScroll.getInitialProps = async (ctx) => {
  return { useragent: ctx.ua.source };
};
export default withUserAgent(AwesomeScroll);
