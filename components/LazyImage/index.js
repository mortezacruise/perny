import React, { useState, useEffect, useRef, Fragment } from "react";
export const LazyImage = ({ src, alt, defaultImage, imageOnload, style, ref }) => {
  let placeHolder = defaultImage || "https://www.novok.com/storage/themes/novok/img/noimg.jpg";
  let placeHolderTwo = "https://www.chd.mhrd.gov.in/sites/default/files/no-image-available.jpg";
  const [imageSrc, setImageSrc] = useState(placeHolder);
  //   const [imageRef, setImageRef] = useState();
  //   //////console.log({ imageSrc });

  const imageRef = ref || useRef(null);
  const onLoad = (event) => {
    //////console.log({ onLoad: event });
    event.target.classList.add("loaded");
    // imageOnload();
  };

  const onError = (event) => {
    //////console.log({ error: event });
    event.target.classList.remove("loaded");

    event.target.classList.add("has-error");
  };
  // useEffect(() => {
  //   imageRef.current.addEventListener("load", onLoad);
  // });
  useEffect(() => {
    imageRef.current.classList.remove("has-error");
    // imageRef.current.classList.add("loaded");

    let observer;
    let didCancel = false;
    // //////console.log("avaaaaaaaaaaaaaaaaz shooood");
    if (imageRef.current && imageSrc !== src) {
      if (IntersectionObserver) {
        observer = new IntersectionObserver(
          (entries) => {
            // //////console.log({ observer, entries });

            entries.forEach((entry) => {
              if (!didCancel && (entry.intersectionRatio > 0 || entry.isIntersecting)) {
                // imageRef.current.classList.add("loaded");
                imageRef.current.classList.remove("has-error");
                setTimeout(() => {
                  setImageSrc(src);
                }, 200);

                observer.unobserve(imageRef.current) && observer.unobserve(imageRef.current);
              }
            });
          },
          {
            threshold: [0.009999999776482582],
            rootMargin: "75%",
          }
        );
        observer.observe(imageRef.current);
      } else {
        imageRef.current.classList.add("loaded");
        imageRef.current.classList.remove("has-error");
        // Old browsers fallback
        setTimeout(() => {
          setImageSrc(src);
        }, 200);
      }
    }
    return () => {
      didCancel = true;
      // on component cleanup, we remove the listner
      if (observer && observer.unobserve) {
        observer.unobserve(imageRef.current);
      }
    };
  }, [src, imageSrc, imageRef.current]);
  //   return <Image ref={setImageRef} src={imageSrc} alt={alt} onLoad={onLoad} onError={onError} />;
  return (
    <Fragment>
      <img className="imageComponent" style={style} ref={ref || imageRef} src={imageSrc} onLoad={onLoad} alt={alt} onError={onError} />
      <style jsx>{`
        .imageComponent {
          display: block;
        }
        .imageComponent.loaded {
          animation: loaded 0.7s ease-in-out;
        }
        // .imageComponent {
        //   content: url(${placeHolder});
        //   width: 100%;
        //   height: 100%;
        // }
       
        .imageComponent.has-error {
          content: url(${placeHolderTwo});
          width: 100%;
          height: 100%;
          animation: loaded 0.7s ease-in-out;
        }
        .imageComponent.has-error::not(.loaded) {
          content: url(${placeHolderTwo});
          width: 100%;
          height: 100%;
          animation: loaded 0.7s ease-in-out;
        }
        .imageComponent::not(.loaded),
        .imageComponent::not(.has-error) {
          content: url(${placeHolder});
          width: 100%;
          height: 100%;
          animation: loaded 0.7s ease-in-out;
        }

        .image-lazyloading-container {
          display: flex;
          justify-content: center;
          align-items: center;
          width: 100%;
          height: 100%;
          background-color: #ccc;
        }
        @keyframes loaded {
          0% {
            opacity: 0.5;
          }
          100% {
            opacity: 1;
          }
        }
      `}</style>
    </Fragment>
  );
};
export default LazyImage;
