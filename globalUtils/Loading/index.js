import React from "react";
// import { CoffeeLoading } from "react-loadingg";

function Loading(Component) {
  return function LoadingComponent({ isLoading, ...props }) {
    if (!isLoading) return <Component {...props} />;
    else return <div style={{ display: "flex", width: "100vw", height: "100vh", justifyContent: "center", alignItem: "center" }}>{/* <CoffeeLoading size={"large"} /> */}</div>;
  };
}

export default Loading;
