import Loading from "./Loading";
import axiosBase from "./axiosBase";
import hoc from "./hoc";
import formatMoney from "./formatMoney";

const globalUtils = {
  Loading,
  axiosBase,
  hoc,
  formatMoney,
};

export default globalUtils;
