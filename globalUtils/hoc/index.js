import changeHeader from "./changeHeader.js";
import routingHandle from "./routingHandle.js";
const hoc = { changeHeader, routingHandle };
export default hoc;
